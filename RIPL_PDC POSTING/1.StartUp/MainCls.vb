Option Strict Off
Option Explicit On
Imports System.Data.SqlClient
Imports SAPbouiCOM

Public Class MainCls

#Region "Declaration"
    Public WithEvents objApplication As SAPbouiCOM.Application
    'Public objcompany As SAPbobsCOM.Company
    Public objDatabaseCreation As DatabaseCreation
    Public objUtilities As Utilities
    Public objCompany As SAPbobsCOM.Company

    'HardwareKey
    'Public HW() As String = {"C1821141207", "Q0784790958", "S2115447209", "C1279360682"}

    'GeneralService
    Public oGeneralService As SAPbobsCOM.GeneralService
    Public oGeneralData As SAPbobsCOM.GeneralData
    Public oSons As SAPbobsCOM.GeneralDataCollection
    Public oSon As SAPbobsCOM.GeneralData
    Public oChildren As SAPbobsCOM.GeneralDataCollection
    Public oChild As SAPbobsCOM.GeneralData
    Public sCmp As SAPbobsCOM.CompanyService
    Public oGeneralParams As SAPbobsCOM.GeneralDataParams
    Public Shared ohtLookUpForm As Hashtable = New Hashtable

    Public DocNum As String = ""
    Public DocDate As String = ""
    Public ForeignName As String = ""
    Public BaseGRForm As String = ""
    Public GRDistrict As String = ""
    Public GRMOD As String = ""
    Public GRPrice As String = ""

    Private oForm As SAPbouiCOM.Form

    Public ObjPDA As ClsPDAInput
    ' Public objPDCP As clsPDCPosting
    'Public ObjIncomingPayments As clsIncomingPayments
    '' Public ObjOutgoingPayments As clsOutgoingPayments
    'Public ObjDeposit As clsDeposit
    'Public objPForm As clsPostingForm
#End Region

    Public Sub New()
        objUtilities = New Utilities
        objDatabaseCreation = New DatabaseCreation
    End Sub

#Region "Initialilse"
    Public Function Initialise() As Boolean

        Dim AlertFlag As Boolean = False
        objApplication = objUtilities.GetApplication()
        If objApplication Is Nothing Then Return False
        objCompany = objUtilities.GetCompany(objApplication)
        If objCompany Is Nothing Then : Return False : Exit Function : End If
        If Not objDatabaseCreation.CreateTables() Then Return False
        CreateObjects()

        Me.LoadFromXML("Menu.xml")

        objApplication.StatusBar.SetText("Payment Posting Addon connected", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

        Return True
    End Function
#End Region

#Region "Create Object"
    Private Sub CreateObjects()
        'oRO = New clsRO
        ObjPDA = New ClsPDAInput
        ' objPDCP = New clsPDCPosting
        ' ObjIncomingPayments = New clsIncomingPayments
        '' ObjOutgoingPayments = New clsOutgoingPayments
        ' ObjDeposit = New clsDeposit
        '  objPForm = New clsPostingForm
    End Sub
#End Region

#Region "    ~Create UDOs for the UDTs defined in DB Creation~     "

    Public Sub CreatePDCInputUDO()
        If Not Me.UDOExists("NX_OPDC") Then
            Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}, {"U_CCODE", "Customer Code"}, {"U_CNAME", "Customer Name"}}
            Me.registerUDO("NX_OPDC", "Payment Post UDO", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "NX_OPDC", "NX_PDC1")
            findAliasNDescription = Nothing
        End If
    End Sub
    'Public Sub CreatePDCPOSTINGUDO()
    '    If Not Me.UDOExists("RIPL_OPPDC") Then
    '        Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}, {"U_FDATE", "From Date"}, {"U_TDATE", "To Date"}}
    '        Me.registerUDO("RIPL_OPPDC", "PPDC Input UDO", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "RIPL_OPPDC", "RIPL_PPDC1")
    '        findAliasNDescription = Nothing
    '    End If
    'End Sub
    'Public Sub CreatePostingFormUDO()
    '    If Not Me.UDOExists("RIPL_OINVP") Then
    '        Dim findAliasNDescription = New String(,) {{"DocNum", "DocNum"}, {"U_BPCODE", "BP Code"}, {"U_CQNUM", "CQNUM"}}
    '        Me.registerUDO("RIPL_OINVP", "PostingFormUDO", SAPbobsCOM.BoUDOObjType.boud_Document, findAliasNDescription, "RIPL_OINP", "RIPL_INVP")
    '        findAliasNDescription = Nothing
    '    End If
    ' End Sub


#End Region

#Region "UDO Exists"
    Public Function UDOExists(ByVal code As String) As Boolean
        GC.Collect()
        Dim v_UDOMD As SAPbobsCOM.UserObjectsMD
        Dim v_ReturnCode As Boolean
        v_UDOMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        v_ReturnCode = v_UDOMD.GetByKey(code)
        System.Runtime.InteropServices.Marshal.ReleaseComObject(v_UDOMD)
        v_UDOMD = Nothing
        Return v_ReturnCode
    End Function
#End Region

#Region "Register UDO"

    Function registerUDODF(ByVal UDOCode As String, ByVal UDOName As String, ByVal UDOType As SAPbobsCOM.BoUDOObjType, ByVal findAliasNDescription As String(,), ByVal parentTableName As String, Optional ByVal DefaultForm As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tNO, Optional ByVal childTable1 As String = "", Optional ByVal childTable2 As String = "", Optional ByVal childTable3 As String = "", Optional ByVal childTable4 As String = "", Optional ByVal childTable5 As String = "", Optional ByVal LogOption As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tNO) As Boolean
        Dim actionSuccess As Boolean = False
        Try

            registerUDODF = False
            Dim v_udoMD As SAPbobsCOM.UserObjectsMD
            v_udoMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.EnableEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.RebuildEnhancedForm = SAPbobsCOM.BoYesNoEnum.tYES
            ' v_udoMD.CanCreateDefaultForm = DefaultForm
            v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanLog = LogOption
            v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.Code = UDOCode
            v_udoMD.Name = UDOName
            v_udoMD.TableName = parentTableName
            'v_udoMD.FormColumns.SonNumber = 1
            If LogOption = SAPbobsCOM.BoYesNoEnum.tYES Then
                v_udoMD.LogTableName = "A" & parentTableName
            End If


            If DefaultForm = SAPbobsCOM.BoYesNoEnum.tYES Then
                If UDOCode = "obj_gt_pass" Then

                    v_udoMD.FormColumns.FormColumnAlias = "DocEntry"
                    v_udoMD.FormColumns.FormColumnDescription = "DocEntry"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "DocNum"
                    v_udoMD.FormColumns.FormColumnDescription = "DocNum"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_vn"
                    v_udoMD.FormColumns.FormColumnDescription = "Vehicle Number"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_doc_date"
                    v_udoMD.FormColumns.FormColumnDescription = "Doc Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_dn"
                    v_udoMD.FormColumns.FormColumnDescription = "Distributor Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_r_vn"
                    v_udoMD.FormColumns.FormColumnDescription = "return Vehicle number"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_dealadd"
                    v_udoMD.FormColumns.FormColumnDescription = "Dealers address"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_itms"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "items"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_a_qty"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Achieved Quantity"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_r_qty"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "received Quantity"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_return"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Returnable"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_remarks"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Remarks"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 8
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_balqty"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Balace Quantity"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_agt_details" Then

                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_cc_Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Customer Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_frmdate"
                    v_udoMD.FormColumns.FormColumnDescription = "From Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_todate"
                    v_udoMD.FormColumns.FormColumnDescription = "TO Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_AgtCode"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Agent Code"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_agt_Comm"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Agent Comm"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_bns_rate"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "BonusRate"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_cc_pricelist" Then
                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_CCCode"
                    v_udoMD.FormColumns.FormColumnDescription = " Chilling Center COde"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_frmdate"
                    v_udoMD.FormColumns.FormColumnDescription = "From Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_todate"
                    v_udoMD.FormColumns.FormColumnDescription = "TO Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_minSNF"
                    v_udoMD.FormColumns.FormColumnDescription = "Minimum SNF"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_maxSNF"
                    v_udoMD.FormColumns.FormColumnDescription = "Maximum SNF"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_minfat"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Minimum Fat"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_maxfat"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Maximum Fat"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_milkrate"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Milk Rate"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_cust_prod_order" Then

                    v_udoMD.FormColumns.FormColumnAlias = "DocEntry"
                    v_udoMD.FormColumns.FormColumnDescription = "DocEntry"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "DocNum"
                    v_udoMD.FormColumns.FormColumnDescription = "DocNum"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_itm_no"
                    v_udoMD.FormColumns.FormColumnDescription = "Item Number"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_doc_date"
                    v_udoMD.FormColumns.FormColumnDescription = "Doc Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_d_fat"
                    v_udoMD.FormColumns.FormColumnDescription = "Fat Difference"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_d_SNF"
                    v_udoMD.FormColumns.FormColumnDescription = "SNF Difference"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_aqty_tot"
                    v_udoMD.FormColumns.FormColumnDescription = "Act Qty Total"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_afat_tot"
                    v_udoMD.FormColumns.FormColumnDescription = "Act Fat TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_asnf_tot"
                    v_udoMD.FormColumns.FormColumnDescription = "Act snf"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_fqty_tot"
                    v_udoMD.FormColumns.FormColumnDescription = "Fin Qty TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_fFat_tot"
                    v_udoMD.FormColumns.FormColumnDescription = "Fin Fat TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_fsnf_tot"
                    v_udoMD.FormColumns.FormColumnDescription = "Fin SNF TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_descrp_1"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Description Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_qty1"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Quantity Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_fatpr1"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Fat Percnet Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_snfpr1"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "SNF Percnet Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_fatkg1"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Fat kg Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 8
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_SNFKg1"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "SNF KG Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_descrp_2"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Description Actual"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_qtykg2"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Qty KGs Final"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_fatpr2"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "FAT Percent Final"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_snfpr2"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "SNF Percent Final"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_SNFkg2"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "SNF KG Final"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 8
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_qtyltr2"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Qty ltrs Final"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()



                ElseIf UDOCode = "obj_fat_rate" Then

                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_CCCode"
                    v_udoMD.FormColumns.FormColumnDescription = "Chilling Center COde"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()


                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_KGFRate"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "kgFat Rate"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_KGSNF"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "kgSNF Rate"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_KGTSRate"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "kg TS Rate"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_frmDate"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "From Date"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_toDate"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "TO Date"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_for_assign_cc" Then

                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_CCName"
                    v_udoMD.FormColumns.FormColumnDescription = "Chilling Center Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_frm_date"
                    v_udoMD.FormColumns.FormColumnDescription = "from Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_to_date"
                    v_udoMD.FormColumns.FormColumnDescription = "to Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_equation"
                    v_udoMD.FormColumns.FormColumnDescription = "formula"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                ElseIf UDOCode = "obj_formula_creation" Then

                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_Doc_date"
                    v_udoMD.FormColumns.FormColumnDescription = "Document Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_CCName"
                    v_udoMD.FormColumns.FormColumnDescription = "Chilling Center Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_formula"
                    v_udoMD.FormColumns.FormColumnDescription = "formula equation"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()


                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_paracode"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Parameter Code"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_paraname"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Parameter Name"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_pay_rec" Then

                    v_udoMD.FormColumns.FormColumnAlias = "DocEntry"
                    v_udoMD.FormColumns.FormColumnDescription = "DocEntry"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "DocNum"
                    v_udoMD.FormColumns.FormColumnDescription = "DocNum"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_vac_id"
                    v_udoMD.FormColumns.FormColumnDescription = "VAC ID"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_doc_date"
                    v_udoMD.FormColumns.FormColumnDescription = "Doc Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_frm_dt"
                    v_udoMD.FormColumns.FormColumnDescription = "from Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_to_date"
                    v_udoMD.FormColumns.FormColumnDescription = "to Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_tovac_id"
                    v_udoMD.FormColumns.FormColumnDescription = "TOVAC ID"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()


                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_ap_inv"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "AP Invoice"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_chk_ap"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "AP Invoice Check"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_tot_amt"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Total Amount"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_tot_payd"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Total Payment Due"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_ap_inv_d"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "AP Invoice Datee"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 8
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_avacid"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Agent VAC ID"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 9
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_agtCode"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Agent Code"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_AR_INV"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "AR Invoice"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_tot_amt"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "total Amount"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_chk_ar"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "AR Invoice Check"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_tot_payd"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "total Payment Due"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_ar_inv_d"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "AR Invoice DAte"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 8
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_cvacid"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Customer VAC ID"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 9
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_ccode"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Customer Code"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_chk_bon"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Check Bnus"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_agt_ID"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Agent ID"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_bon_amt"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Bonus Amount"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_route_master" Then

                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_rou_type"
                    v_udoMD.FormColumns.FormColumnDescription = "Route type"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Code"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                ElseIf UDOCode = "obj_sales" Then

                    v_udoMD.FormColumns.FormColumnAlias = "DocEntry"
                    v_udoMD.FormColumns.FormColumnDescription = "DocEntry"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "DocNum"
                    v_udoMD.FormColumns.FormColumnDescription = "DocNum"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_s_rid"
                    v_udoMD.FormColumns.FormColumnDescription = "Route Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_sname"
                    v_udoMD.FormColumns.FormColumnDescription = "Sales Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_vehino"
                    v_udoMD.FormColumns.FormColumnDescription = "Vehicle Nuber"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_s_date"
                    v_udoMD.FormColumns.FormColumnDescription = "Doc Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_cash_ex"
                    v_udoMD.FormColumns.FormColumnDescription = "Cash(Excess+/Short-)"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_LeakMilk"
                    v_udoMD.FormColumns.FormColumnDescription = "Total Leakage"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_lesswt"
                    v_udoMD.FormColumns.FormColumnDescription = "Less Weight"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_shortage"
                    v_udoMD.FormColumns.FormColumnDescription = "Shortage"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_1000"
                    v_udoMD.FormColumns.FormColumnDescription = "1000"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_500"
                    v_udoMD.FormColumns.FormColumnDescription = "500"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_100"
                    v_udoMD.FormColumns.FormColumnDescription = "100"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_50"
                    v_udoMD.FormColumns.FormColumnDescription = "50"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_20"
                    v_udoMD.FormColumns.FormColumnDescription = "20"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_10"
                    v_udoMD.FormColumns.FormColumnDescription = "10"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_five"
                    v_udoMD.FormColumns.FormColumnDescription = "five"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_1000tot"
                    v_udoMD.FormColumns.FormColumnDescription = "1000 TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_500tot"
                    v_udoMD.FormColumns.FormColumnDescription = "500 TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_100tot"
                    v_udoMD.FormColumns.FormColumnDescription = "100 TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_50tot"
                    v_udoMD.FormColumns.FormColumnDescription = "50 TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_20tot"
                    v_udoMD.FormColumns.FormColumnDescription = "20 TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_10tot"
                    v_udoMD.FormColumns.FormColumnDescription = "10 TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_fivetot"
                    v_udoMD.FormColumns.FormColumnDescription = "Five TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_coins"
                    v_udoMD.FormColumns.FormColumnDescription = "Coins"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_Phone"
                    v_udoMD.FormColumns.FormColumnDescription = "Phone"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_total"
                    v_udoMD.FormColumns.FormColumnDescription = "TOtal"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_tot_rec"
                    v_udoMD.FormColumns.FormColumnDescription = "TOtal DC Cum Receipt"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_rjtdmilk"
                    v_udoMD.FormColumns.FormColumnDescription = "Rejected Milk"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_voucher"
                    v_udoMD.FormColumns.FormColumnDescription = "Voucher"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_icode"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Item COde"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_mloaded"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Milk Loaded"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_rtdqty"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Return Quantity"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_leakqty"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Leakaged Quantity"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_diffqty"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Difference Quantity"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_itms"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Items"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_loaded"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Loaded"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_recd"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Received"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 2
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_diff"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Difference"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_sampling" Then


                    v_udoMD.FormColumns.FormColumnAlias = "DocEntry"
                    v_udoMD.FormColumns.FormColumnDescription = "DocEntry"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "DocNum"
                    v_udoMD.FormColumns.FormColumnDescription = "DocNum"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_sam_date"
                    v_udoMD.FormColumns.FormColumnDescription = "Sample Date"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_cc_name"
                    v_udoMD.FormColumns.FormColumnDescription = "CC Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_Rtype"
                    v_udoMD.FormColumns.FormColumnDescription = "Route Type"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "DocEntry"
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 3
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_fat"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "fat percent"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 4
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_snf"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "snf percent"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 5
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_samid"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "Sample ID"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 6
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_clr"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "CLR Value"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()
                    v_udoMD.EnhancedFormColumns.ChildNumber = 1
                    v_udoMD.EnhancedFormColumns.ColumnNumber = 7
                    v_udoMD.EnhancedFormColumns.ColumnAlias = "U_grpo_no"
                    v_udoMD.EnhancedFormColumns.ColumnDescription = "GRPo Number"
                    v_udoMD.EnhancedFormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.ColumnIsUsed = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.EnhancedFormColumns.Add()

                ElseIf UDOCode = "obj_vehicle_master" Then

                    v_udoMD.FormColumns.FormColumnAlias = "Code"
                    v_udoMD.FormColumns.FormColumnDescription = "Code"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "Name"
                    v_udoMD.FormColumns.FormColumnDescription = "Name"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_Routeid"
                    v_udoMD.FormColumns.FormColumnDescription = "Route ID"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_vehitype"
                    v_udoMD.FormColumns.FormColumnDescription = "Vehicle Type"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_Address"
                    v_udoMD.FormColumns.FormColumnDescription = "Address"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_pan_no"
                    v_udoMD.FormColumns.FormColumnDescription = "PAN No"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_contact"
                    v_udoMD.FormColumns.FormColumnDescription = "contact no"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_DName"
                    v_udoMD.FormColumns.FormColumnDescription = "Driver Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_CName"
                    v_udoMD.FormColumns.FormColumnDescription = "Cleaner Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_TName"
                    v_udoMD.FormColumns.FormColumnDescription = "Transporter Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_cc_name"
                    v_udoMD.FormColumns.FormColumnDescription = "CC Name"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()

                ElseIf UDOCode = "ROUTE" Then

                    v_udoMD.FormColumns.FormColumnAlias = "DocEntry"
                    v_udoMD.FormColumns.FormColumnDescription = "DocEntry"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "DocNum"
                    v_udoMD.FormColumns.FormColumnDescription = "DocNum"
                    v_udoMD.FormColumns.Add()
                    v_udoMD.FormColumns.FormColumnAlias = "U_route"
                    v_udoMD.FormColumns.FormColumnDescription = "Route"
                    v_udoMD.FormColumns.Editable = SAPbobsCOM.BoYesNoEnum.tYES
                    v_udoMD.FormColumns.Add()


                End If

            End If

            v_udoMD.ObjectType = UDOType
            For i As Int16 = 0 To findAliasNDescription.GetLength(0) - 1
                If i > 0 Then v_udoMD.FindColumns.Add()
                v_udoMD.FindColumns.ColumnAlias = findAliasNDescription(i, 0)
                v_udoMD.FindColumns.ColumnDescription = findAliasNDescription(i, 1)
            Next

            If childTable1 <> "" Then
                v_udoMD.ChildTables.TableName = childTable1
                v_udoMD.ChildTables.Add()
            End If
            If childTable2 <> "" Then

                v_udoMD.ChildTables.TableName = childTable2
                v_udoMD.ChildTables.Add()
            End If
            If childTable3 <> "" Then
                v_udoMD.ChildTables.TableName = childTable3
                v_udoMD.ChildTables.Add()
            End If
            If childTable4 <> "" Then
                v_udoMD.ChildTables.TableName = childTable4
                v_udoMD.ChildTables.Add()
            End If
            If childTable5 <> "" Then
                v_udoMD.ChildTables.TableName = childTable5
                v_udoMD.ChildTables.Add()
            End If

            If v_udoMD.Add() = 0 Then
                registerUDODF = True
                objApplication.StatusBar.SetText("Successfully Registered UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            Else
                objApplication.StatusBar.SetText("Failed to Register UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                registerUDODF = False
            End If
            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_udoMD)
            v_udoMD = Nothing
            GC.Collect()
        Catch ex As Exception
            objApplication.SetStatusBarMessage(ex.Message)
        End Try

    End Function


    Function registerUDO(ByVal UDOCode As String, ByVal UDOName As String, ByVal UDOType As SAPbobsCOM.BoUDOObjType, ByVal findAliasNDescription As String(,), ByVal parentTableName As String, Optional ByVal childTable1 As String = "", Optional ByVal childTable2 As String = "", Optional ByVal childTable3 As String = "", Optional ByVal childTable4 As String = "", Optional ByVal childTable5 As String = "", Optional ByVal LogOption As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tNO) As Boolean
        Dim actionSuccess As Boolean = False
        Try
            registerUDO = False
            Dim v_udoMD As SAPbobsCOM.UserObjectsMD
            v_udoMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanLog = LogOption
            v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.Code = UDOCode
            v_udoMD.Name = UDOName
            v_udoMD.TableName = parentTableName
            If LogOption = SAPbobsCOM.BoYesNoEnum.tYES Then
                v_udoMD.LogTableName = "A" & parentTableName
            End If
            v_udoMD.ObjectType = UDOType
            For i As Int16 = 0 To findAliasNDescription.GetLength(0) - 1
                If i > 0 Then v_udoMD.FindColumns.Add()
                v_udoMD.FindColumns.ColumnAlias = findAliasNDescription(i, 0)
                v_udoMD.FindColumns.ColumnDescription = findAliasNDescription(i, 1)
            Next
            If childTable1 <> "" Then
                v_udoMD.ChildTables.TableName = childTable1
                v_udoMD.ChildTables.Add()
            End If
            If childTable2 <> "" Then
                v_udoMD.ChildTables.TableName = childTable2
                v_udoMD.ChildTables.Add()
            End If
            If childTable3 <> "" Then
                v_udoMD.ChildTables.TableName = childTable3
                v_udoMD.ChildTables.Add()
            End If
            If childTable4 <> "" Then
                v_udoMD.ChildTables.TableName = childTable4
                v_udoMD.ChildTables.Add()
            End If
            If childTable5 <> "" Then
                v_udoMD.ChildTables.TableName = childTable5
                v_udoMD.ChildTables.Add()
            End If

            If v_udoMD.Add() = 0 Then
                registerUDO = True
                objApplication.StatusBar.SetText("Successfully Registered UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            Else
                objApplication.StatusBar.SetText("Failed to Register UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                registerUDO = False
            End If
            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_udoMD)
            v_udoMD = Nothing
            GC.Collect()
        Catch ex As Exception
            objApplication.SetStatusBarMessage(ex.Message)
        End Try
    End Function

    Function registerUDONoLog(ByVal UDOCode As String, ByVal UDOName As String, ByVal UDOType As SAPbobsCOM.BoUDOObjType, ByVal findAliasNDescription As String(,), ByVal parentTableName As String, Optional ByVal childTable1 As String = "", Optional ByVal childTable2 As String = "", Optional ByVal childTable3 As String = "", Optional ByVal childTable4 As String = "", Optional ByVal childTable5 As String = "", Optional ByVal LogOption As SAPbobsCOM.BoYesNoEnum = SAPbobsCOM.BoYesNoEnum.tNO) As Boolean
        Dim actionSuccess As Boolean = False
        Try
            registerUDONoLog = False
            Dim v_udoMD As SAPbobsCOM.UserObjectsMD
            v_udoMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
            v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanClose = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanCreateDefaultForm = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanFind = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.CanLog = LogOption
            v_udoMD.CanLog = SAPbobsCOM.BoYesNoEnum.tNO
            v_udoMD.CanYearTransfer = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.ManageSeries = SAPbobsCOM.BoYesNoEnum.tYES
            v_udoMD.Code = UDOCode
            v_udoMD.Name = UDOName
            v_udoMD.TableName = parentTableName
            If LogOption = SAPbobsCOM.BoYesNoEnum.tYES Then
                v_udoMD.LogTableName = "A" & parentTableName
            End If
            v_udoMD.ObjectType = UDOType
            For i As Int16 = 0 To findAliasNDescription.GetLength(0) - 1
                If i > 0 Then v_udoMD.FindColumns.Add()
                v_udoMD.FindColumns.ColumnAlias = findAliasNDescription(i, 0)
                v_udoMD.FindColumns.ColumnDescription = findAliasNDescription(i, 1)
            Next
            If childTable1 <> "" Then
                v_udoMD.ChildTables.TableName = childTable1
                v_udoMD.ChildTables.Add()
            End If
            If childTable2 <> "" Then
                v_udoMD.ChildTables.TableName = childTable2
                v_udoMD.ChildTables.Add()
            End If
            If childTable3 <> "" Then
                v_udoMD.ChildTables.TableName = childTable3
                v_udoMD.ChildTables.Add()
            End If
            If childTable4 <> "" Then
                v_udoMD.ChildTables.TableName = childTable4
                v_udoMD.ChildTables.Add()
            End If
            If childTable5 <> "" Then
                v_udoMD.ChildTables.TableName = childTable5
                v_udoMD.ChildTables.Add()
            End If

            If v_udoMD.Add() = 0 Then
                registerUDONoLog = True
                objApplication.StatusBar.SetText("Successfully Registered UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            Else
                objApplication.StatusBar.SetText("Failed to Register UDO >" & UDOCode & ">" & UDOName & " >" & objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                registerUDONoLog = False
            End If
            System.Runtime.InteropServices.Marshal.ReleaseComObject(v_udoMD)
            v_udoMD = Nothing
            GC.Collect()
        Catch ex As Exception
            objApplication.SetStatusBarMessage(ex.Message)
        End Try
    End Function
#End Region

#Region "Update UDO With Delete Records Feature"
    Public Sub UpdateUDOtoRemoveRecords(ByVal UDOCode As String)
        Dim v_udoMD As SAPbobsCOM.UserObjectsMD
        v_udoMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        v_udoMD.GetByKey(UDOCode)
        v_udoMD.CanDelete = SAPbobsCOM.BoYesNoEnum.tYES
        v_udoMD.Update()
    End Sub
#End Region

#Region "Update UDO With Cancel Records Feature"
    Public Sub UpdateUDOtoCancelRecords(ByVal UDOCode As String)
        Dim v_udoMD As SAPbobsCOM.UserObjectsMD
        v_udoMD = objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oUserObjectsMD)
        v_udoMD.GetByKey(UDOCode)
        v_udoMD.CanCancel = SAPbobsCOM.BoYesNoEnum.tYES
        v_udoMD.Update()
    End Sub
#End Region

#Region "add menus with xml"
    Private Sub LoadFromXML(ByRef FileName As String)

        Dim oXmlDoc As Xml.XmlDocument
        oXmlDoc = New Xml.XmlDocument
        '// load the content of the XML File
        Dim sPath As String
        sPath = IO.Directory.GetParent(System.Windows.Forms.Application.ExecutablePath).ToString
        oXmlDoc.Load(sPath & "\" & FileName)
        '// load the form to the SBO application in one batch
        objApplication.LoadBatchActions(oXmlDoc.InnerXml)
        sPath = objApplication.GetLastBatchResults()

    End Sub

#End Region

#Region "Item Event"
    Private Sub objApplication_ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean) Handles objApplication.ItemEvent
        Try
            '------------------------------------------------------------------------
            Try
                If PaymentPosting.MainCls.ohtLookUpForm.ContainsValue(FormUID) = True Then
                    Dim keys As ICollection = PaymentPosting.MainCls.ohtLookUpForm.Keys
                    Dim keysArray(PaymentPosting.MainCls.ohtLookUpForm.Count - 1) As String
                    keys.CopyTo(keysArray, 0)
                    For Each key As String In keysArray
                        If FormUID = PaymentPosting.MainCls.ohtLookUpForm(key) Then
                            While PaymentPosting.MainCls.ohtLookUpForm.ContainsValue(key) = True
                                For Each dKey As String In keysArray
                                    If key = PaymentPosting.MainCls.ohtLookUpForm(dKey) Then
                                        key = dKey
                                        Exit For
                                    End If
                                Next
                            End While
                            objApplication.Forms.Item(key).Select()
                            BubbleEvent = False
                            Exit Sub
                        End If
                    Next
                End If
            Catch ex As Exception
            End Try

            Select Case pVal.FormTypeEx
                'Addon Files
                Case "NX_PDC_FORM"
                    ObjPDA.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "RIPL_OPPDC_FORM"
                    '    objPDCP.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "170"
                    '    objIncomingPayments.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "426"
                    '    ObjOutgoingPayments.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "606"
                    '    ObjDeposit.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "RIPL_INVP_FORM"
                    '    objPForm.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "CLA_APM_PJEUS"
                    '    oJEPC.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "RIPL_FPL_Form"
                    '    oFertilizerPriceList.ItemEvent(FormUID, pVal, BubbleEvent)
                    'Case "RIPL_BSP_Form"
                    '    oBatchwiseSaleValue.ItemEvent(FormUID, pVal, BubbleEvent)

            End Select

        Catch ex As Exception
            objApplication.MessageBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Menu Events"
    Private Sub objApplication_MenuEvent(ByRef pVal As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean) Handles objApplication.MenuEvent
        Dim objform As SAPbouiCOM.Form
        Try
            'objform = objMain.objApplication.Forms.ActiveForm

            Select Case pVal.MenuUID

                Case "NX_OPDC"
                    ObjPDA.MenuEvent(pVal, BubbleEvent)
                'Case "RIPL_OPPDC"
                '    objPDCP.MenuEvent(pVal, BubbleEvent)
                Case "1282"
                    objform = objMain.objApplication.Forms.ActiveForm
                    If objform.TypeEx = "NX_PDC_FORM" Then
                        ObjPDA.MenuEvent(pVal, BubbleEvent)
                        'ElseIf objform.TypeEx = "RIPL_OPPDC_FORM" Then
                        '    objPDCP.MenuEvent(pVal, BubbleEvent)
                    End If
                Case "Delete Row"
                    objform = objMain.objApplication.Forms.ActiveForm
                    'If objform.TypeEx = "RIPL_OPPDC_FORM" Then
                    '    objPDCP.MenuEvent(pVal, BubbleEvent)
                    'ElseIf objform.Type = "RIPL_PDC_FORM" Then
                    '    ObjPDA.MenuEvent(pVal, BubbleEvent)
                    'End If
                Case "1284"
                    objform = objMain.objApplication.Forms.ActiveForm
                    If objform.TypeEx = "170" Then
                        ' ObjIncomingPayments.MenuEvent(pVal, BubbleEvent)
                        'ElseIf objform.TypeEx = "426" Then
                        '    ObjOutgoingPayments.MenuEvent(pVal, BubbleEvent)
                        'ElseIf objform.TypeEx = "606" Then
                        'ObjDeposit.MenuEvent(pVal, BubbleEvent)
                    End If
                Case "Cancel"
                    objform = objMain.objApplication.Forms.ActiveForm
                    If objform.TypeEx = "NX_PDC_FORM" Then
                        ObjPDA.MenuEvent(pVal, BubbleEvent)
                    End If
            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "Form Data Event"
    Private Sub objApplication_FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean) Handles objApplication.FormDataEvent
        Try
            Select Case BusinessObjectInfo.FormTypeEx
                ''Sales
                'Case "140"
                '    oDelivery.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "133"
                '    oARInvoice.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "179"
                '    oARCreditMemo.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "180"
                '    oSalesReturn.FormDataEvent(BusinessObjectInfo, BubbleEvent)

                '    'Purchase
                'Case "143"
                '    oGRPO.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "141"
                '    oAPInvoice.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "181"
                '    oAPCreditMemo.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "182"
                '    oPurchaseReturn.FormDataEvent(BusinessObjectInfo, BubbleEvent)

                '    'Payments
                'Case "170"
                '    oIncomingPayments.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "426"
                '    oOutgoingPayments.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "606"
                '    oDeposits.FormDataEvent(BusinessObjectInfo, BubbleEvent)

                '    'Inventory
                'Case "940"
                '    oInventoryTransfer.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "720"
                '    oGoodsIssue.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                'Case "721"
                '    oGoodsReceipt.FormDataEvent(BusinessObjectInfo, BubbleEvent)

                Case "NX_PDC_FORM"
                    ObjPDA.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "RIPL_OPPDC_FORM"
                    '    objPDCP.FormDataEvent(BusinessObjectInfo, BubbleEvent)
                    'Case "RIPL_FPL_Form"
                    '    oFertilizerPriceList.FormDataEvent(BusinessObjectInfo, BubbleEvent)
            End Select

        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        Finally
        End Try
    End Sub
#End Region

#Region "Right Click Event"
    Private Sub objApplication_RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean) Handles objApplication.RightClickEvent
        Try
            Dim objForm As SAPbouiCOM.Form
            objForm = objMain.objApplication.Forms.Item(eventInfo.FormUID)

            'If objForm.TypeEx = "RIPL_OPPDC_FORM" Then
            '    ' objPDCP.RightClickEvent(eventInfo, BubbleEvent)
            'ElseIf objForm.TypeEx = "NX_PDC_FORM" Then
            '    ObjPDA.RightClickEvent(eventInfo, BubbleEvent)
            'If objForm.TypeEx = "RIPL_INVP_FORM" Then
            '    objPForm.RightClickEvent(eventInfo, BubbleEvent)
            'ElseIf objForm.TypeEx = "RIPL_FPL_Form" Then
            '    oFertilizerPriceList.RightClickEvent(eventInfo, BubbleEvent)
            'ElseIf objForm.TypeEx = "RIPL_BSP_Form" Then
            '    oBatchwiseSaleValue.RightClickEvent(eventInfo, BubbleEvent)
            ''End If

        Catch ex As Exception
            objMain.objApplication.MessageBox(ex.Message)
        End Try
    End Sub
#End Region

#Region "Application Event"
    Private Sub objApplication_AppEvent(ByVal EventType As SAPbouiCOM.BoAppEventTypes) Handles objApplication.AppEvent
        Select Case EventType
            Case SAPbouiCOM.BoAppEventTypes.aet_CompanyChanged, SAPbouiCOM.BoAppEventTypes.aet_LanguageChanged, SAPbouiCOM.BoAppEventTypes.aet_ServerTerminition, SAPbouiCOM.BoAppEventTypes.aet_ShutDown
                objCompany.Disconnect()
                End
        End Select
    End Sub
#End Region



End Class
