Public Class DatabaseCreation

#Region "Declaration"
    Private objUtilities As Utilities
    Dim DBCode As String = "v0.1"
    Dim DBName As String = "v0.1"
    Dim Version As String = "v0.1"
#End Region

#Region "DB Creation Main"
    Public Sub New()
        objUtilities = New Utilities
    End Sub

    Public Function CreateTables() As Boolean

        Try
            objUtilities.CreateTable("NX_PDC", "NX DBCONFIG(PDC) TABLE", SAPbobsCOM.BoUTBTableType.bott_NoObject)
            objUtilities.AddAlphaField("@NX_PDC", "VERSION", "VERSION", 101)
            Dim oRs As SAPbobsCOM.Recordset
            oRs = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRs.DoQuery("SELECT * FROM ""@NX_PDC"" where ""U_VERSION"" = '" & Version & "'")
            Dim iDBConfigRecordCount As Integer = oRs.RecordCount
            System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs)
            If iDBConfigRecordCount = 0 Then
                objMain.objApplication.StatusBar.SetText("Your Database will now be upgraded to " + Version + ". Please Wait... ", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                '------------------------------------------------------------------------------

                'UDTS
                Me.CreatePDNINPUTTABLE()
                'Me.CreatePDNPOSTINGTABLE()
                'UDO
                objMain.CreatePDCInputUDO()
                'objMain.CreatePDCPOSTINGUDO()

                ' objUtilities.AddAlphaField("OINV", "CQNUM", "Cheque Num", 100)
                'objUtilities.AddFloatField("OINV", "BALAMT", "BalanceAmount", SAPbobsCOM.BoFldSubTypes.st_Price)
                'objUtilities.AddFloatField("OPCH", "BALAMT", "BalanceAmount", SAPbobsCOM.BoFldSubTypes.st_Price)
                'objUtilities.AddAlphaField("ORCT", "PDCENTRY", "PDC Entry", 30)
                'objUtilities.AddAlphaField("ORCT", "STATUS", "Status", 50)
                objUtilities.AddAlphaField("OVPM", "PENTRY", "PaymentPostEntry", 30)
                'objUtilities.AddAlphaField("OVPM", "STATUS", "Status", 50)
                'objUtilities.AddAlphaField("ODPS", "PDCENTRY", "PDC Entry", 30)
                'objUtilities.AddAlphaField("ODPS", "STATUS", "Status", 50)

                ''Close DB Script
                objUtilities.AddDataToNoObjectTable("NX_PDC", DBCode, DBName, "U_Version", Version)

                objMain.objApplication.StatusBar.SetText("Your Database has now been upgraded to Version " + Version + ".", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Return False
        End Try
        Return True
    End Function
#End Region

#Region "Create Tables"

    Sub CreatePDNINPUTTABLE()
        objMain.objUtilities.CreateTable("NX_OPDC", "PaymentPost", SAPbobsCOM.BoUTBTableType.bott_Document)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "CCODE", "CardCode", 30)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "CNAME", "CardName", 100)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "SERIES", "SERIES", 100)
        objMain.objUtilities.AddDateField("@NX_OPDC", "DDATE", "DocDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "STATUS", "Status", 30)
        'objMain.objUtilities.AddAlphaField("@NX_OPDC", "AENTRY", "AENTRY", 100)
        'objMain.objUtilities.AddAlphaField("@NX_OPDC", "OENTRY", "OENTRY", 100)
        objMain.objUtilities.AddAlphaMemoField("@NX_OPDC", "RMRK", "Remarks", 64000)
        objMain.objUtilities.AddDateField("@NX_OPDC", "FDATE", "FromDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objMain.objUtilities.AddDateField("@NX_OPDC", "TDATE", "ToDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objMain.objUtilities.AddDateField("@NX_OPDC", "PDATE", "PostDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objMain.objUtilities.AddDateField("@NX_OPDC", "ASDATE", "AgDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "BGL", "BankGL", 100)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "BRANCH", "BRANCH", 100)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "LTYPE", "LTYPE", 30)
        objMain.objUtilities.AddAlphaField("@NX_OPDC", "PAYMODE", "PAY MODE", 100)


        objMain.objUtilities.CreateTable("NX_PDC1", "Paymentpost1", SAPbobsCOM.BoUTBTableType.bott_DocumentLines)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "CHECK", "CHECK", 1)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "CCODE", "CardCode", 30)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "TYPE", "TYPE", 30)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "CNAME", "CardName", 100)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "INVBAL", "INVBAL", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "INVTTL", "INVTTL", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddDateField("@NX_PDC1", "INDATE", "INVDate", SAPbobsCOM.BoFldSubTypes.st_None)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "IENTRY", "IIENT", 100)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "IDNUM", "PIDN", 100)
        ' objMain.objUtilities.AddAlphaField("@NX_OPDC", "AENTRY", "AENTRY", 100)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "OENTRY", "OENTRY", 100)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "PAMNT", "PayAMt", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "30", "30", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "60", "60", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "90", "90", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "180", "180", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "270", "270", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "360", "360", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddFloatField("@NX_PDC1", "ABOVE", "ABOVE", SAPbobsCOM.BoFldSubTypes.st_Price)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "STATUS", "Status", 30)
        objMain.objUtilities.AddAlphaField("@NX_PDC1", "PAYMODE", "PAY MODE", 100)


    End Sub

#End Region

End Class

