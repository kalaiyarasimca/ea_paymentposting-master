﻿Public Class clsOutgoingPayments
    Dim objForm, objOPForm As SAPbouiCOM.Form
    Dim ODB_Head, oDBs_Details As SAPbouiCOM.DBDataSource
    Dim objMatrix As SAPbouiCOM.Matrix

    Sub MenuEvent(ByRef pVAl As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try

            If pVAl.MenuUID = "1284" And pVAl.BeforeAction = True Then
                objForm = objMain.objApplication.Forms.GetForm("426", objMain.objApplication.Forms.ActiveForm.TypeCount)

                Dim CheckUser As String = "Select ""U_PDCENTRY"" From OVPM Where ""U_PDCENTRY""='" & objForm.Items.Item("edt_PDCERY").Specific.Value & "'"
                Dim oRsCheckUser As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRsCheckUser.DoQuery(CheckUser)

                If oRsCheckUser.RecordCount <> 0 Then
                    objMain.objApplication.StatusBar.SetText("You don't have authorization to cancel the payments...")
                    BubbleEvent = False
                End If
            End If
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try

            Select Case pVal.EventType


                Case SAPbouiCOM.BoEventTypes.et_FORM_LOAD
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    ODB_Head = objForm.DataSources.DBDataSources.Item("OVPM")


                    If pVal.BeforeAction = True Then
                        objMain.objUtilities.AddLabel(objForm.UniqueID, "lbl_PDCERY", objForm.Items.Item("53").Top + objForm.Items.Item("53").Height + 2, objForm.Items.Item("53").Left, _
                                                        objForm.Items.Item("53").Width, "PDC Entry", "53")

                        objMain.objUtilities.AddEditBox(objForm.UniqueID, "edt_PDCERY", objForm.Items.Item("lbl_PDCERY").Top, objForm.Items.Item("52").Left, _
                                              objForm.Items.Item("52").Width, "OVPM", "U_PDCENTRY", "lbl_PDCERY")

                        objMain.objUtilities.AddLinkButton1(objForm.UniqueID, "lnk_PDCERY", objForm.Items.Item("lbl_PDCERY").Top + 1, objForm.Items.Item("52").Left - 20, _
                                                             "edt_PDCERY", "RIPL_OPDC")

                        objMain.objUtilities.AddLabel(objForm.UniqueID, "lbl_STATUS", objForm.Items.Item("lbl_PDCERY").Top + objForm.Items.Item("lbl_PDCERY").Height + 2, objForm.Items.Item("lbl_PDCERY").Left, _
                                                       objForm.Items.Item("lbl_PDCERY").Width, "Status", "lbl_PDCERY")

                        objMain.objUtilities.AddEditBox(objForm.UniqueID, "edt_STATUS", objForm.Items.Item("lbl_STATUS").Top, objForm.Items.Item("edt_PDCERY").Left, _
                                              objForm.Items.Item("edt_PDCERY").Width, "OVPM", "U_STATUS", "lbl_STATUS")


                        objForm.Items.Item("edt_PDCERY").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                        objForm.Items.Item("edt_PDCERY").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

                        objForm.Items.Item("edt_STATUS").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
                        objForm.Items.Item("edt_STATUS").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)


                    End If


                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    ODB_Head = objForm.DataSources.DBDataSources.Item("OVPM")

                    If pVal.ItemUID = "lnk_PDCERY" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        If objForm.Items.Item("edt_PDCERY").Specific.value <> "" Then
                            objMain.objApplication.ActivateMenuItem("RIPL_OPDC")
                            objOPForm = objMain.objApplication.Forms.GetForm("RIPL_PDC_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)
                            objOPForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                            Dim getInputPDC As String = "Select T0.""DocNum"" From ""@NX_OPDC"" T0 " & _
                                " Where T0.""DocEntry""='" & ODB_Head.GetValue("U_PDCENTRY", 0) & "' "
                            Dim OrsgetInputPDC As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            OrsgetInputPDC.DoQuery(getInputPDC)
                            objOPForm.Items.Item("16").Specific.Value = OrsgetInputPDC.Fields.Item("DocNum").Value
                            objOPForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                        End If
                    End If


            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
End Class
