﻿Public Class clsPDCPosting
#Region "Declarations"
    Dim objForm, ObjPostingForm, ObjPDCInputForm As SAPbouiCOM.Form
    Dim oDBs_Head, ODBs_Details As SAPbouiCOM.DBDataSource
    Dim ObjMatrix As SAPbouiCOM.Matrix
    Dim oCombobox, oCombobox1, oCombobox2, oCombobox3 As SAPbouiCOM.ComboBox
    Dim oButtonCombo, oButtonCombo1 As SAPbouiCOM.ButtonCombo
    Dim RowId As Integer
    Dim CHeckFlag, FormAddFlag As Boolean
    Dim FormAddFlag1 As Boolean = False
#End Region

    Sub LoadForm()
        Try
            objMain.objUtilities.LoadForm("PDCPOSTING.XML", "RIPL_OPPDC_FORM", ResourceType.Embeded)
            objForm = objMain.objApplication.Forms.GetForm("RIPL_OPPDC_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)
            objForm.Freeze(True)
            objForm.DataBrowser.BrowseBy = "7"
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
            ObjMatrix = objForm.Items.Item("11").Specific

            objForm.Items.Item("7").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("7").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("12").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("12").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("12").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            'objForm.Items.Item("3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'objForm.Items.Item("3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'objForm.Items.Item("3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            'objForm.Items.Item("5").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'objForm.Items.Item("5").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'objForm.Items.Item("5").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("1000001").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("1000001").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("1000001").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            'objForm.Items.Item("15").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'objForm.Items.Item("15").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'objForm.Items.Item("15").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("16").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("16").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("16").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            objForm.Items.Item("17").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("17").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("17").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            objForm.Items.Item("9").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("9").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("9").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            'objForm.Items.Item("18").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            'objForm.Items.Item("18").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            'objForm.Items.Item("18").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)



            oCombobox = objForm.Items.Item("15").Specific
            oCombobox.ValidValues.Add("", "")
            oCombobox.ValidValues.Add("PDC", "PDC")
            oCombobox.ValidValues.Add("SecurityCheque", "Security Cheque")
            oCombobox.ValidValues.Add("CDC", "CDC")
            oCombobox.ValidValues.Add("LC", "LC")
            objForm.Items.Item("15").DisplayDesc = True

            'oCombobox1 = objForm.Items.Item("22").Specific
            'oCombobox1.ValidValues.Add("", "")
            'oCombobox1.ValidValues.Add("Open", "OPen")
            'oCombobox1.ValidValues.Add("Closed", "Closed")
            'objForm.Items.Item("22").DisplayDesc = True

            oButtonCombo = objForm.Items.Item("1000001").Specific
            oButtonCombo.ValidValues.Add("Incoming Payments", "Incoming Payments")
            oButtonCombo.ValidValues.Add("Outgoing Payments", "Outgoing Payments")

            oButtonCombo1 = objForm.Items.Item("16").Specific
            oButtonCombo1.ValidValues.Add("Cancel Incoming Payments", "Cancel Incoming Payments")
            oButtonCombo1.ValidValues.Add("Cancel Outgoing Payments", "Cancel Outgoing Payments")
            oButtonCombo1.ValidValues.Add("Bounced", "Bounced")
            ObjMatrix.AutoResizeColumns()


            objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("20").Specific, "Select ""Series"", ""SeriesName"" From ""NNM1"" Where ""ObjectCode"" = 'RIPL_OPPDC' And ""Indicator"" In " & _
                     "(Select ""Indicator"" From OFPR Where ""PeriodStat"" = 'N') And ""Locked"" = 'N'")
            objForm.Items.Item("20").DisplayDesc = True


            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText("Load Break Down Failed")
        Finally
        End Try
    End Sub

    Sub SetNewLine(ByVal FormUID As String, ByVal MatrixUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)

            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
            ObjMatrix = objForm.Items.Item("11").Specific

            Select Case MatrixUID
                Case "11"
                    ObjMatrix.AddRow()
                    ODBs_Details.SetValue("LineId", ODBs_Details.Offset, ObjMatrix.VisualRowCount)
                    ODBs_Details.SetValue("U_CCODE", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_ACCODE", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_CNAME", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_PDCDT", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_CQCUR", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_BANK", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_CNUM", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_CQTYPE", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_ACTV", ODBs_Details.Offset, "N")
                    ODBs_Details.SetValue("U_IPDOC", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_OPDOC", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_DENTRY", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_CAMOUN", ODBs_Details.Offset, 0)
                    ODBs_Details.SetValue("U_PONACT", ODBs_Details.Offset, 0)
                    ODBs_Details.SetValue("U_DPST", ODBs_Details.Offset, "")
                    ODBs_Details.SetValue("U_STATUS", ODBs_Details.Offset, "")
                    ObjMatrix.SetLineData(ObjMatrix.VisualRowCount)
            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub SetDefaultValues(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
            ObjMatrix = objForm.Items.Item("11").Specific

            objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("20").Specific, "Select ""Series"", ""SeriesName"" From NNM1 Where ""ObjectCode"" = 'RIPL_OPPDC' And ""Indicator"" In " & _
                              "(Select ""Indicator"" From OFPR Where ""PeriodStat"" = 'N') And ""Locked"" = 'N' ")
            oCombobox2 = objForm.Items.Item("20").Specific
            oCombobox2.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)

            'oCombobox3 = objForm.Items.Item("22").Specific
            'oCombobox3.Select("Open", SAPbouiCOM.BoSearchKey.psk_ByValue)
            oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OPPDC"))
            oDBs_Head.SetValue("U_DDATE", oDBs_Head.Offset, Today.ToString("yyyyMMdd"))

            Dim OComboboxSeries As SAPbouiCOM.ComboBox
            OComboboxSeries = objForm.Items.Item("20").Specific
            Dim DateVal As String = objForm.Items.Item("9").Specific.Value
            Dim Series As String = "Select T0.""Series"", T0.""SeriesName"" from NNM1 T0  " & _
                "Inner join OFPR T1 on T0.""Indicator""=T1.""Indicator"" and T0.""ObjectCode"" = 'RIPL_OPPDC'" & _
                                   "Where " & _
                                   "'" & DateVal & "' Between T1.""F_RefDate"" and T1.""T_RefDate"""
            Dim orsSeries As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            orsSeries.DoQuery(Series)
            OComboboxSeries.Select(orsSeries.Fields.Item("SeriesName").Value, SAPbouiCOM.BoSearchKey.psk_ByDescription)

            ObjMatrix.Clear()
            ODBs_Details.Clear()
            ObjMatrix.FlushToDataSource()
            Me.SetNewLine(objForm.UniqueID, "11")
            ObjMatrix.AutoResizeColumns()

        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub MenuEvent(ByRef pVAl As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVAl.MenuUID = "RIPL_OPPDC" And pVAl.BeforeAction = False Then
                Me.LoadForm()
                Me.SetDefaultValues(objForm.UniqueID)
            ElseIf pVAl.MenuUID = "1282" And pVAl.BeforeAction = False Then
                Me.SetDefaultValues(objForm.UniqueID)
            ElseIf pVAl.MenuUID = "Delete Row" And pVAl.BeforeAction = False Then
                ObjMatrix = objForm.Items.Item("11").Specific
                For i As Integer = 1 To ObjMatrix.VisualRowCount - 1
                    If ObjMatrix.IsRowSelected(i) = True Then
                        ObjMatrix.DeleteRow(i)
                    End If
                Next
                For i As Integer = 1 To ObjMatrix.VisualRowCount
                    ObjMatrix.Columns.Item("V_-1").Cells.Item(i).Specific.string = i
                Next
                If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
            End If
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub LoadData(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                Dim check As String = "Select T1.""U_IPDOC"",T1.""U_OPDOC"" From ""@RIPL_PPDC1"" T1  " & _
                    " Where T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0) & "' and (IFNULL(T1.""U_IPDOC"",'')<>'' or IFNULL(T1.""U_STATUS"",'')<>'')"
                Dim orsCheck As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                orsCheck.DoQuery(check)

                If orsCheck.RecordCount <> 0 Then
                    ' objMain.objApplication.StatusBar.SetText("You cannot load the document again as payment postings are done")
                    Exit Sub
                End If
            End If


            Dim GetInputData As String = ""
            If objForm.Items.Item("15").Specific.Value <> "" And objForm.Items.Item("18").Specific.Value <> "" Then
                GetInputData = "Select ""U_CCODE"",""U_CQCUR"",""U_CNAME"",Cast(""U_CDATE"" as date) ""ChequeDate"",""U_BANK"",""U_CTYPE""," & _
                            """U_CNMBR"",""U_AMNT"",""DocEntry"",""U_POACAMT"" from ""@RIPL_OPDC"" Where Ifnull(""U_STATUS"",'')='Open'" & _
                           "And ""U_CTYPE""='" & objForm.Items.Item("15").Specific.Value & "'  " & _
                           "and Cast(""U_CDATE"" As Date)>='" & objForm.Items.Item("3").Specific.Value.ToString & "'" & _
                            " and Cast(""U_CDATE"" as date)<='" & objForm.Items.Item("5").Specific.Value.ToString & "' And " & _
                            """U_CCODE""='" & objForm.Items.Item("18").Specific.Value.ToString & "' "

            ElseIf objForm.Items.Item("15").Specific.Value <> "" And objForm.Items.Item("18").Specific.Value = "" Then
                GetInputData = "Select ""U_CCODE"",""U_CQCUR"",""U_CNAME"",Cast(""U_CDATE"" as date) ""ChequeDate"",""U_BANK"",""U_CTYPE""," & _
                           """U_CNMBR"",""U_AMNT"",""DocEntry"",""U_POACAMT"" from ""@RIPL_OPDC"" Where Ifnull(""U_STATUS"",'')='Open'" & _
                          "And ""U_CTYPE""='" & objForm.Items.Item("15").Specific.Value & "'  " & _
                          "and Cast(""U_CDATE"" As Date)>='" & objForm.Items.Item("3").Specific.Value.ToString & "'" & _
                           " and Cast(""U_CDATE"" as date)<='" & objForm.Items.Item("5").Specific.Value.ToString & "'"

            ElseIf objForm.Items.Item("15").Specific.Value = "" And objForm.Items.Item("18").Specific.Value <> "" Then
                GetInputData = "Select ""U_CCODE"",""U_CQCUR"",""U_CNAME"",Cast(""U_CDATE"" as date) ""ChequeDate"",""U_BANK"",""U_CTYPE""," & _
                           """U_CNMBR"",""U_AMNT"",""DocEntry"",""U_POACAMT"" from ""@RIPL_OPDC"" Where Ifnull(""U_STATUS"",'')='Open'" & _
                          "And ""U_CCODE""='" & objForm.Items.Item("18").Specific.Value & "'  " & _
                          "and Cast(""U_CDATE"" As Date)>='" & objForm.Items.Item("3").Specific.Value.ToString & "'" & _
                           " and Cast(""U_CDATE"" as date)<='" & objForm.Items.Item("5").Specific.Value.ToString & "'"
                'Else
                '    GetInputData = "Select ""U_CCODE"",""U_CQCUR"",""U_CNAME"",Cast(""U_CDATE"" as date) ""ChequeDate"",""U_BANK""," & _
                '              """U_CNMBR"",""U_AMNT"",""DocEntry"" from ""@RIPL_OPDC"" Where Ifnull(""U_STATUS"",'')='Open'" & _
                '             "And ""U_CTYPE""='" & objForm.Items.Item("15").Specific.Value & "'  " & _
                '             "and Cast(""U_CDATE"" As Date)>='" & objForm.Items.Item("3").Specific.Value.ToString & "'" & _
                '              " and Cast(""U_CDATE"" as date)<='" & objForm.Items.Item("5").Specific.Value.ToString & "'"

            End If
            Dim oRsGetInputData As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetInputData.DoQuery(GetInputData)

            If oRsGetInputData.RecordCount = 0 Then
                objMain.objApplication.StatusBar.SetText("There are no cheques in between the given dates")
                ObjMatrix.Clear()
                ODBs_Details.Clear()
                ObjMatrix.FlushToDataSource()
                ObjMatrix.AutoResizeColumns()
                Exit Sub
            End If

            ObjMatrix.Clear()
            ODBs_Details.Clear()
            ObjMatrix.FlushToDataSource()

            For i As Integer = 1 To oRsGetInputData.RecordCount
                ObjMatrix.AddRow()
                ODBs_Details.SetValue("LineId", ODBs_Details.Offset, ObjMatrix.VisualRowCount)
                ODBs_Details.SetValue("U_ACTV", ODBs_Details.Offset, "N")
                ODBs_Details.SetValue("U_CCODE", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_CCODE").Value)
                ODBs_Details.SetValue("U_CNAME", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_CNAME").Value)
                ODBs_Details.SetValue("U_CQCUR", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_CQCUR").Value)
                ODBs_Details.SetValue("U_CQTYPE", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_CTYPE").Value)
                ODBs_Details.SetValue("U_PONACT", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_POACAMT").Value)
                Dim Chequdate As Date = oRsGetInputData.Fields.Item("ChequeDate").Value
                ODBs_Details.SetValue("U_PDCDT", ODBs_Details.Offset, Chequdate.ToString("yyyyMMdd"))

                Dim getBankCode As String = "Select T0.""BankCode"",T1.""GLAccount"" From ODSC T0 inner join DSC1 T1 on T0.""BankCode""=T1.""BankCode""  Where T0.""BankCode""='" & oRsGetInputData.Fields.Item("U_BANK").Value & "'"
                Dim orsGetBankCode As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                orsGetBankCode.DoQuery(getBankCode)
                ODBs_Details.SetValue("U_ACCODE", ODBs_Details.Offset, orsGetBankCode.Fields.Item("GLAccount").Value)
                ODBs_Details.SetValue("U_BANK", ODBs_Details.Offset, orsGetBankCode.Fields.Item("BankCode").Value)
                ODBs_Details.SetValue("U_CNUM", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_CNMBR").Value)
                ODBs_Details.SetValue("U_CQCUR", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_CQCUR").Value)
                ODBs_Details.SetValue("U_IPDOC", ODBs_Details.Offset, "")
                ODBs_Details.SetValue("U_OPDOC", ODBs_Details.Offset, "")
                ODBs_Details.SetValue("U_DPST", ODBs_Details.Offset, "")
                ODBs_Details.SetValue("U_DENTRY", ODBs_Details.Offset, oRsGetInputData.Fields.Item("DocEntry").Value)
                ODBs_Details.SetValue("U_CAMOUN", ODBs_Details.Offset, oRsGetInputData.Fields.Item("U_AMNT").Value)
                ODBs_Details.SetValue("U_STATUS", ODBs_Details.Offset, "")
                ObjMatrix.SetLineData(ObjMatrix.VisualRowCount)
                oRsGetInputData.MoveNext()
            Next
            ObjMatrix.AutoResizeColumns()
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub PostOutgoingPayments(ByVal FormUID As String, ByVal PPDCEntry As String, ByVal SysCurr As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
            ObjMatrix = objForm.Items.Item("11").Specific

            objForm.Freeze(True)
            Dim GetVendors As String = "Select  T1.""U_CCODE"" ""Vendor"", T1.""LineId"",T1.""U_CQCUR"" From ""@RIPL_PPDC1"" T1 Inner Join OCRD T0 On T1.""U_CCODE"" = T0.""CardCode"" Where " & _
              "T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' And T0.""CardType"" = 'S' " & _
              "And IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_OPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) and IFNULL(T1.""U_STATUS"",'')=''"
            Dim oRsGetVendors As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetVendors.DoQuery(GetVendors)


            If oRsGetVendors.RecordCount > 0 Then
                For i As Integer = 1 To oRsGetVendors.RecordCount
                    Dim GetData1 As String = "Select T1.""U_CCODE"" ""Vendor"",Cast(T1.""U_PDCDT"" As Date) ""PPDC Date"",T4.""LineId"",T1.""U_CQCUR"",T8.""DocEntry"",T5.""GLAccount"", " & _
                               "T2.""Currency"" ""Currency"", IFNULL(T4.""U_SENTRY"",'') ""Inv No"", T4.""U_INVCUR"",T8.""U_POACCT"",T8.""U_POACAMT"", " & _
                               "T4.""U_DPAMNT"" ""Inv Amt"",T4.""U_PAMNT"" ""CQCInv Amt"",T5.""Branch"",T5.""Account"",IFNULL(T5.""Country"",'') ""COUNTRY"", " & _
                               "T5.""BankCode"", T4.""U_DPAMNT"",T1.""U_CNUM"",T1.""U_CAMOUN"",IFNULL(T1.""U_DENTRY"",'') ""PPDC Entry"" " & _
                               "from ""@RIPL_OPPDC"" T0 " & _
                               "Inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry""  " & _
                                 "Inner join ""@RIPL_PDC1"" T4 on T1.""U_DENTRY""=T4.""DocEntry"" " & _
                                 "Inner join ""@RIPL_OPDC"" T8 on T4.""DocEntry""=T8.""DocEntry"" " & _
                               "Inner Join OCRD T2 On T1.""U_CCODE""=T2.""CardCode"" " & _
                               "Left Join DSC1 T5 On T2.""HouseBank"" = T5.""BankCode""  " & _
                               "Where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                               "And IFNULL(T1.""U_ACTV"",'N')='Y'  And T1.""U_CCODE"" = '" & oRsGetVendors.Fields.Item("Vendor").Value & "' And " & _
                               "T1.""LineId"" = '" & oRsGetVendors.Fields.Item("LineId").Value & "' and IFNULL(T4.""U_CHECK"",'N')='Y' and " & _
                               "IFNULL(T1.""U_OPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) "
                    Dim oRsGetData1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsGetData1.DoQuery(GetData1)


                    'Dim PPDate1 As Date = Today.Date


                    Dim oOutgoingPayments As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments)
                    oOutgoingPayments.DocType = SAPbobsCOM.BoRcptTypes.rSupplier


                    If oRsGetData1.RecordCount > 0 Then
                        objMain.objApplication.StatusBar.SetText("Outgoing Payments for Line : " & oRsGetVendors.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Dim PPDate1 As Date = oRsGetData1.Fields.Item("PPDC Date").Value
                        Dim CCODE1 As String = oRsGetData1.Fields.Item("Vendor").Value
                        Dim Val As String = objForm.Items.Item("9").Specific.Value
                        Dim ValD As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                        oOutgoingPayments.DocDate = ValD
                        oOutgoingPayments.Remarks = "PDCPosted OutgoingPayments"
                        oOutgoingPayments.DocCurrency = oRsGetData1.Fields.Item("U_CQCUR").Value
                        oOutgoingPayments.TaxDate = ValD
                        oOutgoingPayments.DueDate = ValD
                        oOutgoingPayments.CardCode = CCODE1
                        Dim Docrate1 As Double = oOutgoingPayments.DocRate

                        If oRsGetData1.Fields.Item("Inv No").Value <> "" Then
                            For k As Integer = 1 To oRsGetData1.RecordCount
                                If k > 1 Then
                                    oOutgoingPayments.Invoices.Add()
                                End If
                                oOutgoingPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseInvoice
                                oOutgoingPayments.Invoices.DocEntry = oRsGetData1.Fields.Item("Inv No").Value
                                Dim getLocalCurrency As String = "Select ""SysCurrncy"",""MainCurncy"" From OADM"
                                Dim oRsgetLocalCurrency As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsgetLocalCurrency.DoQuery(getLocalCurrency)

                                Dim GetExcRate1 As String = "Select ""Rate"" ""Rate"",""Currency"" From ORTT Where " & _
                                 "TO_NVARCHAR(""RateDate"" , 'YYYYMMDD')='" & PPDate1.ToString("yyyyMMdd") & "' " & _
                                 " And ""Currency""='" & oRsGetData1.Fields.Item("U_CQCUR").Value & "'  "
                                Dim orsGetExcRate1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                orsGetExcRate1.DoQuery(GetExcRate1)

                                Dim CheqAmnt As Double = 0
                                Dim InvAmnt As Double = 0
                                If orsGetExcRate1.RecordCount > 0 Then
                                    CheqAmnt = CDbl(oRsGetData1.Fields.Item("CQCInv Amt").Value) / CDbl(orsGetExcRate1.Fields.Item("Rate").Value)
                                Else
                                    CheqAmnt = CDbl(oRsGetData1.Fields.Item("CQCInv Amt").Value)
                                End If

                                Dim GetERate1 As String = "Select ""Rate"" ""Rate"",""Currency"" From ORTT Where " & _
                                       "TO_NVARCHAR(""RateDate"" , 'YYYYMMDD')='" & PPDate1.ToString("yyyyMMdd") & "' " & _
                                       " And ""Currency""='" & oRsGetData1.Fields.Item("U_INVCUR").Value & "'  "
                                Dim orsGetERate1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                orsGetERate1.DoQuery(GetERate1)

                                If oRsGetData1.Fields.Item("U_CQCUR").Value = oRsGetData1.Fields.Item("U_INVCUR").Value Then
                                    If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData1.Fields.Item("U_CQCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData1.Fields.Item("U_CQCUR").Value Then
                                        If orsGetExcRate1.RecordCount > 0 Then
                                            InvAmnt = CheqAmnt * CDbl(orsGetExcRate1.Fields.Item("Rate").Value)
                                        Else
                                            InvAmnt = CheqAmnt
                                        End If
                                        oOutgoingPayments.Invoices.AppliedFC = InvAmnt
                                    Else
                                        InvAmnt = CheqAmnt
                                        oOutgoingPayments.Invoices.SumApplied = InvAmnt
                                    End If
                                Else
                                    If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData1.Fields.Item("U_CQCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData1.Fields.Item("U_CQCUR").Value Then
                                        If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData1.Fields.Item("U_INVCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData1.Fields.Item("U_INVCUR").Value Then
                                            If orsGetERate1.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate1.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            oOutgoingPayments.Invoices.AppliedFC = InvAmnt
                                        Else
                                            If orsGetERate1.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate1.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            oOutgoingPayments.Invoices.SumApplied = InvAmnt
                                        End If

                                    Else
                                        If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData1.Fields.Item("U_INVCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData1.Fields.Item("U_INVCUR").Value Then
                                            If orsGetERate1.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate1.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            oOutgoingPayments.Invoices.AppliedFC = InvAmnt
                                        Else
                                            If orsGetERate1.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate1.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            oOutgoingPayments.Invoices.SumApplied = InvAmnt
                                        End If
                                    End If
                                End If
                                oRsGetData1.MoveNext()
                            Next
                        End If

                        oRsGetData1.MoveFirst()

                        Dim TotalCheckSum1 As Double = CDbl(oRsGetData1.Fields.Item("U_CAMOUN").Value)
                        ' oOutgoingPayments.Checks.Branch = oRsGetData1.Fields.Item("DfltBranch").Value
                        oOutgoingPayments.Checks.DueDate = PPDate1
                        ' 
                        Dim X As String = oRsGetData1.Fields.Item("COUNTRY").Value
                        Dim Y As String = oRsGetData1.Fields.Item("BankCode").Value
                        Dim X1 As String = oRsGetData1.Fields.Item("Branch").Value
                        Dim Y2 As String = oRsGetData1.Fields.Item("GLAccount").Value
                        Dim X3 As String = oRsGetData1.Fields.Item("Account").Value

                        oOutgoingPayments.Checks.CountryCode = oRsGetData1.Fields.Item("COUNTRY").Value
                        oOutgoingPayments.Checks.BankCode = oRsGetData1.Fields.Item("BankCode").Value
                        oOutgoingPayments.Checks.Branch = oRsGetData1.Fields.Item("Branch").Value
                        oOutgoingPayments.Checks.CheckAccount = oRsGetData1.Fields.Item("GLAccount").Value
                        oOutgoingPayments.Checks.AccounttNum = oRsGetData1.Fields.Item("Account").Value
                        oOutgoingPayments.Checks.ManualCheck = SAPbobsCOM.BoYesNoEnum.tNO
                        oOutgoingPayments.Checks.CheckNumber = oRsGetData1.Fields.Item("U_CNUM").Value
                        oOutgoingPayments.Checks.CheckSum = TotalCheckSum1
                        oOutgoingPayments.BankChargeAmount = 0
                        oOutgoingPayments.Checks.Add()



                        If oOutgoingPayments.Add <> 0 Then
                            objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription & " for Line - " & oRsGetVendors.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Else
                            Dim oapinv_docno_str1 As String = objMain.objCompany.GetNewObjectKey()
                            objMain.objApplication.StatusBar.SetText("Outgoing Payments For line No: " & oRsGetVendors.Fields.Item("LineId").Value & " Posted Successfully, SAP Record No - " & oapinv_docno_str1, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                            Dim GetIPNo1 As String = "Select ""DocNum"",""DocEntry"" From OVPM Where ""DocEntry"" = '" & oapinv_docno_str1 & "'"
                            Dim oRsGetIPNo1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetIPNo1.DoQuery(GetIPNo1)

                            Dim UpdatePDAInput1 As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='Closed',""U_OENTRY""='" & oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString & "'  Where ""DocEntry""='" & oRsGetData1.Fields.Item("PPDC Entry").Value & "'"
                            Dim oRsUpdatePDAInput1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdatePDAInput1.DoQuery(UpdatePDAInput1)

                            Dim UpdateOP As String = "Update OVPM set ""U_PDCENTRY""='" & oRsGetData1.Fields.Item("PPDC Entry").Value & "' where ""DocEntry""='" & oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString & "'"
                            Dim oRsUpdateOP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdateOP.DoQuery(UpdateOP)

                            'Me.PDCInputDocOP(oRsGetData1.Fields.Item("PPDC Entry").Value, oRsGetIPNo1.Fields.Item("DocEntry").Value, "Closed")

                            Me.UpdateOutgoingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetVendors.Fields.Item("LineId").Value) - 1, oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString, "Closed")
                        End If
                    Else

                        Dim GetPaymntAct1 As String = "Select Top 1 T1.""U_CCODE"" ""Vendor"",Cast(T1.""U_PDCDT"" As Date) ""PPDC Date"",T4.""LineId"",T1.""U_CQCUR"",T8.""DocEntry"",T5.""GLAccount"", " & _
                               "T2.""Currency"" ""Currency"", IFNULL(T4.""U_SENTRY"",'') ""Inv No"", T4.""U_INVCUR"",IFNULL(T8.""U_POACCT"",'N') ""U_POACCT"",T8.""U_POACAMT"", " & _
                               "T4.""U_DPAMNT"" ""Inv Amt"",T4.""U_PAMNT"" ""CQCInv Amt"",T5.""Branch"",T5.""Account"",IFNULL(T5.""Country"",'') ""COUNTRY"", " & _
                               "T5.""BankCode"", T4.""U_DPAMNT"",T1.""U_CNUM"",T1.""U_CAMOUN"",IFNULL(T1.""U_DENTRY"",'') ""PPDC Entry"" " & _
                               "from ""@RIPL_OPPDC"" T0 " & _
                               "Inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry""  " & _
                                 "Inner join ""@RIPL_PDC1"" T4 on T1.""U_DENTRY""=T4.""DocEntry"" " & _
                                 "Inner join ""@RIPL_OPDC"" T8 on T4.""DocEntry""=T8.""DocEntry"" " & _
                               "Inner Join OCRD T2 On T1.""U_CCODE""=T2.""CardCode""  and T2.""CardType""='S'" & _
                               "Left Join DSC1 T5 On T2.""HouseBank"" = T5.""BankCode""  " & _
                               "Where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                               "And IFNULL(T1.""U_ACTV"",'N')='Y'  And T1.""U_CCODE"" = '" & oRsGetVendors.Fields.Item("Vendor").Value & "' And " & _
                               "T1.""LineId"" = '" & oRsGetVendors.Fields.Item("LineId").Value & "' and IFNULL(T4.""U_CHECK"",'N')='N' and " & _
                               "IFNULL(T1.""U_OPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) "
                        Dim oRsGetPaymntAct1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsGetPaymntAct1.DoQuery(GetPaymntAct1)

                        If oRsGetPaymntAct1.Fields.Item("U_POACCT").Value = "Y" And oRsGetPaymntAct1.Fields.Item("U_POACAMT").Value <> 0 Then
                            objMain.objApplication.StatusBar.SetText("Outgoing Payments for Line : " & oRsGetVendors.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            Dim pdcDate As Date = oRsGetPaymntAct1.Fields.Item("PPDC Date").Value
                            Dim Val As String = objForm.Items.Item("9").Specific.Value
                            Dim PostDate As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                            oOutgoingPayments.DocDate = PostDate
                            oOutgoingPayments.Remarks = "PDCPosted OutgoingPayments"
                            oOutgoingPayments.DocCurrency = oRsGetPaymntAct1.Fields.Item("U_CQCUR").Value
                            oOutgoingPayments.TaxDate = PostDate
                            oOutgoingPayments.DueDate = PostDate
                            oOutgoingPayments.CardCode = oRsGetPaymntAct1.Fields.Item("Vendor").Value

                            Dim TotalCheckSum1 As Double = CDbl(oRsGetPaymntAct1.Fields.Item("U_CAMOUN").Value)
                            oOutgoingPayments.Checks.DueDate = pdcDate
                            ' 
                            Dim X As String = oRsGetPaymntAct1.Fields.Item("COUNTRY").Value
                            Dim Y As String = oRsGetPaymntAct1.Fields.Item("BankCode").Value
                            Dim X1 As String = oRsGetPaymntAct1.Fields.Item("Branch").Value
                            Dim Y2 As String = oRsGetPaymntAct1.Fields.Item("GLAccount").Value
                            Dim X3 As String = oRsGetPaymntAct1.Fields.Item("Account").Value

                            oOutgoingPayments.Checks.CountryCode = oRsGetPaymntAct1.Fields.Item("COUNTRY").Value
                            oOutgoingPayments.Checks.BankCode = oRsGetPaymntAct1.Fields.Item("BankCode").Value
                            oOutgoingPayments.Checks.Branch = oRsGetPaymntAct1.Fields.Item("Branch").Value
                            oOutgoingPayments.Checks.CheckAccount = oRsGetPaymntAct1.Fields.Item("GLAccount").Value
                            oOutgoingPayments.Checks.AccounttNum = oRsGetPaymntAct1.Fields.Item("Account").Value
                            oOutgoingPayments.Checks.ManualCheck = SAPbobsCOM.BoYesNoEnum.tNO
                            oOutgoingPayments.Checks.CheckNumber = oRsGetPaymntAct1.Fields.Item("U_CNUM").Value
                            oOutgoingPayments.Checks.CheckSum = TotalCheckSum1
                            oOutgoingPayments.BankChargeAmount = 0
                            oOutgoingPayments.Checks.Add()
                            If oOutgoingPayments.Add <> 0 Then
                                objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription & " for Line - " & oRsGetVendors.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                            Else
                                Dim oapinv_docno_str1 As String = objMain.objCompany.GetNewObjectKey()
                                objMain.objApplication.StatusBar.SetText("Outgoing Payments For line No: " & oRsGetVendors.Fields.Item("LineId").Value & " Posted Successfully, SAP Record No - " & oapinv_docno_str1, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                                Dim GetIPNo1 As String = "Select ""DocNum"",""DocEntry"" From OVPM Where ""DocEntry"" = '" & oapinv_docno_str1 & "'"
                                Dim oRsGetIPNo1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsGetIPNo1.DoQuery(GetIPNo1)

                                Dim UpdatePDAInput1 As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='Closed',""U_OENTRY""='" & oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString & "'  Where ""DocEntry""='" & oRsGetPaymntAct1.Fields.Item("PPDC Entry").Value & "'"
                                Dim oRsUpdatePDAInput1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsUpdatePDAInput1.DoQuery(UpdatePDAInput1)

                                Dim UpdateOP As String = "Update OVPM set ""U_PDCENTRY""='" & oRsGetPaymntAct1.Fields.Item("PPDC Entry").Value & "' where ""DocEntry""='" & oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString & "'"
                                Dim oRsUpdateOP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsUpdateOP.DoQuery(UpdateOP)

                                'Me.PDCInputDocOP(oRsGetData1.Fields.Item("PPDC Entry").Value, oRsGetIPNo1.Fields.Item("DocEntry").Value, "Closed")

                                Me.UpdateOutgoingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetVendors.Fields.Item("LineId").Value) - 1, oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString, "Closed")
                            End If
                        Else
                            objMain.objApplication.StatusBar.SetText("There are no records for the Vendor - " & oRsGetVendors.Fields.Item("Vendor").Value)

                        End If

                    End If
                    oRsGetVendors.MoveNext()
                Next
                'Else
                '    objMain.objApplication.StatusBar.SetText("There are no records with foredated Checks")
            End If

            '''''''''''''''''''''''''''''''''''''''''''--'-------------------------------GL Outgoing Payments-------------------------------------'''''''''''''''''''
            Dim GetAccount As String = "Select  T1.""U_CCODE"" ""Account"", T1.""LineId"", T1.""U_CQCUR"" From ""@RIPL_PPDC1"" T1 Inner Join OACT T0 On T1.""U_CCODE"" = T0.""AcctCode"" Where " & _
             "T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
             "And IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_OPDOC"",'')='' and IFNULL(T1.""U_IPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) and IFNULL(T1.""U_STATUS"",'')=''"
            Dim oRsGetAccount As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetAccount.DoQuery(GetAccount)
            If oRsGetAccount.RecordCount > 0 Then
                For i As Integer = 1 To oRsGetAccount.RecordCount
                    'If oRsGetAccount.Fields.Item("U_IPDOC").Value = "" Then
                    Dim GetAcctData As String = "Select T1.""U_CCODE"" ,T3.""BankCode"",T8.""U_TAXCODE"",T3.""GLAccount"",T8.""U_PAMNT"",T8.""U_ACCOUNT"",Cast(T1.""U_PDCDT"" As Date) ""PPDC Date"",T1.""U_CQCUR"",T8.""DocEntry"", " & _
                               "T8.""U_POACCT"",T8.""U_POACAMT"",T3.""Country"",T3.""Branch"",T3.""Account"", " & _
                               "T1.""U_CNUM"",T1.""U_CAMOUN"",IFNULL(T1.""U_DENTRY"",'') ""PPDC Entry"" " & _
                               "from ""@RIPL_OPPDC"" T0 " & _
                               "Inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry""  " & _
                               "Inner join ""@RIPL_OPDC"" T8 on T1.""U_DENTRY""=T8.""DocEntry"" " & _
                               "Inner Join OACT T2 On T1.""U_CCODE""=T2.""AcctCode"" " & _
                               "Left Join DSC1 T3 on T8.""U_BANK""=T3.""BankCode"" " & _
                              "Where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                               "And IFNULL(T1.""U_ACTV"",'N')='Y'  And T1.""U_CCODE"" = '" & oRsGetAccount.Fields.Item("Account").Value & "' And " & _
                               "T1.""LineId"" = '" & oRsGetAccount.Fields.Item("LineId").Value & "'  and " & _
                               "IFNULL(T1.""U_OPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) "
                    Dim OrsAcctData As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    OrsAcctData.DoQuery(GetAcctData)

                    If OrsAcctData.RecordCount > 0 Then
                        Dim oOutgoingPayments1 As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments)
                        oOutgoingPayments1.DocType = SAPbobsCOM.BoRcptTypes.rAccount
                        Dim DDate1 As Date = OrsAcctData.Fields.Item("PPDC Date").Value
                        Dim Val As String = objForm.Items.Item("9").Specific.Value
                        Dim PostDate As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                        oOutgoingPayments1.CardName = "PDC Payments"
                        oOutgoingPayments1.DocDate = PostDate
                        oOutgoingPayments1.Remarks = "PDC Payments"
                        oOutgoingPayments1.TaxDate = PostDate
                        oOutgoingPayments1.DocCurrency = OrsAcctData.Fields.Item("U_CQCUR").Value

                        'Dim GetTaxVal As String = "Select ""Rate"" From OVTG Where ""Code""='" & OrsAcctData.Fields.Item("U_TAXCODE").Value & "'"
                        'Dim OrsGetTaxVal As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        'OrsGetTaxVal.DoQuery(GetTaxVal)

                        ' Dim TaxAmnt As Double = OrsAcctData.Fields.Item("U_CAMOUN").Value * OrsGetTaxVal.Fields.Item("Rate").Value / 100
                        oOutgoingPayments1.AccountPayments.Decription = "PDC Outgoing Payments"
                        oOutgoingPayments1.AccountPayments.AccountCode = OrsAcctData.Fields.Item("U_CCODE").Value
                        oOutgoingPayments1.AccountPayments.VatGroup = OrsAcctData.Fields.Item("U_TAXCODE").Value
                        oOutgoingPayments1.AccountPayments.SumPaid = OrsAcctData.Fields.Item("U_CAMOUN").Value


                        oOutgoingPayments1.Checks.DueDate = DDate1
                        oOutgoingPayments1.Checks.CountryCode = OrsAcctData.Fields.Item("Country").Value
                        oOutgoingPayments1.Checks.BankCode = OrsAcctData.Fields.Item("BankCode").Value
                        oOutgoingPayments1.Checks.Branch = OrsAcctData.Fields.Item("Branch").Value
                        oOutgoingPayments1.Checks.CheckAccount = OrsAcctData.Fields.Item("GLAccount").Value
                        oOutgoingPayments1.Checks.AccounttNum = OrsAcctData.Fields.Item("Account").Value
                        oOutgoingPayments1.Checks.ManualCheck = SAPbobsCOM.BoYesNoEnum.tNO
                        oOutgoingPayments1.Checks.CheckNumber = OrsAcctData.Fields.Item("U_CNUM").Value
                        oOutgoingPayments1.Checks.CheckSum = CDbl(OrsAcctData.Fields.Item("U_PAMNT").Value)
                        oOutgoingPayments1.BankChargeAmount = 0
                        oOutgoingPayments1.Checks.Add()

                        If oOutgoingPayments1.Add <> 0 Then
                            objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription & " for Line - " & oRsGetAccount.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Else
                            Dim oapinv_docno_str1 As String = objMain.objCompany.GetNewObjectKey()
                            objMain.objApplication.StatusBar.SetText("Outgoing Payments For line No: " & oRsGetAccount.Fields.Item("LineId").Value & " Posted Successfully, SAP Record No - " & oapinv_docno_str1, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                            Dim GetIPNo1 As String = "Select ""DocNum"",""DocEntry"" From OVPM Where ""DocEntry"" = '" & oapinv_docno_str1 & "'"
                            Dim oRsGetIPNo1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetIPNo1.DoQuery(GetIPNo1)

                            Dim UpdatePDAInput1 As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='Closed',""U_OENTRY""='" & oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString & "'  Where ""DocEntry""='" & OrsAcctData.Fields.Item("PPDC Entry").Value & "'"
                            Dim oRsUpdatePDAInput1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdatePDAInput1.DoQuery(UpdatePDAInput1)

                            Dim UpdateOP As String = "Update OVPM set ""U_PDCENTRY""='" & OrsAcctData.Fields.Item("PPDC Entry").Value & "' where ""DocEntry""='" & oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString & "'"
                            Dim oRsUpdateOP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdateOP.DoQuery(UpdateOP)

                            Me.UpdateOutgoingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetAccount.Fields.Item("LineId").Value) - 1, oRsGetIPNo1.Fields.Item("DocEntry").Value.ToString, "Closed")

                        End If

                    End If
                    'End If
                    oRsGetAccount.MoveNext()
                Next
            End If


            Me.RefreshData(objForm.UniqueID)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.Status1Bar.SetText(ex.Message)
        End Try
    End Sub

    Sub PostIncomingPayments(ByVal FormUID As String, ByVal PPDCEntry As String, ByVal SysCur As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
            ObjMatrix = objForm.Items.Item("11").Specific

            objForm.Freeze(True)
            Dim GetCustomers As String = "Select  T1.""U_CCODE"" ""Customer"", T1.""LineId"",T1.""U_CQCUR"" From  " & _
                """@RIPL_PPDC1"" T1  " & _
                "Inner Join OCRD T0 On T1.""U_CCODE"" = T0.""CardCode"" Where " & _
                "T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' And T0.""CardType"" = 'C' " & _
                "And IFNULL(T1.""U_ACTV"",'N')='Y' and " & _
                "IFNULL(T1.""U_IPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) and IFNULL(T1.""U_STATUS"",'')=''"
            Dim oRsGetCustomers As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetCustomers.DoQuery(GetCustomers)

            If oRsGetCustomers.RecordCount > 0 Then
                For i As Integer = 1 To oRsGetCustomers.RecordCount
                    Dim GetData As String = "Select T1.""U_CCODE"" ""Customer"",Cast(T1.""U_PDCDT"" As Date) ""PPDC Date"",T1.""U_PONACT"",T4.""LineId"",T1.""U_CQCUR"",T8.""DocEntry"",T5.""GLAccount"", " & _
                               "T2.""Currency"" ""Currency"", IFNULL(T4.""U_SENTRY"",'') ""Inv No"",T4.""U_INVCUR"",T8.""U_PAMNT"",IFNULL(T8.""U_POACCT"",'N') ""U_POACCT"",T8.""U_POACAMT"", " & _
                               "T4.""U_DPAMNT"" ""Inv Amt"",T4.""U_PAMNT"" ""CQCInv Amt"",T5.""Branch"",T5.""Account"",IFNULL(T5.""Country"",'') ""COUNTRY"", " & _
                               "T2.""HouseBank"", T1.""U_CNUM"",T1.""U_CAMOUN"",IFNULL(T1.""U_DENTRY"",'') ""PPDC Entry"" " & _
                               "from ""@RIPL_OPPDC"" T0 " & _
                               "Inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry""  " & _
                               "Inner join ""@RIPL_PDC1"" T4 on T1.""U_DENTRY""=T4.""DocEntry"" " & _
                               "Inner join ""@RIPL_OPDC"" T8 on T4.""DocEntry""=T8.""DocEntry"" " & _
                               "Inner Join OCRD T2 On T1.""U_CCODE""=T2.""CardCode""  " & _
                               "Left Join DSC1 T5 On T2.""HouseBank"" = T5.""BankCode""  " & _
                               "Where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                               "And IFNULL(T1.""U_ACTV"",'N')='Y' And T1.""U_CCODE"" = '" & oRsGetCustomers.Fields.Item("Customer").Value & "' " & _
                               "And T1.""LineId"" = '" & oRsGetCustomers.Fields.Item("LineId").Value & "' and IFNULL(T4.""U_CHECK"",'N')='Y'  " & _
                               "And IFNULL(T1.""U_IPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) "
                    Dim oRsGetData As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsGetData.DoQuery(GetData)


                    Dim OIncomingPayments As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments)
                    OIncomingPayments.DocType = SAPbobsCOM.BoRcptTypes.rCustomer

                    If oRsGetData.RecordCount > 0 Then
                        objMain.objApplication.StatusBar.SetText("Incoming Payments for Line : " & oRsGetCustomers.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                        Dim PPDate As Date = oRsGetData.Fields.Item("PPDC Date").Value
                        Dim CCODE As String = oRsGetData.Fields.Item("Customer").Value
                        Dim Val As String = objForm.Items.Item("9").Specific.Value
                        Dim ValD As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                        OIncomingPayments.DocDate = ValD
                        OIncomingPayments.Remarks = "PDCPosted IncomingPayments"
                        OIncomingPayments.DocCurrency = oRsGetData.Fields.Item("U_CQCUR").Value
                        OIncomingPayments.TaxDate = ValD
                        OIncomingPayments.DueDate = ValD
                        OIncomingPayments.CardCode = CCODE

                        Dim Docrate As Double = OIncomingPayments.DocRate
                       
                        If oRsGetData.Fields.Item("Inv No").Value <> "" Then
                            For j As Integer = 1 To oRsGetData.RecordCount
                                If j > 1 Then
                                    OIncomingPayments.Invoices.Add()
                                End If
                                OIncomingPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_Invoice
                                OIncomingPayments.Invoices.DocEntry = oRsGetData.Fields.Item("Inv No").Value

                                Dim getLocalCurrency As String = "Select ""SysCurrncy"",""MainCurncy"" From OADM"
                                Dim oRsgetLocalCurrency As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsgetLocalCurrency.DoQuery(getLocalCurrency)

                                Dim GetExcRate As String = "Select IFNULL(""Rate"",1) ""Rate"",""Currency"" From ORTT Where " & _
                                 "TO_NVARCHAR(""RateDate"" , 'YYYYMMDD')='" & PPDate.ToString("yyyyMMdd") & "' " & _
                                 " And ""Currency""='" & oRsGetData.Fields.Item("U_CQCUR").Value & "'  "
                                Dim orsGetExcRate As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                orsGetExcRate.DoQuery(GetExcRate)

                                Dim CheqAmnt As Double = 0
                                Dim InvAmnt As Double = 0
                                If orsGetExcRate.RecordCount > 0 Then
                                    CheqAmnt = CDbl(oRsGetData.Fields.Item("CQCInv Amt").Value) / CDbl(orsGetExcRate.Fields.Item("Rate").Value)
                                Else
                                    CheqAmnt = CDbl(oRsGetData.Fields.Item("CQCInv Amt").Value)
                                End If

                                Dim GetERate As String = "Select ""Rate"",""Currency"" From ORTT Where " & _
                                       "TO_NVARCHAR(""RateDate"" , 'YYYYMMDD')='" & PPDate.ToString("yyyyMMdd") & "' " & _
                                       " And ""Currency""='" & oRsGetData.Fields.Item("U_INVCUR").Value & "'  "
                                Dim orsGetERate As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                orsGetERate.DoQuery(GetERate)

                                If oRsGetData.Fields.Item("U_CQCUR").Value = oRsGetData.Fields.Item("U_INVCUR").Value Then
                                    If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData.Fields.Item("U_CQCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData.Fields.Item("U_CQCUR").Value Then
                                        If orsGetExcRate.RecordCount > 0 Then
                                            InvAmnt = CheqAmnt * CDbl(orsGetExcRate.Fields.Item("Rate").Value)
                                        Else
                                            InvAmnt = CheqAmnt
                                        End If
                                        OIncomingPayments.Invoices.AppliedFC = InvAmnt
                                    Else
                                        InvAmnt = CheqAmnt
                                        OIncomingPayments.Invoices.SumApplied = InvAmnt
                                    End If
                                Else
                                    If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData.Fields.Item("U_CQCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData.Fields.Item("U_CQCUR").Value Then
                                        If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData.Fields.Item("U_INVCUR").Value And _
                                      oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData.Fields.Item("U_INVCUR").Value Then
                                            If orsGetERate.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            OIncomingPayments.Invoices.AppliedFC = InvAmnt
                                        Else
                                            If orsGetERate.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            OIncomingPayments.Invoices.SumApplied = InvAmnt
                                        End If
                                    Else
                                        If oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value <> oRsGetData.Fields.Item("U_INVCUR").Value And _
                                        oRsgetLocalCurrency.Fields.Item("MainCurncy").Value <> oRsGetData.Fields.Item("U_INVCUR").Value Then
                                            If orsGetERate.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            OIncomingPayments.Invoices.AppliedFC = InvAmnt
                                        Else
                                            If orsGetERate.RecordCount > 0 Then
                                                InvAmnt = CheqAmnt * CDbl(orsGetERate.Fields.Item("Rate").Value)
                                            Else
                                                InvAmnt = CheqAmnt
                                            End If
                                            OIncomingPayments.Invoices.SumApplied = InvAmnt
                                        End If
                                    End If
                                End If
                                oRsGetData.MoveNext()
                            Next
                        End If

                        oRsGetData.MoveFirst()
                        Dim TotalCheckSum As Double = CDbl(oRsGetData.Fields.Item("U_CAMOUN").Value)
                        OIncomingPayments.Checks.DueDate = PPDate
                        'Dim X As String = oRsGetData.Fields.Item("COUNTRY").Value
                        'Dim Y As String = oRsGetData.Fields.Item("BankCode").Value
                        'Dim X1 As String = oRsGetData.Fields.Item("Branch").Value
                        'Dim Y2 As String = oRsGetData.Fields.Item("GLAccount").Value
                        'Dim X3 As String = oRsGetData.Fields.Item("Account").Value
                        'OIncomingPayments.Checks.CountryCode = oRsGetData.Fields.Item("COUNTRY").Value
                        'OIncomingPayments.Checks.BankCode = oRsGetData.Fields.Item("BankCode").Value
                        'OIncomingPayments.Checks.Branch = oRsGetData.Fields.Item("Branch").Value
                        'OIncomingPayments.Checks.CheckAccount = oRsGetData.Fields.Item("GLAccount").Value
                        'OIncomingPayments.Checks.AccounttNum = oRsGetData.Fields.Item("Account").Value
                        OIncomingPayments.Checks.ManualCheck = SAPbobsCOM.BoYesNoEnum.tNO
                        OIncomingPayments.Checks.CheckNumber = oRsGetData.Fields.Item("U_CNUM").Value
                        OIncomingPayments.Checks.CheckSum = TotalCheckSum
                        OIncomingPayments.BankChargeAmount = 0
                        OIncomingPayments.Checks.Add()



                        If OIncomingPayments.Add <> 0 Then
                            objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription & " for Line - " & oRsGetCustomers.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Else
                            Dim oapinv_docno_str As String = objMain.objCompany.GetNewObjectKey()
                            objMain.objApplication.StatusBar.SetText("Incoming Payments for line No: " & oRsGetCustomers.Fields.Item("LineId").Value & " Posted Successfully, SAP Record No - " & oapinv_docno_str, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                            Dim GetIPNo As String = "Select ""DocNum"",""DocEntry"" From ORCT Where ""DocEntry"" = '" & oapinv_docno_str & "'"
                            Dim oRsGetIPNo As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetIPNo.DoQuery(GetIPNo)

                            Dim UpdatePDAInput As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='Closed',""U_IENTRY""='" & oRsGetIPNo.Fields.Item("DocEntry").Value.ToString & "'  Where ""DocEntry""='" & oRsGetData.Fields.Item("PPDC Entry").Value & "'"
                            Dim oRsUpdatePDAInput As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdatePDAInput.DoQuery(UpdatePDAInput)

                            Dim UpdateIP As String = "Update ORCT set ""U_PDCENTRY""='" & oRsGetData.Fields.Item("PPDC Entry").Value & "' where ""DocEntry""='" & oRsGetIPNo.Fields.Item("DocEntry").Value.ToString & "'"
                            Dim oRsUpdateIP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdateIP.DoQuery(UpdateIP)

                            '  Me.PDCInputDocIP(oRsGetData.Fields.Item("PPDC Entry").Value, oRsGetIPNo.Fields.Item("DocEntry").Value, "Closed")

                            Me.UpdateIncomingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetCustomers.Fields.Item("LineId").Value) - 1, oRsGetIPNo.Fields.Item("DocEntry").Value.ToString, "", "C")
                        End If
                    Else
                        '''''--------On Account Payments------
                        Dim GetPamntOnAcct As String = "Select Top 1 T1.""U_CCODE"" ""Customer"",Cast(T1.""U_PDCDT"" As Date) ""PPDC Date"",T1.""U_PONACT"",T4.""LineId"",T1.""U_CQCUR"",T8.""DocEntry"",T5.""GLAccount"", " & _
                              "T2.""Currency"" ""Currency"", IFNULL(T4.""U_SENTRY"",'') ""Inv No"",T4.""U_INVCUR"",T8.""U_PAMNT"",IFNULL(T8.""U_POACCT"",'N') ""U_POACCT"",T8.""U_POACAMT"", " & _
                              "T4.""U_DPAMNT"" ""Inv Amt"",T4.""U_PAMNT"" ""CQCInv Amt"",T5.""Branch"",T5.""Account"",IFNULL(T5.""Country"",'') ""COUNTRY"", " & _
                              "T2.""HouseBank"", T1.""U_CNUM"",T1.""U_CAMOUN"",IFNULL(T1.""U_DENTRY"",'') ""PPDC Entry"" " & _
                              "from ""@RIPL_OPPDC"" T0 " & _
                              "Inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry""  " & _
                              "Inner join ""@RIPL_PDC1"" T4 on T1.""U_DENTRY""=T4.""DocEntry"" " & _
                              "Inner join ""@RIPL_OPDC"" T8 on T4.""DocEntry""=T8.""DocEntry"" " & _
                              "Inner Join OCRD T2 On T1.""U_CCODE""=T2.""CardCode"" and T2.""CardType""='C'  " & _
                              "Left Join DSC1 T5 On T2.""BankCode"" = T5.""BankCode""  " & _
                              "Where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                              "And IFNULL(T1.""U_ACTV"",'N')='Y' And T1.""U_CCODE"" = '" & oRsGetCustomers.Fields.Item("Customer").Value & "' " & _
                              "And T1.""LineId"" = '" & oRsGetCustomers.Fields.Item("LineId").Value & "' and IFNULL(T4.""U_CHECK"",'N')='N'  " & _
                              "And IFNULL(T1.""U_IPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) "
                        Dim oRsGetPamntOnAcct As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsGetPamntOnAcct.DoQuery(GetPamntOnAcct)


                        If oRsGetPamntOnAcct.Fields.Item("U_POACCT").Value = "Y" And oRsGetPamntOnAcct.Fields.Item("U_POACAMT").Value <> 0 Then

                            objMain.objApplication.StatusBar.SetText("Incoming Payments for Line : " & oRsGetCustomers.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                            Dim PPDate As Date = oRsGetPamntOnAcct.Fields.Item("PPDC Date").Value
                            Dim Val As String = objForm.Items.Item("9").Specific.Value
                            Dim PostDate As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                            Dim CCODE As String = oRsGetPamntOnAcct.Fields.Item("Customer").Value
                            OIncomingPayments.DocDate = PostDate
                            OIncomingPayments.Remarks = "PDCPosted IncomingPayments"
                            OIncomingPayments.DocCurrency = oRsGetPamntOnAcct.Fields.Item("U_CQCUR").Value
                            OIncomingPayments.TaxDate = PostDate
                            OIncomingPayments.DueDate = PostDate
                            OIncomingPayments.CardCode = CCODE

                            Dim TotalCheckSum As Double = CDbl(oRsGetPamntOnAcct.Fields.Item("U_CAMOUN").Value)
                            OIncomingPayments.Checks.DueDate = PPDate
                            'Dim X As String = oRsGetPamntOnAcct.Fields.Item("COUNTRY").Value
                            'Dim Y As String = oRsGetPamntOnAcct.Fields.Item("BankCode").Value
                            'Dim X1 As String = oRsGetPamntOnAcct.Fields.Item("Branch").Value
                            'Dim Y2 As String = oRsGetPamntOnAcct.Fields.Item("GLAccount").Value
                            'Dim X3 As String = oRsGetPamntOnAcct.Fields.Item("Account").Value
                            'OIncomingPayments.Checks.CountryCode = oRsGetPamntOnAcct.Fields.Item("COUNTRY").Value
                            'OIncomingPayments.Checks.BankCode = oRsGetPamntOnAcct.Fields.Item("BankCode").Value
                            'OIncomingPayments.Checks.Branch = oRsGetPamntOnAcct.Fields.Item("Branch").Value
                            'OIncomingPayments.Checks.CheckAccount = oRsGetPamntOnAcct.Fields.Item("GLAccount").Value
                            'OIncomingPayments.Checks.AccounttNum = oRsGetPamntOnAcct.Fields.Item("Account").Value
                            OIncomingPayments.Checks.ManualCheck = SAPbobsCOM.BoYesNoEnum.tNO
                            OIncomingPayments.Checks.CheckNumber = oRsGetPamntOnAcct.Fields.Item("U_CNUM").Value
                            OIncomingPayments.Checks.CheckSum = TotalCheckSum
                            OIncomingPayments.BankChargeAmount = 0
                            OIncomingPayments.Checks.Add()
                            If OIncomingPayments.Add <> 0 Then
                                objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription & " for Line - " & oRsGetCustomers.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                            Else
                                Dim oapinv_docno_str As String = objMain.objCompany.GetNewObjectKey()
                                objMain.objApplication.StatusBar.SetText("Incoming Payments for line No: " & oRsGetCustomers.Fields.Item("LineId").Value & " Posted Successfully, SAP Record No - " & oapinv_docno_str, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                                Dim GetIPNo As String = "Select ""DocNum"",""DocEntry"" From ORCT Where ""DocEntry"" = '" & oapinv_docno_str & "'"
                                Dim oRsGetIPNo As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsGetIPNo.DoQuery(GetIPNo)

                                Dim UpdatePDAInput As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='Closed',""U_IENTRY""='" & oRsGetIPNo.Fields.Item("DocEntry").Value.ToString & "'  Where ""DocEntry""='" & oRsGetPamntOnAcct.Fields.Item("PPDC Entry").Value & "'"
                                Dim oRsUpdatePDAInput As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsUpdatePDAInput.DoQuery(UpdatePDAInput)

                                Dim UpdateIP As String = "Update ORCT set ""U_PDCENTRY""='" & oRsGetPamntOnAcct.Fields.Item("PPDC Entry").Value & "' where ""DocEntry""='" & oRsGetIPNo.Fields.Item("DocEntry").Value.ToString & "'"
                                Dim oRsUpdateIP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsUpdateIP.DoQuery(UpdateIP)

                                '  Me.PDCInputDocIP(oRsGetData.Fields.Item("PPDC Entry").Value, oRsGetIPNo.Fields.Item("DocEntry").Value, "Closed")

                                Me.UpdateIncomingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetCustomers.Fields.Item("LineId").Value) - 1, oRsGetIPNo.Fields.Item("DocEntry").Value.ToString, "", "C")
                            End If
                            'Else
                            '    objMain.objApplication.StatusBar.SetText("There are no records for the Customer - " & oRsGetCustomers.Fields.Item("Customer").Value)
                        End If

                        '''''''''''''''''''''''''',,,,,,,,,,,,,,,,,,'


                    End If

                    oRsGetCustomers.MoveNext()
                Next
                'Else
                '    objMain.objApplication.StatusBar.SetText("There are no records with foredated Checks")
            End If

            '''''''''''''''''''''''''''''''''''''''''''--'-------------------------------GL Incoming Payments-------------------------------------'''''''''''''''''''
            Dim GetAccount1 As String = "Select  T1.""U_CCODE"" ""Account"", T1.""LineId"",T1.""U_CQCUR"" From ""@RIPL_PPDC1"" T1 Inner Join OACT T0 On T1.""U_CCODE"" = T0.""AcctCode"" Where " & _
             "T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
             "And IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_IPDOC"",'')='' and IFNULL(T1.""U_OPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) and IFNULL(T1.""U_STATUS"",'')=''"
            Dim oRsGetAccount1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetAccount1.DoQuery(GetAccount1)
            If oRsGetAccount1.RecordCount > 0 Then
                For i As Integer = 1 To oRsGetAccount1.RecordCount
                    ' If oRsGetAccount.Fields.Item("U_OPDOC").Value = "" Then
                    Dim GetAcctData1 As String = "Select T1.""U_CCODE"" ,T3.""BankCode"",T8.""U_TAXCODE"",T3.""GLAccount"",T8.""U_PAMNT"",T8.""U_ACCOUNT"",Cast(T1.""U_PDCDT"" As Date) ""PPDC Date"",T1.""U_CQCUR"",T8.""DocEntry"", " & _
                               "T8.""U_POACCT"",T8.""U_POACAMT"",T3.""Country"",T3.""Branch"",T3.""Account"", " & _
                               "T1.""U_CNUM"",T1.""U_CAMOUN"",IFNULL(T1.""U_DENTRY"",'') ""PPDC Entry"" " & _
                               "from ""@RIPL_OPPDC"" T0 " & _
                               "Inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry""  " & _
                               "Inner join ""@RIPL_OPDC"" T8 on T1.""U_DENTRY""=T8.""DocEntry"" " & _
                               "Inner Join OACT T2 On T1.""U_CCODE""=T2.""AcctCode"" " & _
                               "Left Join DSC1 T3 on T8.""U_BANK""=T3.""BankCode"" " & _
                              "Where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                               "And IFNULL(T1.""U_ACTV"",'N')='Y'  And T1.""U_CCODE"" = '" & oRsGetAccount1.Fields.Item("Account").Value & "' And " & _
                               "T1.""LineId"" = '" & oRsGetAccount1.Fields.Item("LineId").Value & "'  and " & _
                               "IFNULL(T1.""U_OPDOC"",'')='' And Cast(T1.""U_PDCDT"" as Date) <= Cast(CURRENT_DATE as Date) "
                    Dim OrsAcctData1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    OrsAcctData1.DoQuery(GetAcctData1)

                    If OrsAcctData1.RecordCount > 0 Then
                        Dim oInComingPayments1 As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments)
                        oInComingPayments1.DocType = SAPbobsCOM.BoRcptTypes.rAccount
                        Dim DDate1 As Date = OrsAcctData1.Fields.Item("PPDC Date").Value
                        Dim Val As String = objForm.Items.Item("9").Specific.Value
                        Dim PostDate As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                        oInComingPayments1.CardName = "PDC Payments"
                        oInComingPayments1.DocDate = PostDate
                        oInComingPayments1.Remarks = "PDC Payments"
                        oInComingPayments1.TaxDate = PostDate
                        oInComingPayments1.DocCurrency = OrsAcctData1.Fields.Item("U_CQCUR").Value

                        'Dim GetTaxVal As String = "Select ""Rate"" From OVTG Where ""Code""='" & OrsAcctData.Fields.Item("U_TAXCODE").Value & "'"
                        'Dim OrsGetTaxVal As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        'OrsGetTaxVal.DoQuery(GetTaxVal)

                        ' Dim TaxAmnt As Double = OrsAcctData.Fields.Item("U_CAMOUN").Value * OrsGetTaxVal.Fields.Item("Rate").Value / 100

                        oInComingPayments1.AccountPayments.Decription = "PDC Account incomingPayments"
                        oInComingPayments1.AccountPayments.AccountCode = OrsAcctData1.Fields.Item("U_CCODE").Value
                        oInComingPayments1.AccountPayments.VatGroup = OrsAcctData1.Fields.Item("U_TAXCODE").Value
                        oInComingPayments1.AccountPayments.SumPaid = OrsAcctData1.Fields.Item("U_CAMOUN").Value


                        oInComingPayments1.Checks.DueDate = DDate1
                        'oInComingPayments1.Checks.CountryCode = OrsAcctData1.Fields.Item("Country").Value
                        'oInComingPayments1.Checks.BankCode = OrsAcctData1.Fields.Item("BankCode").Value
                        'oInComingPayments1.Checks.Branch = OrsAcctData1.Fields.Item("Branch").Value
                        'oInComingPayments1.Checks.CheckAccount = OrsAcctData1.Fields.Item("GLAccount").Value
                        'oInComingPayments1.Checks.AccounttNum = OrsAcctData1.Fields.Item("Account").Value
                        oInComingPayments1.Checks.ManualCheck = SAPbobsCOM.BoYesNoEnum.tNO
                        oInComingPayments1.Checks.CheckNumber = OrsAcctData1.Fields.Item("U_CNUM").Value
                        oInComingPayments1.Checks.CheckSum = CDbl(OrsAcctData1.Fields.Item("U_PAMNT").Value)
                        oInComingPayments1.BankChargeAmount = 0
                        oInComingPayments1.Checks.Add()

                        If oInComingPayments1.Add <> 0 Then
                            objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription & " for Line - " & oRsGetAccount1.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                        Else
                            Dim oapinv_docno_str2 As String = objMain.objCompany.GetNewObjectKey()
                            objMain.objApplication.StatusBar.SetText("Incoming Payments For line No: " & oRsGetAccount1.Fields.Item("LineId").Value & " Posted Successfully, SAP Record No - " & oapinv_docno_str2, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                            Dim GetIPNo2 As String = "Select ""DocNum"",""DocEntry"" From ORCT Where ""DocEntry"" = '" & oapinv_docno_str2 & "'"
                            Dim oRsGetIPNo2 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetIPNo2.DoQuery(GetIPNo2)

                            Dim UpdatePDAInput1 As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='Closed',""U_IENTRY""='" & oRsGetIPNo2.Fields.Item("DocEntry").Value.ToString & "'  Where ""DocEntry""='" & OrsAcctData1.Fields.Item("PPDC Entry").Value & "'"
                            Dim oRsUpdatePDAInput1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdatePDAInput1.DoQuery(UpdatePDAInput1)

                            Dim UpdateOP As String = "Update ORCT set ""U_PDCENTRY""='" & OrsAcctData1.Fields.Item("PPDC Entry").Value & "' where ""DocEntry""='" & oRsGetIPNo2.Fields.Item("DocEntry").Value.ToString & "'"
                            Dim oRsUpdateOP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsUpdateOP.DoQuery(UpdateOP)

                            Me.UpdateIncomingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetAccount1.Fields.Item("LineId").Value) - 1, oRsGetIPNo2.Fields.Item("DocEntry").Value.ToString, "Closed", "A")

                        End If

                    End If
                    ' End If
                    oRsGetAccount1.MoveNext()
                Next
            End If
            Me.RefreshData(objForm.UniqueID)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.Status1Bar.SetText(ex.Message)
        End Try
    End Sub

    Sub UpdateIncomingPaymentsNo(ByVal FormUID As String, ByVal DocEntry As String, ByVal LineID As Integer, ByVal IPNo As String, ByVal sts As String, ByVal FrmType As String)
        Try
            objForm.Freeze(True)
            objMain.sCmp = objMain.objCompany.GetCompanyService
            objMain.oGeneralService = objMain.sCmp.GetGeneralService("RIPL_OPPDC")

            objMain.oGeneralParams = objMain.oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
            objMain.oGeneralParams.SetProperty("DocEntry", DocEntry)
            objMain.oGeneralData = objMain.oGeneralService.GetByParams(objMain.oGeneralParams)

            objMain.oChildren = objMain.oGeneralData.Child("RIPL_PPDC1")
            If sts = "" Then
                objMain.oChildren.Item(LineID).SetProperty("U_IPDOC", IPNo.ToString)
            End If
            If FrmType = "A" Then
                objMain.oChildren.Item(LineID).SetProperty("U_IPDOC", IPNo.ToString)
            End If
            objMain.oChildren.Item(LineID).SetProperty("U_STATUS", sts.ToString)


            objMain.oGeneralService.Update(objMain.oGeneralData)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub UpdateOutgoingPaymentsNo(ByVal FormUID As String, ByVal DocEntry As String, ByVal LineID As Integer, ByVal IPNo As String, ByVal Sts As String)
        Try
            objForm.Freeze(True)
            objMain.sCmp = objMain.objCompany.GetCompanyService
            objMain.oGeneralService = objMain.sCmp.GetGeneralService("RIPL_OPPDC")

            objMain.oGeneralParams = objMain.oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
            objMain.oGeneralParams.SetProperty("DocEntry", DocEntry)
            objMain.oGeneralData = objMain.oGeneralService.GetByParams(objMain.oGeneralParams)

            objMain.oChildren = objMain.oGeneralData.Child("RIPL_PPDC1")
            If Sts = "Closed" Then
                objMain.oChildren.Item(LineID).SetProperty("U_OPDOC", IPNo.ToString)
            End If
            objMain.oChildren.Item(LineID).SetProperty("U_STATUS", Sts.ToString)

            objMain.oGeneralService.Update(objMain.oGeneralData)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub UpdateDepositNo(ByVal FormUID As String, ByVal DocEntry As String, ByVal LineID As Integer, ByVal DPNo As String, ByVal Sts As String)
        Try
            objForm.Freeze(True)
            objMain.sCmp = objMain.objCompany.GetCompanyService
            objMain.oGeneralService = objMain.sCmp.GetGeneralService("RIPL_OPPDC")

            objMain.oGeneralParams = objMain.oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
            objMain.oGeneralParams.SetProperty("DocEntry", DocEntry)
            objMain.oGeneralData = objMain.oGeneralService.GetByParams(objMain.oGeneralParams)

            objMain.oChildren = objMain.oGeneralData.Child("RIPL_PPDC1")
            If Sts = "Closed" Then
                objMain.oChildren.Item(LineID).SetProperty("U_DPST", DPNo.ToString)
            End If
            objMain.oChildren.Item(LineID).SetProperty("U_STATUS", Sts.ToString)

            objMain.oGeneralService.Update(objMain.oGeneralData)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType

                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    objForm = objMain.objApplication.Forms.Item(FormUID)

                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
                    ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
                    ObjMatrix = objForm.Items.Item("11").Specific

                    If pVal.ItemUID = "11" And pVal.ColUID = "V_8" And pVal.BeforeAction = True And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        objMain.objApplication.ActivateMenuItem("RIPL_OPDC")
                        ObjPDCInputForm = objMain.objApplication.Forms.GetForm("RIPL_PDC_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)
                        ObjPDCInputForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                        Dim getInputPDC As String = "Select T0.""DocNum"" From ""@RIPL_OPDC"" T0 " & _
                            " Where T0.""DocEntry""='" & ObjMatrix.Columns.Item("V_8").Cells.Item(pVal.Row).Specific.value & "' "
                        Dim OrsgetInputPDC As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        OrsgetInputPDC.DoQuery(getInputPDC)
                        ObjPDCInputForm.Items.Item("16").Specific.Value = OrsgetInputPDC.Fields.Item("DocNum").Value
                        ObjPDCInputForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                    End If

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    objForm = objMain.objApplication.Forms.Item(FormUID)

                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
                    ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
                    ObjMatrix = objForm.Items.Item("11").Specific

                    If pVal.ItemUID = "1" And pVal.BeforeAction = True And (pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        If Me.Validation(objForm.UniqueID) = False Then
                            BubbleEvent = False
                        End If
                    End If

                    If pVal.ItemUID = "1" And pVal.BeforeAction = False And pVal.ActionSuccess = True And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        Me.SetDefaultValues(objForm.UniqueID)
                    End If

                    If pVal.ItemUID = "17" And pVal.BeforeAction = False And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                        Me.DepositCheque(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0))
                        Me.RefreshData(objForm.UniqueID)
                    End If



                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT

                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
                    ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
                    ObjMatrix = objForm.Items.Item("11").Specific

                    If pVal.ItemUID = "1000001" And pVal.BeforeAction = False Then
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

                        Dim getLocalCurrency As String = "Select ""SysCurrncy"" From OADM"
                        Dim oRsgetLocalCurrency As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsgetLocalCurrency.DoQuery(getLocalCurrency)

                        oButtonCombo = objForm.Items.Item("1000001").Specific

                        If oButtonCombo.Selected.Value = "Incoming Payments" Then
                            Me.PostIncomingPayments(objForm.UniqueID, oDBs_Head.GetValue("DocEntry", 0), oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value.Trim)
                        ElseIf oButtonCombo.Selected.Value = "Outgoing Payments" Then
                            Me.PostOutgoingPayments(objForm.UniqueID, oDBs_Head.GetValue("DocEntry", 0), oRsgetLocalCurrency.Fields.Item("SysCurrncy").Value.Trim)
                        End If
                        oButtonCombo.Caption = "Post"
                        Me.RefreshData(objForm.UniqueID)
                    End If
                    If pVal.ItemUID = "15" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        If objForm.Items.Item("3").Specific.Value <> "" And objForm.Items.Item("5").Specific.Value <> "" Then
                            Me.LoadData(objForm.UniqueID)
                        End If
                    End If
                    If pVal.ItemUID = "20" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        oCombobox2 = objForm.Items.Item("20").Specific
                        If oCombobox2.Selected.Value <> "" Then
                            If objForm.Items.Item("18").Specific.Value <> "" Then
                                Dim getBPType As String = "Select ""CardType"" From OCRD Where ""CardCode""='" & oDBs_Head.GetValue("U_BPCODE", 0) & "'"
                                Dim OrsGetBPType As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                OrsGetBPType.DoQuery(getBPType)

                                Dim CheckSeries As String = "Select ""Series"" From NNM1 Where ""Remark"" = '" & OrsGetBPType.Fields.Item("CardType").Value & "' " & _
                                  "And ""Series"" = '" & objForm.Items.Item("20").Specific.Value.Trim & "' And ""ObjectCode"" = 'RIPL_OPPDC'"
                                Dim oRsCheckSeries As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsCheckSeries.DoQuery(CheckSeries)
                                oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OPPDC", oRsCheckSeries.Fields.Item("Series").Value))
                            Else
                                Dim CheckSeries As String = "Select ""Series"" From NNM1 Where ""Series"" = '" & oCombobox2.Selected.Value & "' " & _
                                  " And ""ObjectCode"" = 'RIPL_OPPDC'"
                                Dim oRsCheckSeries As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                oRsCheckSeries.DoQuery(CheckSeries)
                                oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OPPDC", oRsCheckSeries.Fields.Item("Series").Value))
                            End If
                        End If
                    End If


                    If pVal.ItemUID = "16" And pVal.BeforeAction = False Then
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE Then objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)

                        oButtonCombo1 = objForm.Items.Item("16").Specific

                        If oButtonCombo1.Selected.Value = "Cancel Incoming Payments" Then
                            Dim GetVal As String = "Select T1.""U_IPDOC"",T1.""DocEntry"",T1.""LineId"",IFNULL(T1.""U_DPST"",'') ""U_DPST"" from ""@RIPL_OPPDC"" T0 inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry"" Where " & _
                                              " T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0) & "' and IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_IPDOC"",'')<>'' "
                            Dim oRsGetVal As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetVal.DoQuery(GetVal)
                            If oRsGetVal.RecordCount > 0 Then
                                For m = 1 To oRsGetVal.RecordCount
                                    Me.CancelIPayments(objForm.UniqueID, oRsGetVal.Fields.Item("U_IPDOC").Value, oRsGetVal.Fields.Item("LineId").Value, "Cancelled")
                                    If oRsGetVal.Fields.Item("U_DPST").Value <> "" Then
                                        Me.CancelDeposit(objForm.UniqueID, oRsGetVal.Fields.Item("U_DPST").Value, oRsGetVal.Fields.Item("LineId").Value, "Cancelled")
                                    End If

                                    oRsGetVal.MoveNext()
                                Next
                            End If
                        ElseIf oButtonCombo1.Selected.Value = "Cancel Outgoing Payments" Then
                            Dim GetVal1 As String = "Select T1.""U_OPDOC"",T1.""DocEntry"",T1.""LineId"" from ""@RIPL_OPPDC"" T0 inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry"" Where " & _
                                             " T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0) & "' and IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_OPDOC"",'')<>'' "
                            Dim oRsGetVal1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetVal1.DoQuery(GetVal1)
                            If oRsGetVal1.RecordCount > 0 Then
                                For n = 1 To oRsGetVal1.RecordCount
                                    Me.CancelOPayments(objForm.UniqueID, oRsGetVal1.Fields.Item("U_OPDOC").Value, oRsGetVal1.Fields.Item("LineId").Value, "Cancelled")
                                    oRsGetVal1.MoveNext()
                                Next
                            End If
                        ElseIf oButtonCombo1.Selected.Value = "Bounced" Then
                            Dim GetValOP As String = "Select T1.""U_OPDOC"",T1.""DocEntry"",T1.""LineId"" from ""@RIPL_OPPDC"" T0 inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry"" Where " & _
                                             " T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0) & "' and IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_OPDOC"",'')<>'' "
                            Dim oRsGetValOP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetValOP.DoQuery(GetValOP)
                            If oRsGetValOP.RecordCount > 0 Then
                                For n = 1 To oRsGetValOP.RecordCount
                                    Me.CancelOPayments(objForm.UniqueID, oRsGetValOP.Fields.Item("U_OPDOC").Value, oRsGetValOP.Fields.Item("LineId").Value, "Bounced")
                                    oRsGetValOP.MoveNext()
                                Next
                            End If
                            Dim GetValIP As String = "Select T1.""U_IPDOC"",T1.""DocEntry"",T1.""LineId"",T1.""U_DPST"" from ""@RIPL_OPPDC"" T0 inner join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry"" Where " & _
                                             " T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0) & "' and IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_IPDOC"",'')<>'' "
                            Dim oRsGetValIP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            oRsGetValIP.DoQuery(GetValIP)
                            If oRsGetValIP.RecordCount > 0 Then
                                For n = 1 To oRsGetValIP.RecordCount
                                    Me.CancelIPayments(objForm.UniqueID, oRsGetValIP.Fields.Item("U_IPDOC").Value, oRsGetValIP.Fields.Item("LineId").Value, "Bounced")
                                    If oRsGetValIP.Fields.Item("U_DPST").Value <> "" Then
                                        Me.CancelDeposit(objForm.UniqueID, oRsGetValIP.Fields.Item("U_DPST").Value, oRsGetValIP.Fields.Item("LineId").Value, "Bounced")
                                    End If
                                    oRsGetValIP.MoveNext()
                                Next
                            End If

                        End If
                        oButtonCombo1.Caption = "Cancellations"
                        Me.RefreshData(objForm.UniqueID)
                    End If

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST


                        objForm = objMain.objApplication.Forms.Item(FormUID)
                        oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
                        ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
                        ObjMatrix = objForm.Items.Item("11").Specific

                        Dim oCFL As SAPbouiCOM.ChooseFromList
                        Dim CFLEvent As SAPbouiCOM.IChooseFromListEvent = pVal
                        Dim CFL_ID As String
                        CFL_ID = CFLEvent.ChooseFromListUID
                        oCFL = objForm.ChooseFromLists.Item(CFL_ID)
                        Dim oDT As SAPbouiCOM.DataTable
                        oDT = CFLEvent.SelectedObjects

                    If pVal.BeforeAction = True Then
                        If objForm.Items.Item("20").Specific.Value <> "" Then
                            If oCFL.UniqueID = "CFL_3" Then
                                Me.CFLFilter(objForm.UniqueID, oCFL.UniqueID)
                            End If
                        End If
                    End If

                    'objForm = objMain.objApplication.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount)

                    If (Not oDT Is Nothing) And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And pVal.BeforeAction = False Then
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE


                        If oCFL.UniqueID = "CFL_3" Then
                            oDBs_Head.SetValue("U_BPCODE", oDBs_Head.Offset, oDT.GetValue("CardCode", 0))
                            Me.LoadData(objForm.UniqueID)
                            If objForm.Items.Item("18").Specific.Value <> "" Then
                                oCombobox2 = objForm.Items.Item("20").Specific
                                Dim getBPType As String = "Select ""CardType"" From OCRD Where ""CardCode""='" & objForm.Items.Item("18").Specific.Value & "'"
                                Dim OrsGetBPType As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                                OrsGetBPType.DoQuery(getBPType)
                                If OrsGetBPType.Fields.Item("CardType").Value = "C" Then
                                    objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("20").Specific, "Select ""Series"", ""SeriesName"" From NNM1 Where ""ObjectCode"" = 'RIPL_OPPDC' And ""Indicator"" In " & _
                                      "(Select ""Indicator"" From OFPR Where ""PeriodStat"" = 'N') And ""Locked"" = 'N' And ""Remark"" = 'C'")
                                    oCombobox2 = objForm.Items.Item("20").Specific
                                    oCombobox2.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OPPDC"))
                                ElseIf OrsGetBPType.Fields.Item("CardType").Value = "S" Then
                                    objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("20").Specific, "Select ""Series"", ""SeriesName"" From NNM1 Where ""ObjectCode"" = 'RIPL_OPPDC' And ""Indicator"" In " & _
                                    "(Select ""Indicator"" From OFPR Where ""PeriodStat"" = 'N') And ""Locked"" = 'N' And ""Remark"" = 'S'")
                                    oCombobox2 = objForm.Items.Item("20").Specific
                                    oCombobox2.Select("", SAPbouiCOM.BoSearchKey.psk_ByValue)
                                    oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OPPDC"))
                                End If
                            End If
                        End If

                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                        objForm = objMain.objApplication.Forms.Item(FormUID)
                        oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
                        ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
                        ObjMatrix = objForm.Items.Item("11").Specific

                    If pVal.ItemUID = "3" And pVal.BeforeAction = False And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If objForm.Items.Item("3").Specific.Value <> "" And objForm.Items.Item("5").Specific.Value <> "" And objForm.Items.Item("15").Specific.Value <> "" Then
                            Me.LoadData(objForm.UniqueID)
                        End If
                    End If

                    If pVal.ItemUID = "5" And pVal.BeforeAction = False And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        If objForm.Items.Item("3").Specific.Value <> "" And objForm.Items.Item("5").Specific.Value <> "" And objForm.Items.Item("15").Specific.Value <> "" Then
                            Me.LoadData(objForm.UniqueID)
                        End If
                    End If

                    If pVal.ItemUID = "9" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        If objForm.Items.Item("9").Specific.Value <> "" Then
                            Dim ocomboboxCDate As SAPbouiCOM.ComboBox
                            ocomboboxCDate = objForm.Items.Item("20").Specific
                            Dim DateVal As String = objForm.Items.Item("9").Specific.Value
                            Dim Series As String = "Select T0.""Series"", T0.""SeriesName"" from NNM1 T0  " & _
                                "Inner join OFPR T1 on T0.""Indicator""=T1.""Indicator"" and T0.""ObjectCode"" = 'RIPL_OPPDC'" & _
                                                   "Where " & _
                                                   "'" & DateVal & "' Between T1.""F_RefDate"" and T1.""T_RefDate"""
                            Dim orsSeries As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            orsSeries.DoQuery(Series)
                            ocomboboxCDate.Select(orsSeries.Fields.Item("SeriesName").Value, SAPbouiCOM.BoSearchKey.psk_ByDescription)
                        End If
                    End If


            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText("Item Event Failed : " & ex.Message)
        Finally
        End Try
    End Sub

    Function Validation(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)

            If objForm.Items.Item("3").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Please enter From date")
                Return False
            ElseIf objForm.Items.Item("5").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Please enter To date")
                Return False
            ElseIf objForm.Items.Item("20").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("series cannot be left blank")
                Return False
            End If

            If ObjMatrix.VisualRowCount = 0 Then
                objMain.objApplication.StatusBar.SetText("Matrix cannot be Empty")
                Return False
            ElseIf ObjMatrix.VisualRowCount = 1 Then
                If ObjMatrix.Columns.Item("V_3").Cells.Item(1).Specific.Value.Trim = "" Then
                    objMain.objApplication.StatusBar.SetText("Matrix cannot be Empty")
                    Return False
                End If
            End If

            CHeckFlag = False
            For i As Integer = 1 To ObjMatrix.VisualRowCount
                Dim icheckbox As SAPbouiCOM.CheckBox
                icheckbox = ObjMatrix.Columns.Item("V_6").Cells.Item(i).Specific
                If icheckbox.Checked = True Then
                    If CHeckFlag = False Then
                        CHeckFlag = True
                    End If
                End If
            Next
            If CHeckFlag = False Then
                objMain.objApplication.StatusBar.SetText("You cannot add the document with zero row selection")
                Return False
            End If

            Return True
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Function

    Sub CancelIPayments(ByVal FormUID As String, ByVal DocEntry As String, ByVal LineId As String, ByVal Stts As String)
        Try

            Dim oPay As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments)
            Dim oRs As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            Dim s As String = "SELECT T0.""DocEntry"" FROM ORCT T0 Where " & _
                "T0.""DocEntry"" = '" & DocEntry & "'"
            oRs.DoQuery(s)

            Dim GetCustomers As String = "Select Distinct T1.""U_CCODE"" ""Customer"", T1.""LineId"",T1.""U_CQCUR"" From ""@RIPL_PPDC1"" T1 Inner Join OCRD T0 On T1.""U_CCODE"" = T0.""CardCode"" Where " & _
             "T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' And T0.""CardType"" = 'C' " & _
             "And IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_IPDOC"",'')<>''"
            Dim oRsGetCustomers As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetCustomers.DoQuery(GetCustomers)

            Do While Not oRs.EoF
                oPay = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oIncomingPayments)
                If oPay.GetByKey(oRs.Fields.Item(0).Value.ToString()) Then
                    If oPay.Cancel <> 0 Then
                        MsgBox(String.Format("{0}-{1}", objMain.objCompany.GetLastErrorCode, objMain.objCompany.GetLastErrorDescription))
                    End If
                    Dim datUpdate As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='" & Stts & "' Where ""U_IENTRY""='" & oRs.Fields.Item("DocEntry").Value & "'"
                    Dim oRsdatUpdate As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsdatUpdate.DoQuery(datUpdate)

                    Dim UpdateIP As String = "Update ORCT set ""U_STATUS""='" & Stts & "' Where ""DocEntry""='" & oRs.Fields.Item("DocEntry").Value & "'"
                    Dim oRsUpdateIP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsUpdateIP.DoQuery(UpdateIP)

                    Me.UpdateIncomingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetCustomers.Fields.Item("LineId").Value) - 1, "", Stts, "")
                Else
                    MsgBox("Payment not found")
                End If
                oRs.MoveNext()
            Loop
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            GC.Collect()
        End Try
    End Sub

    Sub CancelOPayments(ByVal FormUID As String, ByVal DocEntry As String, ByVal LineId As String, ByVal Stts As String)
        Try

            Dim oPay As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments)
            Dim oRs As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            Dim s As String = "SELECT T0.""DocEntry"" FROM OVPM T0 Where  " & _
                "T0.""DocEntry"" = '" & DocEntry & "'"
            oRs.DoQuery(s)

            Dim GetCustomers As String = "Select Distinct T1.""U_CCODE"" ""Customer"", T1.""LineId"",T1.""U_CQCUR"" From ""@RIPL_PPDC1"" T1 Inner Join OCRD T0 On T1.""U_CCODE"" = T0.""CardCode"" Where " & _
               "T1.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' And T0.""CardType"" = 'S' " & _
               "And IFNULL(T1.""U_ACTV"",'N')='Y' and IFNULL(T1.""U_OPDOC"",'')<>''"
            Dim oRsGetCustomers As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetCustomers.DoQuery(GetCustomers)

            Do While Not oRs.EoF
                oPay = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments)
                If oPay.GetByKey(oRs.Fields.Item(0).Value.ToString()) Then
                    If oPay.Cancel <> 0 Then
                        MsgBox(String.Format("{0}-{1}", objMain.objCompany.GetLastErrorCode, objMain.objCompany.GetLastErrorDescription))
                    End If
                    Dim datUpdate1 As String = "Update ""@RIPL_OPDC"" set ""U_STATUS""='" & Stts & "' Where ""U_OENTRY""='" & oRs.Fields.Item("DocEntry").Value & "'"
                    Dim oRsdatUpdate1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsdatUpdate1.DoQuery(datUpdate1)

                    Dim UpdateOP As String = "Update OVPM set ""U_STATUS""='" & Stts & "' Where ""DocEntry""='" & oRs.Fields.Item("DocEntry").Value & "'"
                    Dim oRsUpdateOP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsUpdateOP.DoQuery(UpdateOP)

                    Me.UpdateOutgoingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetCustomers.Fields.Item("LineId").Value) - 1, "", Stts)
                Else
                    MsgBox("Payment not found")
                End If

                oRs.MoveNext()
            Loop
        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            GC.Collect()
        End Try
    End Sub

    Sub CancelDeposit(ByVal FormUID As String, ByVal DPENtry As String, ByVal LineId As String, ByVal Stts As String)
        Try
            Dim oService As SAPbobsCOM.CompanyService = objMain.objCompany.GetCompanyService
            Dim dpService As SAPbobsCOM.DepositsService = oService.GetBusinessService(SAPbobsCOM.ServiceTypes.DepositsService)
            Dim dpsAddCash As SAPbobsCOM.Deposit = dpService.GetDataInterface(SAPbobsCOM.DepositsServiceDataInterfaces.dsDeposit)

            Dim oRs As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)

            Dim s As String = "SELECT T0.""DeposId"" FROM ODPS T0 Where  " & _
                "T0.""DeposType"" = 'K' and T0.""DeposId""= '" & DPENtry & "'"
            oRs.DoQuery(s)

            Do While Not oRs.EoF

                Dim dpsParamCancel As SAPbobsCOM.DepositParams = dpService.GetDataInterface(SAPbobsCOM.DepositsServiceDataInterfaces.dsDepositParams)
                Dim DpNo As Integer = dpsParamCancel.DepositNumber

                dpsParamCancel.AbsEntry = DPENtry
                dpService.CancelDeposit(dpsParamCancel)

                Dim UpdateIP As String = "Update ODPS set ""U_STATUS""='" & Stts & "' Where ""DeposId""='" & oRs.Fields.Item("DeposId").Value & "'"
                Dim oRsUpdateIP As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRsUpdateIP.DoQuery(UpdateIP)
                ' Me.UpdateIncomingPaymentsNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetCustomers.Fields.Item("LineId").Value) - 1, "", "Cancelled")
                oRs.MoveNext()
            Loop
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText("Cheque deposit cancellation failed: " & ex.Message)
        Finally
            GC.Collect()
        End Try
    End Sub

    Sub RefreshData(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
            ObjMatrix = objForm.Items.Item("11").Specific

            objForm.Freeze(True)
            objMain.objUtilities.RefreshDatasourceFromDB(FormUID, objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC"), "DocEntry", oDBs_Head.GetValue("DocEntry", 0))
            objMain.objUtilities.RefreshDatasourceFromDB(FormUID, objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1"), "DocEntry", ODBs_Details.GetValue("DocEntry", 0))
            ObjMatrix.LoadFromDataSource()
            objForm.Refresh()

            'Me.SetRowEditable(objForm.UniqueID)

            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub DepositCheque(ByVal FormId As String, ByVal DocEntry As String)
        Try

        objForm = objMain.objApplication.Forms.Item(FormId)
        oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OPPDC")
        ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_PPDC1")
        ObjMatrix = objForm.Items.Item("11").Specific


            Dim GetCqDetails As String = "Select  T0.""DocEntry"",T1.""U_CQCUR"",T1.""U_IPDOC"",T1.""U_ACCODE"",Cast(T1.""U_PDCDT"" As Date) ""PDCDATE"",T1.""U_CCODE"" ""U_CCODE"",(Select ""Currency"" From OCRD Where ""CardCode""=T1.""U_CCODE"" and ""CardType""='C')  ""Currency"", " & _
                              "T1.""U_CAMOUN"" ""U_CAMOUN"",T1.""LineId"",T1.""U_CNUM"" ""ChequeNum"",T1.""U_DENTRY"" From ""@RIPL_OPPDC"" T0 " & _
                            "Inner Join ""@RIPL_PPDC1"" T1 on T0.""DocEntry""=T1.""DocEntry"" " & _
                            "where T0.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0) & "' " & _
                            "and T1.""U_ACTV""='Y' and IFNULL( T1.""U_IPDOC"",'')<>'' and IFNULL(T1.""U_DPST"",'')='' and IFNULL(T1.""U_STATUS"",'')=''"
        Dim oRsGetCqDetails As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetCqDetails.DoQuery(GetCqDetails)



            If oRsGetCqDetails.RecordCount > 0 Then
                For i As Integer = 1 To oRsGetCqDetails.RecordCount

                    Dim oService As SAPbobsCOM.CompanyService = objMain.objCompany.GetCompanyService
                    Dim dpService As SAPbobsCOM.DepositsService = oService.GetBusinessService(SAPbobsCOM.ServiceTypes.DepositsService)
                    Dim dpsAddCash As SAPbobsCOM.Deposit = dpService.GetDataInterface(SAPbobsCOM.DepositsServiceDataInterfaces.dsDeposit)

                    Dim DepositDeatils As String = "Select Distinct ""CheckKey"" From OCHH Where ""CheckNum""='" & oRsGetCqDetails.Fields.Item("ChequeNum").Value & "' and ""RcptNum""='" & oRsGetCqDetails.Fields.Item("U_IPDOC").Value & "'"
                    Dim oRsDepositDeatils As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsDepositDeatils.DoQuery(DepositDeatils)

                    Dim chkLine As SAPbobsCOM.CheckLine


                    dpsAddCash.DepositType = SAPbobsCOM.BoDepositTypeEnum.dtChecks
                    dpsAddCash.DepositAccountType = SAPbobsCOM.BoDepositAccountTypeEnum.datBankAccount
                    dpsAddCash.DepositAccount = oRsGetCqDetails.Fields.Item("U_ACCODE").Value
                    dpsAddCash.CheckDepositType = SAPbobsCOM.BoCheckDepositTypeEnum.cdtCashChecks
                    dpsAddCash.DepositCurrency = oRsGetCqDetails.Fields.Item("U_CQCUR").Value
                    dpsAddCash.UserFields.Item("U_PDCENTRY").Value = oRsGetCqDetails.Fields.Item("U_DENTRY").Value
                    chkLine = dpsAddCash.Checks.Add
                    chkLine.CheckKey = oRsDepositDeatils.Fields.Item("CheckKey").Value


                    dpsAddCash.DepositDate = DateTime.Now
                    dpsAddCash.ReconcileAfterDeposit = SAPbobsCOM.BoYesNoEnum.tYES

                    Dim dpsParamAddCash As SAPbobsCOM.DepositParams = dpService.AddDeposit(dpsAddCash)
                    Dim oapinv_docno_str3 As String = dpsParamAddCash.AbsEntry
                    objMain.objApplication.StatusBar.SetText("Deposit  Posted Successfully, SAP Record No - " & oapinv_docno_str3, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)
                    Dim GetDepositNo As String = "Select ""DeposId"" From ODPS Where ""DeposId"" = '" & oapinv_docno_str3 & "'"
                    Dim oRsGetDepositNo As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsGetDepositNo.DoQuery(GetDepositNo)

                    Me.UpdateDepositNo(objForm.UniqueID, ODBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetCqDetails.Fields.Item("LineId").Value) - 1, oRsGetDepositNo.Fields.Item("DeposId").Value.ToString, "Closed")

                    oRsGetCqDetails.MoveNext()
                Next
            End If


        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText("Cheque deposit failed: " & ex.Message)
        End Try
    End Sub

#Region " Right Click Event"
    Public Sub RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
        Dim objForm As SAPbouiCOM.Form
        Dim oMenuItem As SAPbouiCOM.MenuItem
        Dim oMenus As SAPbouiCOM.Menus
        Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
        oCreationPackage = objMain.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
        objForm = objMain.objApplication.Forms.Item(eventInfo.FormUID)

        Try
            If eventInfo.FormUID = objForm.UniqueID Then
                If (eventInfo.BeforeAction = True) Then
                    If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_VIEW_MODE Then
                        ObjMatrix = objForm.Items.Item("11").Specific
                        If eventInfo.ItemUID = "11" And eventInfo.ColUID = "V_-1" And ObjMatrix.RowCount > 1 Then
                            Try
                                oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
                                oMenus = oMenuItem.SubMenus
                                If oMenus.Exists("Delete Row") = False Then
                                    oCreationPackage.UniqueID = "Delete Row"
                                    oCreationPackage.String = "Delete Row"
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If
                                If oMenus.Exists("Add Row") = False Then
                                    oCreationPackage.UniqueID = "Add Row"
                                    oCreationPackage.String = "Add Row"
                                    oCreationPackage.Enabled = True
                                    oMenus.AddEx(oCreationPackage)
                                End If
                            Catch ex As Exception
                                objMain.objApplication.StatusBar.SetText(ex.Message)
                            End Try
                        ElseIf eventInfo.ItemUID = "11" And ObjMatrix.RowCount <= 1 Then
                            oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
                            oMenus = oMenuItem.SubMenus
                            Try
                                If oMenus.Exists("Delete Row") = True Then
                                    objMain.objApplication.Menus.RemoveEx("Delete Row")
                                End If
                                If oMenus.Exists("Add Row") = True Then
                                    objMain.objApplication.Menus.RemoveEx("Add Row")
                                End If
                            Catch ex As Exception
                                objMain.objApplication.StatusBar.SetText(ex.Message)
                            End Try
                        End If
                        If eventInfo.ItemUID <> "11" Then
                            Try
                                oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
                                oMenus = oMenuItem.SubMenus
                                If oMenus.Exists("Delete Row") = True Then
                                    objMain.objApplication.Menus.RemoveEx("Delete Row")
                                End If
                                If oMenus.Exists("Add Row") = True Then
                                    objMain.objApplication.Menus.RemoveEx("Add Row")
                                End If
                            Catch ex As Exception
                                objMain.objApplication.StatusBar.SetText(ex.Message)
                            End Try
                        End If
                    End If
                Else
                    Try
                        oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
                        oMenus = oMenuItem.SubMenus
                        If oMenus.Exists("Delete Row") = True Then
                            objMain.objApplication.Menus.RemoveEx("Delete Row")
                        End If
                        If oMenus.Exists("Add Row") = True Then
                            objMain.objApplication.Menus.RemoveEx("Add Row")
                        End If
                    Catch ex As Exception
                        objMain.objApplication.StatusBar.SetText(ex.Message)
                    End Try
                End If
            End If

        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

    Sub DeleteUncheckRows(ByVal FormUID As String)

        Dim COunt As Integer
        COunt = ObjMatrix.VisualRowCount


        Dim GetMaxCode As String = "Select Max(""DocEntry"") ""MaxDE"" From ""@RIPL_OPPDC"" "
        Dim OrsGetMaxCode As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        OrsGetMaxCode.DoQuery(GetMaxCode)

        Dim getuncheckRows As String = "Select T1.""LineId"" From ""@RIPL_OPPDC"" T0 inner join ""@RIPL_PPDC1"" T1 on  " & _
            "T0.""DocEntry""=T1.""DocEntry"" Where T1.""U_ACTV""='N'  and T0.""DocEntry""='" & OrsGetMaxCode.Fields.Item("MaxDE").Value & "' and (IFNULL(T1.""U_OPDOC"",'')='' or  IFNULL(T1.""U_IPDOC"",'')='' )  order by T1.""LineId"" DESC "
        Dim OrsgetuncheckRows As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
        OrsgetuncheckRows.DoQuery(getuncheckRows)
        Dim RECORDS As Integer = OrsgetuncheckRows.RecordCount

        If OrsgetuncheckRows.RecordCount <> 0 Then
            For i As Integer = COunt To 1 Step -1
                If RECORDS > 0 Then
                    Dim Line As Integer = ObjMatrix.Columns.Item("V_-1").Cells.Item(i).Specific.Value
                    If ObjMatrix.Columns.Item("V_-1").Cells.Item(i).Specific.Value = OrsgetuncheckRows.Fields.Item("LineId").Value Then
                        ObjMatrix.Columns.Item("V_-1").Cells.Item(i).Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                        If ObjMatrix.IsRowSelected(i) = True Then
                            ObjMatrix.DeleteRow(i)
                            For j As Integer = 1 To ObjMatrix.VisualRowCount
                                ObjMatrix.Columns.Item("V_-1").Cells.Item(j).Specific.string = j
                            Next
                            objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                        End If
                        RECORDS = RECORDS - 1
                        OrsgetuncheckRows.MoveNext()
                    End If
                End If
            Next


        End If


    End Sub

    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            objForm = objMain.objApplication.Forms.GetForm("RIPL_OPPDC_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)
            objForm.Freeze(True)
            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD
                    If BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
                        ' Me.DeleteUncheckRows(objForm.UniqueID)
                    End If

                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
                        Dim CheckData As String = "Select  IFNULL(""U_IPDOC"",'') ""U_IPDOC"",IFNULL(""U_OPDOC"",'') ""U_OPDOC"" From ""@RIPL_PPDC1"" Where ""DocEntry"" = '" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " & _
                            "And IFNULL(""U_STATUS"",'')=''"
                        Dim oRsCheckData As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsCheckData.DoQuery(CheckData)

                        If oRsCheckData.RecordCount <> 0 Then
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE
                        Else
                            objForm.Mode = SAPbouiCOM.BoFormMode.fm_VIEW_MODE
                        End If
                        ObjMatrix.AutoResizeColumns()
                    End If

            End Select
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub CFLFilter(ByVal FormUID As String, ByVal CFL_ID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            Select Case CFL_ID

                Case "CFL_2"
                    Dim oConditions As SAPbouiCOM.Conditions
                    Dim oCondition As SAPbouiCOM.Condition
                    Dim oChooseFromList As SAPbouiCOM.ChooseFromList
                    Dim emptyCon As New SAPbouiCOM.Conditions
                    oChooseFromList = objMain.objApplication.Forms.Item(FormUID).ChooseFromLists.Item(CFL_ID)
                    oChooseFromList.SetConditions(emptyCon)
                    oConditions = oChooseFromList.GetConditions()

                    Dim GetBankAccount As String = "Select ""AcctCode"" From OACT Where ""Finanse""='Y' "
                    Dim oRsGetBankAccount As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsGetBankAccount.DoQuery(GetBankAccount)


                    If oRsGetBankAccount.RecordCount > 0 Then
                        For i As Integer = 1 To oRsGetBankAccount.RecordCount
                            If oConditions.Count > 0 Then oConditions.Item(oConditions.Count - 1).Relationship = SAPbouiCOM.BoConditionRelationship.cr_OR
                            oCondition = oConditions.Add()
                            oCondition.Alias = "AcctCode"
                            oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                            oCondition.CondVal = oRsGetBankAccount.Fields.Item("AcctCode").Value
                            oChooseFromList.SetConditions(oConditions)
                            oRsGetBankAccount.MoveNext()
                        Next
                    Else
                        oCondition = oConditions.Add()
                        oCondition.Alias = "AcctCode"
                        oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                        oCondition.CondVal = ""
                    End If
                    oChooseFromList.SetConditions(oConditions)

                Case "CFL_3"
                    Dim oConditions As SAPbouiCOM.Conditions
                    Dim oCondition As SAPbouiCOM.Condition
                    Dim oChooseFromList As SAPbouiCOM.ChooseFromList
                    Dim emptyCon As New SAPbouiCOM.Conditions
                    oChooseFromList = objMain.objApplication.Forms.Item(FormUID).ChooseFromLists.Item(CFL_ID)
                    oChooseFromList.SetConditions(emptyCon)
                    oConditions = oChooseFromList.GetConditions()

                    Dim CheckSeries As String = "Select ""Series"" ,""Remark"" From NNM1 Where " & _
                              " ""Series"" = '" & objForm.Items.Item("20").Specific.Value.Trim & "' And ""ObjectCode"" = 'RIPL_OPPDC'"
                    Dim oRsCheckSeries As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsCheckSeries.DoQuery(CheckSeries)

                    Dim CheckBPs As String = "Select ""CardCode""  From OCRD Where " & _
                              " ""CardType"" = '" & oRsCheckSeries.Fields.Item("Remark").Value & "'"
                    Dim oRsCheckBPs As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsCheckBPs.DoQuery(CheckBPs)

                    If oRsCheckBPs.RecordCount > 0 Then
                        For i As Integer = 1 To oRsCheckBPs.RecordCount
                            If oConditions.Count > 0 Then oConditions.Item(oConditions.Count - 1).Relationship = SAPbouiCOM.BoConditionRelationship.cr_OR
                            oCondition = oConditions.Add()
                            oCondition.Alias = "CardCode"
                            oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                            oCondition.CondVal = oRsCheckBPs.Fields.Item("CardCode").Value
                            oChooseFromList.SetConditions(oConditions)
                            oRsCheckBPs.MoveNext()
                        Next
                    End If


            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

End Class
