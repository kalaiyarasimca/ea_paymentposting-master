﻿Public Class clsPostingForm
    Dim objForm As SAPbouiCOM.Form
    Dim ODBs_Details, oDBs_Head As SAPbouiCOM.DBDataSource
    Dim ObjMatrix As SAPbouiCOM.Matrix
    Dim ObjCheckBox As SAPbouiCOM.CheckBox
    Sub LoadForm(ByVal BaseForm As String, ByVal syscurrn As String, ByVal DocEnt As String, ByVal BaseLine As Integer, ByVal Val As String, ByVal BPCODE As String, ByVal BPNAME As String, ByVal CheqNum As String, ByVal ChqAmt As Double, ByVal CQTYPE As String, ByVal CqCurrency As String)
        'Sub LoadForm(ByVal BaseForm As String, ByVal DocEnt As String, ByVal BaseLine As Integer, ByVal BPCODE As String)
        Try
            objMain.objUtilities.LoadForm("InvoicePostingForm.xml", "RIPL_INVP_FORM", ResourceType.Embeded)
            objForm = objMain.objApplication.Forms.GetForm("RIPL_INVP_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)
            objForm.Freeze(True)
            objForm.DataBrowser.BrowseBy = "4"
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OINP")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_INVP")
            ObjMatrix = objForm.Items.Item("1000001").Specific

            ObjMatrix.CommonSetting.EnableArrowKey = True

            If RIPL_PDC_POSTING.MainCls.ohtLookUpForm.ContainsKey(objForm.UniqueID) = False Then
                RIPL_PDC_POSTING.MainCls.ohtLookUpForm.Add(objForm.UniqueID, BaseForm)
            End If


            objForm.Items.Item("4").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("6").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("8").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("10").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("12").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("14").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("17").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("19").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("22").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("25").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("1000002").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("26").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)

            objForm.Items.Item("4").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("6").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("8").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("10").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("12").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("14").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("17").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("19").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("22").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("25").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("1000002").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("26").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            Dim GetRecords As String = "Select T0.""DocEntry"" from ""@RIPL_OINP"" T0 " & _
                                    "inner join ""@RIPL_INVP"" T1 on T0.""DocEntry""=T1.""DocEntry"" " & _
                                    "Where T0.""U_BLID""= '" & BaseLine & "' And T0.""U_PPDCE""='" & DocEnt & "'"
            Dim ORSGetRecords As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            ORSGetRecords.DoQuery(GetRecords)


            If ORSGetRecords.RecordCount = 0 Then

                If Val = "C" Then

                    Dim GetInvoices As String = "Select ""DocNum""," & _
                                                """DocEntry"",""DocCur"",""DocTotal"",Cast(""DocDate"" as Date) as ""DDate"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                                """DocTotalFC"",(""DocTotalFC""-""PaidFC"") ""BalanceFC"" " & _
                                                "From OINV Where ""DocStatus""='O' And ""CardCode""='" & BPCODE & "'"
                    Dim oRsGetInvoices As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsGetInvoices.DoQuery(GetInvoices)



                    oDBs_Head.SetValue("U_BPCODE", oDBs_Head.Offset, BPCODE)
                    oDBs_Head.SetValue("U_BPNAME", oDBs_Head.Offset, BPNAME)
                    oDBs_Head.SetValue("U_CQNUM", oDBs_Head.Offset, CheqNum)
                    oDBs_Head.SetValue("U_CQAMT", oDBs_Head.Offset, ChqAmt)
                    oDBs_Head.SetValue("U_CQCR", oDBs_Head.Offset, CqCurrency)
                    oDBs_Head.SetValue("U_PPDCE", oDBs_Head.Offset, DocEnt)
                    oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OINVP"))
                    oDBs_Head.SetValue("U_BLID", oDBs_Head.Offset, BaseLine)
                    oDBs_Head.SetValue("U_INVTT", oDBs_Head.Offset, 0)
                    oDBs_Head.SetValue("U_PTTL", oDBs_Head.Offset, 0)
                    'oDBs_Head.SetValue("U_LCUR", oDBs_Head.Offset, "")
                    'oDBs_Head.SetValue("U_PCUR", oDBs_Head.Offset, "")

                    For i As Integer = 1 To oRsGetInvoices.RecordCount
                        ObjMatrix.AddRow()
                        ODBs_Details.SetValue("LineId", ODBs_Details.Offset, i)
                        ODBs_Details.SetValue("U_CHECK", ODBs_Details.Offset, "N")
                        ODBs_Details.SetValue("U_SIENT", ODBs_Details.Offset, oRsGetInvoices.Fields.Item("DocEntry").Value)
                        ODBs_Details.SetValue("U_SIDN", ODBs_Details.Offset, oRsGetInvoices.Fields.Item("DocNum").Value)
                        ODBs_Details.SetValue("U_INVCR", ODBs_Details.Offset, oRsGetInvoices.Fields.Item("DocCur").Value)

                        Dim GetExchangeRates As String = "Select " & _
                          "Case T0.""Currency"" When  (Select ""SysCurrncy"" From OADM) Then '0' " & _
                           "Else T0.""Rate"" " & _
                           "End As Rate From ORTT T0 " & _
                           "Where TO_NVARCHAR(""RateDate"",'YYYYMMDD')= TO_NVARCHAR(Current_Date,'YYYYMMDD') and T0.""Currency""='" & oRsGetInvoices.Fields.Item("DocCur").Value & "'"
                        Dim oRsGetExchangeRates As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsGetExchangeRates.DoQuery(GetExchangeRates)

                        ODBs_Details.SetValue("U_RATE", ODBs_Details.Offset, oRsGetExchangeRates.Fields.Item("Rate").Value)
                        ODBs_Details.SetValue("U_PINEN", ODBs_Details.Offset, "")
                        ODBs_Details.SetValue("U_PDNUM", ODBs_Details.Offset, "")
                        ODBs_Details.SetValue("U_IAMNT", ODBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("DocTotal").Value))
                        ODBs_Details.SetValue("U_INVBAL", ODBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("Balance").Value))

                        ODBs_Details.SetValue("U_DTFC", ODBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("DocTotalFC").Value))
                        ODBs_Details.SetValue("U_BALFC", ODBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("BalanceFC").Value))

                        If syscurrn = oRsGetInvoices.Fields.Item("DocCur").Value Then
                            ODBs_Details.SetValue("U_PAMT", ODBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("Balance").Value))
                        Else
                            ODBs_Details.SetValue("U_PAMT", ODBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("BalanceFC").Value))
                        End If

                        Dim Ddate As Date = oRsGetInvoices.Fields.Item("DDate").Value
                        ODBs_Details.SetValue("U_IDATE", ODBs_Details.Offset, Ddate.ToString("yyyyMMdd"))
                        ODBs_Details.SetValue("U_CTYPE", ODBs_Details.Offset, CQTYPE)
                        ObjMatrix.SetLineData(ObjMatrix.VisualRowCount)
                        oRsGetInvoices.MoveNext()
                    Next

                ElseIf Val = "S" Then

                    Dim GetInvoices1 As String = "Select ""DocNum"", " & _
                                                 """DocEntry"",""DocCur"",""DocTotal"",Cast(""DocDate"" as Date) as ""DDate"",(""DocTotal""-""PaidSum"") ""Balance"",""DocTotalFC"",(""DocTotalFC""-""PaidFC"") ""BalanceFC""  " & _
                                                 "From OPCH Where ""DocStatus""='O' And ""CardCode""='" & BPCODE & "'"
                    Dim oRsGetInvoices1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    oRsGetInvoices1.DoQuery(GetInvoices1)


                    oDBs_Head.SetValue("U_BPCODE", oDBs_Head.Offset, BPCODE)
                    oDBs_Head.SetValue("U_BPNAME", oDBs_Head.Offset, BPNAME)
                    oDBs_Head.SetValue("U_CQNUM", oDBs_Head.Offset, CheqNum)
                    oDBs_Head.SetValue("U_CQAMT", oDBs_Head.Offset, ChqAmt)
                    oDBs_Head.SetValue("U_CQCR", oDBs_Head.Offset, CqCurrency)
                    oDBs_Head.SetValue("U_PPDCE", oDBs_Head.Offset, DocEnt)
                    oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "RIPL_OINVP"))
                    oDBs_Head.SetValue("U_BLID", oDBs_Head.Offset, BaseLine)
                    oDBs_Head.SetValue("U_INVTT", oDBs_Head.Offset, 0)
                    oDBs_Head.SetValue("U_PTTL", oDBs_Head.Offset, 0)
                    'oDBs_Head.SetValue("U_LCUR", oDBs_Head.Offset, "")
                    'oDBs_Head.SetValue("U_PCUR", oDBs_Head.Offset, "")


                    For l As Integer = 1 To oRsGetInvoices1.RecordCount
                        ObjMatrix.AddRow()
                        ODBs_Details.SetValue("LineId", ODBs_Details.Offset, l)
                        ODBs_Details.SetValue("U_CHECK", ODBs_Details.Offset, "N")
                        ODBs_Details.SetValue("U_SIENT", ODBs_Details.Offset, "") 'U_PDNUM
                        ODBs_Details.SetValue("U_SIDN", ODBs_Details.Offset, "")
                        ODBs_Details.SetValue("U_INVCR", ODBs_Details.Offset, oRsGetInvoices1.Fields.Item("DocCur").Value)

                        Dim GetExchangeRates As String = "Select " & _
                        "Case T0.""Currency"" When  (Select ""SysCurrncy"" From OADM) Then '0' " & _
                         "Else T0.""Rate"" " & _
                         "End As Rate From ORTT T0 " & _
                         "Where TO_NVARCHAR(""RateDate"",'YYYYMMDD')= TO_NVARCHAR(Current_Date,'YYYYMMDD') and T0.""Currency""='" & oRsGetInvoices1.Fields.Item("DocCur").Value & "'"
                        Dim oRsGetExchangeRates As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsGetExchangeRates.DoQuery(GetExchangeRates)

                        ODBs_Details.SetValue("U_RATE", ODBs_Details.Offset, oRsGetExchangeRates.Fields.Item("Rate").Value)
                        ODBs_Details.SetValue("U_PINEN", ODBs_Details.Offset, oRsGetInvoices1.Fields.Item("DocEntry").Value)
                        ODBs_Details.SetValue("U_PDNUM", ODBs_Details.Offset, oRsGetInvoices1.Fields.Item("DocNum").Value)
                        ODBs_Details.SetValue("U_IAMNT", ODBs_Details.Offset, oRsGetInvoices1.Fields.Item("DocTotal").Value)
                        ODBs_Details.SetValue("U_INVBAL", ODBs_Details.Offset, CDbl(oRsGetInvoices1.Fields.Item("Balance").Value))
                        ODBs_Details.SetValue("U_DTFC", ODBs_Details.Offset, CDbl(oRsGetInvoices1.Fields.Item("DocTotalFC").Value))
                        ODBs_Details.SetValue("U_BALFC", ODBs_Details.Offset, CDbl(oRsGetInvoices1.Fields.Item("BalanceFC").Value))


                        If syscurrn = oRsGetInvoices1.Fields.Item("DocCur").Value Then
                            ODBs_Details.SetValue("U_PAMT", ODBs_Details.Offset, CDbl(oRsGetInvoices1.Fields.Item("Balance").Value))
                        Else
                            ODBs_Details.SetValue("U_PAMT", ODBs_Details.Offset, CDbl(oRsGetInvoices1.Fields.Item("BalanceFC").Value))
                        End If
                        '   ODBs_Details.SetValue("U_PAMT", ODBs_Details.Offset, CDbl(oRsGetInvoices1.Fields.Item("DocTotal").Value))
                        Dim Ddate As Date = oRsGetInvoices1.Fields.Item("DDate").Value
                        ODBs_Details.SetValue("U_IDATE", ODBs_Details.Offset, Ddate.ToString("yyyyMMdd"))
                        ODBs_Details.SetValue("U_CTYPE", ODBs_Details.Offset, CQTYPE)
                        ObjMatrix.SetLineData(ObjMatrix.VisualRowCount)
                        oRsGetInvoices1.MoveNext()
                    Next
                    ObjMatrix.AutoResizeColumns()
                End If
            Else

                objForm.Mode = SAPbouiCOM.BoFormMode.fm_FIND_MODE
                objForm.Items.Item("6").Specific.Value = BPCODE
                objForm.Items.Item("14").Specific.Value = DocEnt
                objForm.Items.Item("17").Specific.Value = BaseLine
                objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                ObjMatrix.AutoResizeColumns()

            End If


            objForm.Freeze(False)

        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText("Load Break Down Failed")
        Finally
        End Try
    End Sub
    Sub SetDefaultValues(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)

            oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OINP")
            ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_INVP")
            ObjMatrix = objForm.Items.Item("1000001").Specific

            ObjMatrix.Clear()
            ODBs_Details.Clear()
            ObjMatrix.FlushToDataSource()
            'Me.SetNewLine(objForm.UniqueID, "1000001")
            ' objMatrix.AutoResizeColumns()

        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
    ''Sub SetNewLine(ByVal FormUID As String, ByVal MatrixUID As String)
    '    Try
    '        objForm = objMain.objApplication.Forms.Item(FormUID)

    '        oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OINP")
    '        ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_INVP")
    '        ObjMatrix = objForm.Items.Item("1000001").Specific

    '        Select Case MatrixUID
    '            Case "1000001"
    '                ObjMatrix.AddRow()
    '                ODBs_Details.SetValue("LineId", ODBs_Details.Offset, ObjMatrix.VisualRowCount)
    '                ODBs_Details.SetValue("U_CHECK", ODBs_Details.Offset, "N")
    '                ODBs_Details.SetValue("U_SIENT", ODBs_Details.Offset, "")
    '                ODBs_Details.SetValue("U_SIDN", ODBs_Details.Offset, "")
    '                ODBs_Details.SetValue("U_IAMNT", ODBs_Details.Offset, 0)
    '                ODBs_Details.SetValue("U_CTYPE", ODBs_Details.Offset, "")
    '                ObjMatrix.SetLineData(ObjMatrix.VisualRowCount)
    '        End Select
    '    Catch ex As Exception
    '        objMain.objApplication.StatusBar.SetText(ex.Message)
    '    End Try
    'End Sub
    'Sub MenuEvent(ByRef pVAl As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
    '    Try
    '        If pVAl.MenuUID = "Delete Row" And pVAl.BeforeAction = False Then
    '            ObjMatrix = objForm.Items.Item("1000001").Specific
    '            For i As Integer = 1 To objMatrix.VisualRowCount - 1
    '                If objMatrix.IsRowSelected(i) = True Then
    '                    objMatrix.DeleteRow(i)
    '                End If
    '            Next
    '            For i As Integer = 1 To objMatrix.VisualRowCount
    '                objMatrix.Columns.Item("V_-1").Cells.Item(i).Specific.string = i
    '            Next
    '            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
    '        End If
    '    Catch ex As Exception
    '        objMain.objApplication.StatusBar.SetText(ex.Message)
    '    End Try
    'End Sub
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    objForm = objMain.objApplication.Forms.Item(FormUID)

                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OINP")
                    ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_INVP")
                    ObjMatrix = objForm.Items.Item("1000001").Specific

                    If pVal.ItemUID = "1" And pVal.BeforeAction = True And (pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        If Me.Validation(objForm.UniqueID) = False Then
                            BubbleEvent = False
                        End If
                    End If
                    If pVal.ItemUID = "1" And pVal.BeforeAction = False And pVal.ActionSuccess = True And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        objForm.Close()
                    End If

                    If pVal.ItemUID = "1000001" And pVal.ColUID = "V_6" And pVal.BeforeAction = False Then
                        Dim Amount As Double = 0.0
                        Dim PayAmount As Double = 0.0
                        objForm.Items.Item("19").Specific.Value = 0.0
                        objForm.Items.Item("22").Specific.Value = 0.0
                        For i As Integer = 1 To ObjMatrix.VisualRowCount
                            ObjCheckBox = ObjMatrix.Columns.Item("V_6").Cells.Item(i).Specific
                            If ObjMatrix.Columns.Item("V_1").Cells.Item(i).Specific.Value <> 0 And ObjCheckBox.Checked = True Then
                                Amount = Amount + CDbl(ObjMatrix.Columns.Item("V_1").Cells.Item(i).Specific.Value)
                                objForm.Items.Item("19").Specific.Value = Amount
                                PayAmount = PayAmount + CDbl(ObjMatrix.Columns.Item("V_7").Cells.Item(i).Specific.Value)
                                objForm.Items.Item("22").Specific.Value = PayAmount
                            End If
                        Next
                        ' objForm.Items.Item("1000002").Specific.Value = ObjMatrix.Columns.Item("V_9").Cells.Item(pVal.Row).Specific.Value
                    End If
                Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                    objForm = objMain.objApplication.Forms.Item(FormUID)

                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@RIPL_OINP")
                    ODBs_Details = objForm.DataSources.DBDataSources.Item("@RIPL_INVP")
                    ObjMatrix = objForm.Items.Item("1000001").Specific

                    If pVal.ItemUID = "1000001" And pVal.ColUID = "V_7" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Dim PYamt As Double = 0.0
                        For Aa As Integer = 1 To ObjMatrix.VisualRowCount
                            ObjCheckBox = ObjMatrix.Columns.Item("V_6").Cells.Item(Aa).Specific
                            If ObjCheckBox.Checked = True And ObjMatrix.Columns.Item("V_7").Cells.Item(Aa).Specific.Value <> 0.0 Then
                                PYamt = PYamt + CDbl(ObjMatrix.Columns.Item("V_7").Cells.Item(Aa).Specific.Value)
                                objForm.Items.Item("22").Specific.Value = PYamt
                            End If
                        Next
                        '  objForm.Items.Item("1000002").Specific.Value = ObjMatrix.Columns.Item("V_9").Cells.Item(pVal.Row).Specific.Value
                        If CDbl(ObjMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.Value) < CDbl(ObjMatrix.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.Value) Then
                            ObjMatrix.Columns.Item("V_7").Cells.Item(pVal.Row).Specific.Value = CDbl(ObjMatrix.Columns.Item("V_1").Cells.Item(pVal.Row).Specific.Value)
                        End If
                    End If



                    'Case SAPbouiCOM.BoEventTypes.et_FORM_DEACTIVATE
                    '    objForm = objMain.objApplication.Forms.Item(FormUID)

                    '    Dim oMenuItem As SAPbouiCOM.MenuItem
                    '    Dim oMenus As SAPbouiCOM.Menus
                    '    oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
                    '    oMenus = oMenuItem.SubMenus
                    '    Try
                    '        If oMenus.Exists("Delete Row") = True Then
                    '            objMain.objApplication.Menus.RemoveEx("Delete Row")
                    '        End If
                    '    Catch ex As Exception
                    '        objMain.objApplication.StatusBar.SetText(ex.Message)
                    '    End Try

                    'Case SAPbouiCOM.BoEventTypes.et_FORM_CLOSE
                    '    objForm = objMain.objApplication.Forms.Item(FormUID)

                    '    Dim oMenuItem As SAPbouiCOM.MenuItem
                    '    Dim oMenus As SAPbouiCOM.Menus
                    '    oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
                    '    oMenus = oMenuItem.SubMenus
                    '    Try
                    '        If oMenus.Exists("Delete Row") = True Then
                    '            objMain.objApplication.Menus.RemoveEx("Delete Row")
                    '        End If
                    '    Catch ex As Exception
                    '        objMain.objApplication.StatusBar.SetText(ex.Message)
                    '    End Try




                    '                    If pVal.ItemUID = "12" And pVal.BeforeAction = False And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                    '                        Me.LoadData(objForm.UniqueID)
                    '                    End If

                    '                    If pVal.ItemUID = "11000001" And pVal.BeforeAction = False And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                    '                        Dim GetDatA1 As String = "Select  T1.U_CNUM ,T1.LineId 'LineID',T0.DocNum 'DN',Isnull(T1.U_IPDOC,'') 'IP NO' " & _
                    '                           " from dbo.[@RIPL_OPPDC] T0 inner join dbo.[@RIPL_PPDC1] T1 on T0.DocENtry=T1.DocEntry " & _
                    '"                         Where Isnull(T1.U_ACTV,'')='Y' And Isnull(T1.U_IPDOC,'')='' and T0.DocEntry='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "'"
                    '                        Dim oRsGetDatA1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                    '                        oRsGetDatA1.DoQuery(GetDatA1)

                    '                        If oRsGetDatA1.RecordCount > 0 Then
                    '                            Me.PostIncomingPayments(objForm.UniqueID)
                    '                        End If
                    '                    End If
            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText("Item Event Failed : " & ex.Message)
        Finally
        End Try
    End Sub
    Function Validation(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)

            If objForm.Items.Item("6").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Form cannot be added")
                Return False
            ElseIf objForm.Items.Item("8").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Form cannot be added")
                Return False
            ElseIf objForm.Items.Item("10").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Form cannot be added")
                Return False
            ElseIf objForm.Items.Item("12").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Form cannot be added")
                Return False
            ElseIf objForm.Items.Item("14").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Form cannot be added")
                Return False
            ElseIf objForm.Items.Item("17").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Form cannot be added")
                Return False
            End If

            'If CDbl(objForm.Items.Item("12").Specific.Value) < CDbl(objForm.Items.Item("22").Specific.Value) Then
            '    objMain.objApplication.StatusBar.SetText("cheque amount is less than the Payment total")
            '    Return False
            'End If

            Return True
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Function
    'Public Sub RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
    '    Dim objForm As SAPbouiCOM.Form
    '    Dim oMenuItem As SAPbouiCOM.MenuItem
    '    Dim oMenus As SAPbouiCOM.Menus
    '    Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
    '    oCreationPackage = objMain.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
    '    oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
    '    objForm = objMain.objApplication.Forms.Item(eventInfo.FormUID)
    '    Try
    '        If eventInfo.FormUID = objForm.UniqueID Then
    '            If (eventInfo.BeforeAction = True) Then
    '                If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_VIEW_MODE Then
    '                    ObjMatrix = objForm.Items.Item("1000001").Specific
    '                    If eventInfo.ItemUID = "1000001" And eventInfo.ColUID = "V_-1" And ObjMatrix.RowCount > 1 Then
    '                        Try
    '                            oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '                            oMenus = oMenuItem.SubMenus
    '                            If oMenus.Exists("Delete Row") = False Then
    '                                oCreationPackage.UniqueID = "Delete Row"
    '                                oCreationPackage.String = "Delete Row"
    '                                oCreationPackage.Enabled = True
    '                                oMenus.AddEx(oCreationPackage)
    '                            End If
    '                        Catch ex As Exception
    '                            objMain.objApplication.StatusBar.SetText(ex.Message)
    '                        End Try
    '                    ElseIf eventInfo.ItemUID = "1000001" And ObjMatrix.RowCount <= 1 Then
    '                        oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '                        oMenus = oMenuItem.SubMenus
    '                        Try
    '                            If oMenus.Exists("Delete Row") = True Then
    '                                objMain.objApplication.Menus.RemoveEx("Delete Row")
    '                            End If
    '                        Catch ex As Exception
    '                            objMain.objApplication.StatusBar.SetText(ex.Message)
    '                        End Try
    '                    End If
    '                    If eventInfo.ItemUID <> "1000001" Then
    '                        Try
    '                            oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '                            oMenus = oMenuItem.SubMenus
    '                            If oMenus.Exists("Delete Row") = True Then
    '                                objMain.objApplication.Menus.RemoveEx("Delete Row")
    '                            End If
    '                        Catch ex As Exception
    '                            objMain.objApplication.StatusBar.SetText(ex.Message)
    '                        End Try
    '                    End If
    '                End If
    '            Else
    '                Try
    '                    oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '                    oMenus = oMenuItem.SubMenus
    '                    If oMenus.Exists("Delete Row") = True Then
    '                        objMain.objApplication.Menus.RemoveEx("Delete Row")
    '                    End If
    '                Catch ex As Exception
    '                    objMain.objApplication.StatusBar.SetText(ex.Message)
    '                End Try
    '            End If
    '        End If

    '    Catch ex As Exception
    '        objMain.objApplication.StatusBar.SetText(ex.Message)
    '    End Try
    ' End Sub
End Class
