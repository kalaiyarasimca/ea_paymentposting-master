﻿Imports SAPbouiCOM

Public Class ClsPDAInput

#Region "Declarations"
    Dim objForm As SAPbouiCOM.Form
    Dim ObjMatrix As SAPbouiCOM.Matrix
    Dim oDBs_Head, oDBs_Details As SAPbouiCOM.DBDataSource
    Dim oCombobox, OCombobox1, OCombobox2, OCombobox3, OCombobox4, OCombobox5, OCombobox6, OCombobox7, OCombobox8, OCombobox9, objrowCombo, objHedCombo As SAPbouiCOM.ComboBox
    Dim ObjCheckBox, objCheckBox1, objCheckBox2 As SAPbouiCOM.CheckBox
#End Region

#Region "LoadForm"
    Sub LoadForm()
        Try
            objMain.objUtilities.LoadForm("PDCInput.xml", "NX_PDC_FORM", ResourceType.Embeded)
            objForm = objMain.objApplication.Forms.GetForm("NX_PDC_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)
            objForm.Freeze(True)
            objForm.DataBrowser.BrowseBy = "16"
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
            oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
            ObjMatrix = objForm.Items.Item("37").Specific



            objForm.Items.Item("43").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("43").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("43").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("16").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("16").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("Item_3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("Item_3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("Item_3").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("Item_4").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("Item_4").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("Item_4").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("Item_8").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("Item_8").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("Item_8").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("Item_11").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("Item_11").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("Item_11").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("25").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("25").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("25").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("1000001").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("1000001").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            '' objForm.Items.Item("1000001").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objForm.Items.Item("27").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, -1, SAPbouiCOM.BoModeVisualBehavior.mvb_False)
            objForm.Items.Item("27").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 4, SAPbouiCOM.BoModeVisualBehavior.mvb_True)
            objForm.Items.Item("27").SetAutoManagedAttribute(SAPbouiCOM.BoAutoManagedAttr.ama_Editable, 2, SAPbouiCOM.BoModeVisualBehavior.mvb_True)

            objHedCombo = objForm.Items.Item("Item_14").Specific
            objHedCombo.ValidValues.Add("FT-l", "FT-l")
            objHedCombo.ValidValues.Add("NEFT-N", "NEFT-N")
            objHedCombo.ValidValues.Add("RTGS-R", "RTGS-R")




            OCombobox1 = objForm.Items.Item("27").Specific
            OCombobox1.ValidValues.Add("IN", "IN")
            OCombobox1.ValidValues.Add("DP", "DP")
            OCombobox1.ValidValues.Add("All", "All")
            objForm.Items.Item("27").DisplayDesc = True

            objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("Item_10").Specific, "Select T1.""AbsEntry"",T1.""BankCode"" From  DSC1 T1 where IFNULL(T1.""GLAccount"",'')<>'' ")
            objForm.Items.Item("Item_10").DisplayDesc = True

            objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("Item_11").Specific, "Select T1.""BPLId"",T1.""BPLName"" From  OBPL T1 where T1.""Disabled""='N' ")
            objForm.Items.Item("Item_11").DisplayDesc = True


            objMain.objUtilities.ComboBoxLoadValues(objForm.Items.Item("43").Specific, "Select ""Series"", ""SeriesName"" From ""NNM1"" Where ""ObjectCode"" = 'NX_OPDC' And ""Indicator"" In " &
                      "(Select ""Indicator"" From OFPR Where ""PeriodStat"" = 'N') And ""Locked"" = 'N'")
            objForm.Items.Item("43").DisplayDesc = True


            Dim oColumn As SAPbouiCOM.Column
            oColumn = ObjMatrix.Columns.Item("V_3")
            oColumn.ColumnSetting.SumType = BoColumnSumType.bst_Auto

            objForm.Mode = BoFormMode.fm_ADD_MODE


            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText("Load Break Down Failed")
        Finally
        End Try
    End Sub
#End Region

#Region "SetDefaultValues"
    Sub SetDefaultValues(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)

            oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
            oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
            ObjMatrix = objForm.Items.Item("37").Specific
            OCombobox4 = objForm.Items.Item("43").Specific


            oDBs_Head.SetValue("DocNum", oDBs_Head.Offset, objMain.objUtilities.GetNextDocNum(objForm, "NX_OPDC"))
            oDBs_Head.SetValue("U_DDATE", oDBs_Head.Offset, Today.ToString("yyyyMMdd"))
            oDBs_Head.SetValue("U_PDATE", oDBs_Head.Offset, Today.ToString("yyyyMMdd"))
            oDBs_Head.SetValue("U_STATUS", oDBs_Head.Offset, "Open")
            oDBs_Head.SetValue("U_ASDATE", oDBs_Head.Offset, Today.ToString("yyyyMMdd"))
            oDBs_Head.SetValue("U_LTYPE", oDBs_Head.Offset, "All")

            ' oDBs_Head.SetValue("U_BPTYPE", oDBs_Head.Offset, "C")


            Dim DateVal As String = objForm.Items.Item("18").Specific.Value
            Dim Series As String = "Select T0.""Series"", T0.""SeriesName"" from NNM1 T0  " &
                "Inner join OFPR T1 on T0.""Indicator""=T1.""Indicator"" and T0.""ObjectCode"" = 'NX_OPDC'" &
                                   "Where " &
                                   "'" & DateVal & "' Between T1.""F_RefDate"" and T1.""T_RefDate"""
            Dim orsSeries As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            orsSeries.DoQuery(Series)
            OCombobox4.Select(orsSeries.Fields.Item("SeriesName").Value, SAPbouiCOM.BoSearchKey.psk_ByDescription)

        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "LoadData"
    Sub LoadData(ByVal FormUID As String, ByVal CCode As String, ByVal Type As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)

            oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
            oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
            ObjMatrix = objForm.Items.Item("37").Specific

            Dim GetInvoices As String = ""
            If Type = "All" Then
                If CCode = "" Then
                    GetInvoices = "Select * from (Select 'IN' ""Type"", ""DocNum"",""CardCode"",""CardName""," &
                                            """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                            "Cast(""DocDate"" as Date) ""DocDate"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                            "From OPCH Where ""CANCELED""='N' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 " &
                                            " UNION ALL " &
                                            "Select 'DP' ""Type"",""DocNum"",""CardCode"",""CardName""," &
                                            """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                            "Cast(""DocDate"" as Date) ""DocDate"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                            "From ODPO Where ""CANCELED""='N' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 ) OVERALLDATA Order by ""CardCode"",""DocDate"""
                ElseIf CCode <> "" Then
                    GetInvoices = "Select * from (Select 'IN' ""Type"", ""DocNum"",""CardCode"",""CardName""," &
                                        """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                        "Cast(""DocDate"" as Date) ""DocDate"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                        "From OPCH Where ""CANCELED""='N' and ""CardCode""='" & CCode & "' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 " &
                                        " UNION ALL " &
                                        "Select 'DP' ""Type"",""DocNum"",""CardCode"",""CardName""," &
                                        """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                        "Cast(""DocDate"" as Date) ""DocDate"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                        "From ODPO Where ""CANCELED""='N'  and ""CardCode""='" & CCode & "' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 ) OVERALLDATA Order by ""CardCode"",""DocDate"""

                End If
            ElseIf Type = "IN" Then
                If CCode = "" Then
                    GetInvoices = "Select * from (Select 'IN' ""Type"", ""DocNum"",""CardCode"",""CardName""," &
                                            """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                            "Cast(""DocDate"" as Date) ""DocDate"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                            "From OPCH Where ""CANCELED""='N' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 " &
                                             ") OVERALLDATA Order by ""CardCode"",""DocDate"""
                ElseIf CCode <> "" Then
                    GetInvoices = "Select * from (Select 'IN' ""Type"", ""DocNum"",""CardCode"",""CardName""," &
                                        """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                        "Cast(""DocDate"" as Date) ""DocDate"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                        "From OPCH Where ""CANCELED""='N' and ""CardCode""='" & CCode & "' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 " &
                                        ") OVERALLDATA Order by ""CardCode"",""DocDate"""

                End If
            ElseIf Type = "DP" Then
                If CCode = "" Then
                    GetInvoices = "Select * from (Select 'DP' ""Type"",""DocNum"",""CardCode"",""CardName""," &
                                            """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                            "Cast(""DocDate"" as Date) ""DocDate"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                            "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                            "From ODPO Where ""CANCELED""='N' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 ) OVERALLDATA Order by ""CardCode"",""DocDate"""
                ElseIf CCode <> "" Then
                    GetInvoices = "Select * from (Select 'DP' ""Type"",""DocNum"",""CardCode"",""CardName""," &
                                        """DocEntry"",""DocTotal"",(""DocTotal""-""PaidSum"") ""Balance"", " &
                                        "Cast(""DocDate"" as Date) ""DocDate"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 0 and 30 Then (""DocTotal""-""PaidSum"") Else 0 End ""0-30"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 31 and 60 Then (""DocTotal""-""PaidSum"") Else 0 End ""31-60"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 61 and 90 Then (""DocTotal""-""PaidSum"") Else 0 End ""61-90"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 91 and 180 Then (""DocTotal""-""PaidSum"") Else 0 End ""91-180"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 181 and 270 Then (""DocTotal""-""PaidSum"") Else 0 End ""181-270"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 Between 271 and 360 Then (""DocTotal""-""PaidSum"") Else 0 End ""271-360"", " &
                                        "Case When Days_between(""DocDate"",'" & objForm.Items.Item("Item_8").Specific.Value & "')+1 >360 Then (""DocTotal""-""PaidSum"") Else 0 End ""360 Above"" " &
                                        "From ODPO Where ""CANCELED""='N'  and ""CardCode""='" & CCode & "' and ""DocDate"" Between '" & objForm.Items.Item("Item_3").Specific.Value & "' and '" & objForm.Items.Item("Item_4").Specific.Value & "' and ""BPLId""='" & objForm.Items.Item("Item_11").Specific.Value & "'  and ""DocTotal""-""PaidSum"" <>0 ) OVERALLDATA Order by ""CardCode"",""DocDate"""
                End If
            End If
            Dim oRsGetInvoices As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetInvoices.DoQuery(GetInvoices)

            ObjMatrix.Clear()
            oDBs_Details.Clear()
            ObjMatrix.FlushToDataSource()

            If oRsGetInvoices.RecordCount <> 0 Then
                objMain.objApplication.StatusBar.SetText("Data Loading,please wait......", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                For i As Integer = 1 To oRsGetInvoices.RecordCount
                    ObjMatrix.AddRow()
                    oDBs_Details.SetValue("LineId", oDBs_Details.Offset, i)
                    oDBs_Details.SetValue("U_CHECK", oDBs_Details.Offset, "N")
                    oDBs_Details.SetValue("U_CCODE", oDBs_Details.Offset, oRsGetInvoices.Fields.Item("CardCode").Value)
                    oDBs_Details.SetValue("U_CNAME", oDBs_Details.Offset, oRsGetInvoices.Fields.Item("CardName").Value)
                    oDBs_Details.SetValue("U_TYPE", oDBs_Details.Offset, oRsGetInvoices.Fields.Item("Type").Value)
                    oDBs_Details.SetValue("U_IENTRY", oDBs_Details.Offset, oRsGetInvoices.Fields.Item("DocEntry").Value)
                    oDBs_Details.SetValue("U_IDNUM", oDBs_Details.Offset, oRsGetInvoices.Fields.Item("DocNum").Value)
                    oDBs_Details.SetValue("U_OENTRY", oDBs_Details.Offset, "")
                    Dim ddt As Date = oRsGetInvoices.Fields.Item("DocDate").Value
                    oDBs_Details.SetValue("U_INDATE", oDBs_Details.Offset, ddt.ToString("yyyyMMdd"))
                    oDBs_Details.SetValue("U_INVTTL", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("DocTotal").Value))
                    oDBs_Details.SetValue("U_INVBAL", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("Balance").Value))
                    oDBs_Details.SetValue("U_PAMNT", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("Balance").Value))
                    oDBs_Details.SetValue("U_30", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("0-30").Value))
                    oDBs_Details.SetValue("U_60", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("31-60").Value))
                    oDBs_Details.SetValue("U_90", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("61-90").Value))
                    oDBs_Details.SetValue("U_180", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("91-180").Value))
                    oDBs_Details.SetValue("U_270", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("181-270").Value))
                    oDBs_Details.SetValue("U_360", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("271-360").Value))
                    oDBs_Details.SetValue("U_ABOVE", oDBs_Details.Offset, CDbl(oRsGetInvoices.Fields.Item("360 Above").Value))
                    oDBs_Details.SetValue("U_STATUS", oDBs_Details.Offset, "Open")
                    objrowCombo = ObjMatrix.Columns.Item("Col_7").Cells(i - 1).Specific
                    If i = 1 Then
                        If (objrowCombo.ValidValues.Count <> 0) Then
                            For R As Integer = objrowCombo.ValidValues.Count - 1 To 0 Step -1
                                Try
                                    objrowCombo.ValidValues.Remove(R, SAPbouiCOM.BoSearchKey.psk_Index)
                                Catch ex As Exception
                                End Try
                            Next
                        End If

                        objrowCombo.ValidValues.Add("FT-l", "FT-l")
                        objrowCombo.ValidValues.Add("NEFT-N", "NEFT-N")
                        objrowCombo.ValidValues.Add("RTGS-R", "RTGS-R")
                    End If
                    oDBs_Details.SetValue("U_PAYMODE", oDBs_Details.Offset, oDBs_Head.GetValue("U_PAYMODE", 0))
                    ObjMatrix.SetLineData(ObjMatrix.VisualRowCount)
                    oRsGetInvoices.MoveNext()

                    'objrowCombo.Select(oDBs_Head.GetValue("U_PAYMODE", 0), BoSearchKey.psk_ByValue)
                Next
                objMain.objApplication.StatusBar.SetText("Data loaded successfully", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

            End If
            ObjMatrix.AutoResizeColumns()
            'Dim oColumn As SAPbouiCOM.Column
            'oColumn = ObjMatrix.Columns.Item("V_3")
            'oColumn.ColumnSetting.SumType = BoColumnSumType.bst_Auto

            objForm.Freeze(False)

        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText("Load Break Down Failed")
        Finally
        End Try
    End Sub
#End Region

#Region "Menu Event"
    Sub MenuEvent(ByRef pVAl As SAPbouiCOM.MenuEvent, ByRef BubbleEvent As Boolean)
        Try
            If pVAl.MenuUID = "NX_OPDC" And pVAl.BeforeAction = False Then
                Me.LoadForm()
                Me.SetDefaultValues(objForm.UniqueID)
            ElseIf pVAl.MenuUID = "1282" And pVAl.BeforeAction = False Then
                Me.SetDefaultValues(objForm.UniqueID)
                'ElseIf pVAl.MenuUID = "Delete Row" And pVAl.BeforeAction = False Then
                '    ObjMatrix = objForm.Items.Item("37").Specific
                '    For i As Integer = 1 To ObjMatrix.VisualRowCount - 1
                '        If ObjMatrix.IsRowSelected(i) = True Then
                '            ObjMatrix.DeleteRow(i)
                '        End If
                '    Next
                '    For i As Integer = 1 To ObjMatrix.VisualRowCount
                '        ObjMatrix.Columns.Item("V_-1").Cells.Item(i).Specific.string = i
                '    Next
                '    If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                'ElseIf pVAl.MenuUID = "Cancel" And pVAl.BeforeAction = False Then
                '    If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                '        If objForm.Items.Item("20").Specific.Value = "Open" Then
                '            oDBs_Head.SetValue("U_STATUS", oDBs_Head.Offset, "Cancel")
                '            If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE
                '            objForm.Items.Item("1").Click(SAPbouiCOM.BoCellClickType.ct_Regular)
                '        End If
                '    End If
            End If
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region

#Region "ItemEvent"
    Sub ItemEvent(ByVal FormUID As String, ByRef pVal As SAPbouiCOM.ItemEvent, ByRef BubbleEvent As Boolean)
        Try
            Select Case pVal.EventType
                Case SAPbouiCOM.BoEventTypes.et_MATRIX_LINK_PRESSED
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
                    oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
                    ObjMatrix = objForm.Items.Item("37").Specific

                    If pVal.ItemUID = "37" And pVal.ColUID = "V_8" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        Try
                            Dim EdTx As SAPbouiCOM.Columns
                            EdTx = ObjMatrix.Columns
                            Dim ocolumn As SAPbouiCOM.Column = EdTx.Item("V_8")
                            Dim olink As SAPbouiCOM.LinkedButton = ocolumn.ExtendedObject

                            If ObjMatrix.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.Value = "IN" Then
                                olink.LinkedObjectType = "18"
                            ElseIf ObjMatrix.Columns.Item("V_5").Cells.Item(pVal.Row).Specific.Value = "DP" Then
                                olink.LinkedObjectType = "204"
                            End If
                        Catch ex As Exception
                        End Try
                    End If

                Case SAPbouiCOM.BoEventTypes.et_CHOOSE_FROM_LIST
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
                    oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
                    ObjMatrix = objForm.Items.Item("37").Specific


                    Dim oCFL As SAPbouiCOM.ChooseFromList
                    Dim CFLEvent As SAPbouiCOM.IChooseFromListEvent = pVal
                    Dim CFL_ID As String
                    CFL_ID = CFLEvent.ChooseFromListUID
                    oCFL = objForm.ChooseFromLists.Item(CFL_ID)
                    Dim oDT As SAPbouiCOM.DataTable
                    oDT = CFLEvent.SelectedObjects

                    If pVal.BeforeAction = True Then
                        If oCFL.UniqueID = "CFL_CCODE" Then
                            Me.CFLFilter(objForm.UniqueID, oCFL.UniqueID)
                        End If
                    End If

                    objForm = objMain.objApplication.Forms.GetForm(pVal.FormTypeEx, pVal.FormTypeCount)
                    If (Not oDT Is Nothing) And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And pVal.BeforeAction = False Then
                        If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then objForm.Mode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE

                        If oCFL.UniqueID = "CFL_CCODE" Then
                            oDBs_Head.SetValue("U_CCODE", oDBs_Head.Offset, oDT.GetValue("CardCode", 0))
                            oDBs_Head.SetValue("U_CNAME", oDBs_Head.Offset, oDT.GetValue("CardName", 0))

                            Dim GetHouseBank As String = "Select T1.""BankCode"",T1.""AbsEntry"" From OCRD T0 inner join DSC1 T1 on T0.""HouseBank"" = T1.""BankCode"" Where T0.""CardCode""='" & oDT.GetValue("CardCode", 0) & "' and  IFNULL(T1.""GLAccount"",'')<>'' "
                            Dim OrsGetHouseBank As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                            OrsGetHouseBank.DoQuery(GetHouseBank)

                            If OrsGetHouseBank.RecordCount <> 0 Then
                                oDBs_Head.SetValue("U_BGL", oDBs_Head.Offset, OrsGetHouseBank.Fields.Item("AbsEntry").Value)
                                objForm.Items.Item("Item_10").DisplayDesc = True
                            Else
                                oDBs_Head.SetValue("U_BGL", oDBs_Head.Offset, "")
                            End If

                            'If oDT.GetValue("CardCode", 0) <> "" And objForm.Items.Item("Item_3").Specific.Value <> "" And objForm.Items.Item("Item_4").Specific.Value <> "" And objForm.Items.Item("Item_11").Specific.Value <> "" Then
                            '    Me.LoadData(objForm.UniqueID, oDT.GetValue("CardCode", 0), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                            'End If
                        End If
                    End If

                Case SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
                    oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
                    ObjMatrix = objForm.Items.Item("37").Specific


                    If pVal.ItemUID = "1" And pVal.BeforeAction = True And (pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        If Me.Validation(objForm.UniqueID) = False Then
                            BubbleEvent = False
                        End If
                    End If

                    If pVal.ItemUID = "1" And pVal.BeforeAction = False And pVal.ActionSuccess = True And pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Then
                        Me.SetDefaultValues(objForm.UniqueID)
                    End If

                    If pVal.ItemUID = "Item_6" And pVal.BeforeAction = False Then
                        If pVal.FormMode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
                            Me.PostOutgoingPayments(objForm.UniqueID)
                        ElseIf (pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                            objMain.objApplication.StatusBar.SetText("Please add/Update the document inorder to post the payments")
                        End If
                    End If
                    If pVal.ItemUID = "btnRef" And pVal.BeforeAction = True And (pVal.FormMode = SAPbouiCOM.BoFormMode.fm_ADD_MODE Or pVal.FormMode = SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) Then
                        If objForm.Items.Item("25").Specific.Value = "" Then
                            objForm.Items.Item("1000001").Specific.Value = ""
                        End If
                        Me.LoadData(objForm.UniqueID, oDBs_Head.GetValue("U_CCODE", oDBs_Head.Offset), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                    End If


                'comment by pritesh
                'Case SAPbouiCOM.BoEventTypes.et_LOST_FOCUS
                '    objForm = objMain.objApplication.Forms.Item(FormUID)
                '    oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
                '    oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
                '    ObjMatrix = objForm.Items.Item("37").Specific

                '    If pVal.ItemUID = "Item_3" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                '        If objForm.Items.Item("Item_3").Specific.Value <> "" And objForm.Items.Item("Item_4").Specific.Value <> "" And objForm.Items.Item("Item_11").Specific.Value <> "" Then
                '            Me.LoadData(objForm.UniqueID, oDBs_Details.GetValue("U_CCODE", oDBs_Details.Offset), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                '        End If
                '    End If

                '    If pVal.ItemUID = "Item_4" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                '        If objForm.Items.Item("Item_3").Specific.Value <> "" And objForm.Items.Item("Item_4").Specific.Value <> "" And objForm.Items.Item("Item_11").Specific.Value <> "" Then
                '            Me.LoadData(objForm.UniqueID, oDBs_Details.GetValue("U_CCODE", oDBs_Details.Offset), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                '        End If
                '    End If

                '    If pVal.ItemUID = "Item_8" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                '        If objForm.Items.Item("Item_3").Specific.Value <> "" And objForm.Items.Item("Item_4").Specific.Value <> "" And objForm.Items.Item("Item_11").Specific.Value <> "" Then
                '            Me.LoadData(objForm.UniqueID, oDBs_Details.GetValue("U_CCODE", oDBs_Details.Offset), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                '        End If
                '    End If


                Case SAPbouiCOM.BoEventTypes.et_COMBO_SELECT
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
                    oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
                    ObjMatrix = objForm.Items.Item("37").Specific
                    If pVal.ItemUID = "Item_14" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        For index = 1 To ObjMatrix.RowCount
                            objrowCombo = ObjMatrix.Columns.Item("Col_7").Cells(index - 1).specific
                            objrowCombo.Select(oDBs_Head.GetValue("U_PAYMODE", 0), BoSearchKey.psk_ByValue)
                        Next
                    End If
                    'If pVal.ItemUID = "Item_11" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    '    If objForm.Items.Item("Item_3").Specific.Value <> "" And objForm.Items.Item("Item_4").Specific.Value <> "" And objForm.Items.Item("Item_11").Specific.Value <> "" Then
                    '        Me.LoadData(objForm.UniqueID, oDBs_Details.GetValue("U_CCODE", oDBs_Details.Offset), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                    '    End If
                    'End If

                    'If pVal.ItemUID = "27" And pVal.BeforeAction = False And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                    '    If objForm.Items.Item("Item_3").Specific.Value <> "" And objForm.Items.Item("Item_4").Specific.Value <> "" And objForm.Items.Item("Item_11").Specific.Value <> "" Then
                    '        Me.LoadData(objForm.UniqueID, oDBs_Details.GetValue("U_CCODE", oDBs_Details.Offset), oDBs_Head.GetValue("U_LTYPE", oDBs_Head.Offset))
                    '    End If
                    'End If
                 'Change by Pritesh
                Case SAPbouiCOM.BoEventTypes.et_DOUBLE_CLICK
                    objForm = objMain.objApplication.Forms.Item(FormUID)
                    oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
                    oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
                    ObjMatrix = objForm.Items.Item("37").Specific

                    If pVal.ColUID = "V_9" And pVal.BeforeAction = True And pVal.FormMode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE Then
                        'objForm.Freeze(True)
                        If ObjMatrix.RowCount < 150 Then
                            For index = 1 To ObjMatrix.RowCount
                                'oDBs_Details.SetValue("U_CHECK", index - 1, "Y")
                                'Keys.RShiftKey
                                'ObjMatrix.Columns.Item("V_9").Cells(0).click()
                                'ObjMatrix.Columns.Item("V_9").Cells(ObjMatrix.VisualRowCount - 1).click()
                                ObjCheckBox = ObjMatrix.Columns.Item("V_9").Cells(index - 1).specific
                                ObjCheckBox.Checked = True
                            Next
                        Else
                            objMain.objApplication.StatusBar.SetText("Loaded Data is out of range please select short time range", BoMessageTime.bmt_Short, BoStatusBarMessageType.smt_Error)
                        End If

                    End If
                    'objForm.Freeze(False)
            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText("Item Event Failed : " & ex.Message)
        Finally
        End Try
    End Sub
#End Region

#Region "Validation"
    Function Validation(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            ObjMatrix = objForm.Items.Item("37").Specific
            If objForm.Items.Item("43").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("series is left blank")
                Return False
            ElseIf objForm.Items.Item("Item_8").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("series is left blank")
                Return False
            ElseIf objForm.Items.Item("Item_10").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("House Bank left blank")
                Return False
            ElseIf objForm.Items.Item("Item_11").Specific.Value.Trim = "" Then
                objMain.objApplication.StatusBar.SetText("Please select Branch")
                Return False
            End If

            For index = 1 To ObjMatrix.RowCount
                If ObjMatrix.Columns.Item("V_3").Cells(index - 1).Specific.Value < ObjMatrix.Columns.Item("V_10").Cells(index - 1).Specific.Value Then
                    objMain.objApplication.StatusBar.SetText("Payment Amount should not be Greater than Invoice Amount")
                    Return False
                End If
            Next


            If ObjMatrix.VisualRowCount = 0 Then
                objMain.objApplication.StatusBar.SetText("Matrix cannot be left blank left blank")
                Return False
            End If
            Return True
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Function
#End Region

    Sub CFLFilter(ByVal FormUID As String, ByVal CFL_ID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            Select Case CFL_ID

                Case "CFL_CCODE"
                    Dim oConditions As SAPbouiCOM.Conditions
                    Dim oCondition As SAPbouiCOM.Condition
                    Dim oChooseFromList As SAPbouiCOM.ChooseFromList
                    Dim emptyCon As New SAPbouiCOM.Conditions
                    oChooseFromList = objMain.objApplication.Forms.Item(FormUID).ChooseFromLists.Item(CFL_ID)
                    oChooseFromList.SetConditions(emptyCon)
                    oConditions = oChooseFromList.GetConditions()

                    If oConditions.Count > 0 Then oConditions.Item(oConditions.Count - 1).Relationship = SAPbouiCOM.BoConditionRelationship.cr_OR
                    oCondition = oConditions.Add()
                    oCondition.Alias = "CardType"
                    oCondition.Operation = SAPbouiCOM.BoConditionOperation.co_EQUAL
                    oCondition.CondVal = "S"
                    oChooseFromList.SetConditions(oConditions)
            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub UpdateOutgoingPaymentsNo(ByVal FormUID As String, ByVal DocEntry As String, ByVal LineID As Integer, ByVal OPNo As String)
        Try
            objForm.Freeze(True)
            objMain.sCmp = objMain.objCompany.GetCompanyService
            objMain.oGeneralService = objMain.sCmp.GetGeneralService("NX_OPDC")

            objMain.oGeneralParams = objMain.oGeneralService.GetDataInterface(SAPbobsCOM.GeneralServiceDataInterfaces.gsGeneralDataParams)
            objMain.oGeneralParams.SetProperty("DocEntry", DocEntry)
            objMain.oGeneralData = objMain.oGeneralService.GetByParams(objMain.oGeneralParams)

            objMain.oChildren = objMain.oGeneralData.Child("NX_PDC1")
            objMain.oChildren.Item(LineID).SetProperty("U_OENTRY", OPNo.ToString)
            objMain.oChildren.Item(LineID).SetProperty("U_STATUS", "Closed")

            objMain.oGeneralService.Update(objMain.oGeneralData)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

    Sub PostOutgoingPayments(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
            oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
            ObjMatrix = objForm.Items.Item("37").Specific

            objForm.Freeze(True)

            Dim GetVendorList As String = "Select Distinct T4.""U_CCODE"" ""Vendor"" " &
                               "from  ""@NX_PDC1"" T4 " &
                               "Where T4.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " &
                               "And " &
                               "IFNULL(T4.""U_CHECK"",'N')='Y' and T4.""U_STATUS""='Open' and IFNULL(T4.""U_OENTRY"",'')='' "
            Dim oRsGetVendorList As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
            oRsGetVendorList.DoQuery(GetVendorList)


            objMain.objApplication.StatusBar.SetText("Posting initiated ,please wait......", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            For Cust As Integer = 1 To oRsGetVendorList.RecordCount

                Dim GetData1 As String = "Select T4.""U_CCODE"" ""Vendor"",Cast(T8.""U_PDATE"" As Date) ""PPDC Date"",T4.""LineId"",T8.""DocEntry"", " &
                               " IFNULL(T4.""U_IENTRY"",'') ""Inv No"", T4.""U_TYPE"", " &
                               " T4.""U_PAMNT"" ""CQCInv Amt"",(Select A.""GLAccount"" From DSC1 A where  A.""AbsEntry""='" & objForm.Items.Item("Item_10").Specific.value & "') ""House Bank GL"", " &
                               " (Select Sum(A0.""U_PAMNT"") From ""@NX_PDC1"" A0 Where A0.""U_STATUS""='Open' and A0.""DocEntry""=T4.""DocEntry"" and IFNULL(A0.""U_CHECK"",'N')='Y' and IFNULL(A0.""U_OENTRY"",'')='' and A0.""U_CCODE""='" & oRsGetVendorList.Fields.Item("Vendor").Value & "') ""Total Amnt"",T8.""U_BRANCH"" " &
                               "from  ""@NX_PDC1"" T4 " &
                               "Inner join ""@NX_OPDC"" T8 on T4.""DocEntry""=T8.""DocEntry"" " &
                               "Where T4.""DocEntry""='" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' " &
                               "And " &
                               "IFNULL(T4.""U_CHECK"",'N')='Y' and IFNULL(T4.""U_OENTRY"",'')='' and T4.""U_STATUS""='Open' and T4.""U_CCODE""='" & oRsGetVendorList.Fields.Item("Vendor").Value & "'"
                Dim oRsGetData1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                oRsGetData1.DoQuery(GetData1)

                If oRsGetData1.RecordCount <> 0 Then
                    Dim oOutgoingPayments As SAPbobsCOM.Payments = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.oVendorPayments)
                    oOutgoingPayments.DocType = SAPbobsCOM.BoRcptTypes.rSupplier

                    'objMain.objApplication.StatusBar.SetText("Outgoing Payments for Line : " & oRsGetData1.Fields.Item("LineId").Value, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)
                    Dim PPDate1 As Date = oRsGetData1.Fields.Item("PPDC Date").Value
                    Dim CCODE1 As String = oRsGetData1.Fields.Item("Vendor").Value
                    Dim Val As String = objForm.Items.Item("Item_1").Specific.Value
                    Dim ValD As Date = Date.ParseExact(Val, "yyyyMMdd", System.Globalization.DateTimeFormatInfo.InvariantInfo)
                    oOutgoingPayments.DocDate = ValD
                    oOutgoingPayments.Remarks = "Payment Postings OutgoingPayments"
                    '' oOutgoingPayments.DocCurrency = oRsGetData1.Fields.Item("U_CQCUR").Value
                    oOutgoingPayments.TaxDate = ValD
                    oOutgoingPayments.DueDate = ValD
                    oOutgoingPayments.CardCode = CCODE1
                    oOutgoingPayments.BPLID = oRsGetData1.Fields.Item("U_BRANCH").Value
                    oOutgoingPayments.UserFields.Fields.Item("U_PENTRY").Value = oDBs_Head.GetValue("DocEntry", oDBs_Head.Offset)
                    Dim Docrate1 As Double = oOutgoingPayments.DocRate

                    If oRsGetData1.Fields.Item("Inv No").Value <> "" Then
                        For k As Integer = 1 To oRsGetData1.RecordCount
                            If k > 1 Then
                                oOutgoingPayments.Invoices.Add()
                            End If
                            If oRsGetData1.Fields.Item("U_TYPE").Value = "IN" Then
                                oOutgoingPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseInvoice
                            ElseIf oRsGetData1.Fields.Item("U_TYPE").Value = "DP" Then
                                oOutgoingPayments.Invoices.InvoiceType = SAPbobsCOM.BoRcptInvTypes.it_PurchaseDownPayment
                            End If
                            oOutgoingPayments.Invoices.DocEntry = oRsGetData1.Fields.Item("Inv No").Value
                            oOutgoingPayments.Invoices.SumApplied = oRsGetData1.Fields.Item("CQCInv Amt").Value
                            oRsGetData1.MoveNext()
                        Next
                    End If

                    oRsGetData1.MoveFirst()

                    oOutgoingPayments.TransferAccount = oRsGetData1.Fields.Item("House Bank GL").Value
                    oOutgoingPayments.TransferDate = ValD
                    oOutgoingPayments.TransferReference = "Payment Posting"
                    oOutgoingPayments.TransferSum = oRsGetData1.Fields.Item("Total Amnt").Value


                    If oOutgoingPayments.Add <> 0 Then
                        objMain.objApplication.StatusBar.SetText(objMain.objCompany.GetLastErrorDescription, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Error)
                    Else
                        Dim oapinv_docno_str1 As String = objMain.objCompany.GetNewObjectKey()
                        objMain.objApplication.StatusBar.SetText("Outgoing Payments  Posted Successfully for vendor - " & oRsGetData1.Fields.Item("Vendor").Value & ", SAP Record No - " & oapinv_docno_str1, SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Success)

                        Dim GetOPNo1 As String = "Select ""DocNum"",""DocEntry"" From OVPM Where ""DocEntry"" = '" & oapinv_docno_str1 & "'"
                        Dim oRsGetOPNo1 As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsGetOPNo1.DoQuery(GetOPNo1)

                        For i As Integer = 1 To oRsGetData1.RecordCount
                            Me.UpdateOutgoingPaymentsNo(objForm.UniqueID, oDBs_Details.GetValue("DocEntry", 0).Trim, CInt(oRsGetData1.Fields.Item("LineId").Value) - 1, oRsGetOPNo1.Fields.Item("DocEntry").Value.ToString)
                            oRsGetData1.MoveNext()
                        Next
                    End If

                End If
                oRsGetVendorList.MoveNext()
            Next
            ''  objMain.objApplication.StatusBar.SetText("Posting initiated ,please wait......", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Warning)

            Me.RefreshData(objForm.UniqueID)
            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.Status1Bar.SetText(ex.Message)
        End Try
    End Sub

    Sub RefreshData(ByVal FormUID As String)
        Try
            objForm = objMain.objApplication.Forms.Item(FormUID)
            oDBs_Head = objForm.DataSources.DBDataSources.Item("@NX_OPDC")
            oDBs_Details = objForm.DataSources.DBDataSources.Item("@NX_PDC1")
            ObjMatrix = objForm.Items.Item("37").Specific

            objForm.Freeze(True)
            objMain.objUtilities.RefreshDatasourceFromDB(FormUID, objForm.DataSources.DBDataSources.Item("@NX_OPDC"), "DocEntry", oDBs_Head.GetValue("DocEntry", 0))
            objMain.objUtilities.RefreshDatasourceFromDB(FormUID, objForm.DataSources.DBDataSources.Item("@NX_PDC1"), "DocEntry", oDBs_Details.GetValue("DocEntry", 0))
            ObjMatrix.LoadFromDataSource()
            objForm.Refresh()

            'Me.SetRowEditable(objForm.UniqueID)

            objForm.Freeze(False)
        Catch ex As Exception
            objForm.Freeze(False)
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub

#Region "FormDataEvent"
    Sub FormDataEvent(ByRef BusinessObjectInfo As SAPbouiCOM.BusinessObjectInfo, ByRef BubbleEvent As Boolean)
        Try
            objForm = objMain.objApplication.Forms.GetForm("NX_PDC_FORM", objMain.objApplication.Forms.ActiveForm.TypeCount)

            Select Case BusinessObjectInfo.EventType
                Case SAPbouiCOM.BoEventTypes.et_FORM_DATA_LOAD
                    If BusinessObjectInfo.BeforeAction = False And BusinessObjectInfo.ActionSuccess = True Then
                        Dim CheckData As String = "Select ""LineId"" From ""@NX_PDC1"" Where ""DocEntry"" = '" & oDBs_Head.GetValue("DocEntry", 0).Trim & "' and IFNULL(""U_STATUS"",'Open')='Closed' "
                        Dim oRsCheckData As SAPbobsCOM.Recordset = objMain.objCompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset)
                        oRsCheckData.DoQuery(CheckData)

                        If oRsCheckData.RecordCount <> 0 Then
                            objForm.Mode = BoFormMode.fm_VIEW_MODE
                        Else
                            objForm.Mode = BoFormMode.fm_OK_MODE
                        End If

                    End If
            End Select
        Catch ex As Exception
            objMain.objApplication.StatusBar.SetText(ex.Message)
        End Try
    End Sub
#End Region


    '#Region " Right Click Event"
    '    Public Sub RightClickEvent(ByRef eventInfo As SAPbouiCOM.ContextMenuInfo, ByRef BubbleEvent As Boolean)
    '        Dim objForm As SAPbouiCOM.Form
    '        Dim oMenuItem As SAPbouiCOM.MenuItem
    '        Dim oMenus As SAPbouiCOM.Menus
    '        Dim oCreationPackage As SAPbouiCOM.MenuCreationParams
    '        oCreationPackage = objMain.objApplication.CreateObject(SAPbouiCOM.BoCreatableObjectType.cot_MenuCreationParams)
    '        oCreationPackage.Type = SAPbouiCOM.BoMenuType.mt_STRING
    '        objForm = objMain.objApplication.Forms.Item(eventInfo.FormUID)

    '        Try
    '            If eventInfo.FormUID = objForm.UniqueID Then
    '                If (eventInfo.BeforeAction = True) Then
    '                    If objForm.Mode = SAPbouiCOM.BoFormMode.fm_OK_MODE Then
    '                        Try
    '                            oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '                            oMenus = oMenuItem.SubMenus
    '                            If oMenus.Exists("Cancel") = False Then
    '                                oCreationPackage.UniqueID = "Cancel"
    '                                oCreationPackage.String = "Cancel"
    '                                oCreationPackage.Enabled = True
    '                                oMenus.AddEx(oCreationPackage)
    '                            End If
    '                        Catch ex As Exception
    '                            objMain.objApplication.StatusBar.SetText(ex.Message)
    '                        End Try
    '                    End If
    '                End If
    '            Else
    '                Try
    '                    oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '                    oMenus = oMenuItem.SubMenus
    '                    If oMenus.Exists("Cancel") = True Then
    '                        objMain.objApplication.Menus.RemoveEx("Cancel")
    '                    End If
    '                Catch ex As Exception
    '                    objMain.objApplication.StatusBar.SetText(ex.Message)
    '                End Try
    '            End If

    '            'If eventInfo.FormUID = objForm.UniqueID Then
    '            '    If (eventInfo.BeforeAction = True) Then
    '            '        If objForm.Mode <> SAPbouiCOM.BoFormMode.fm_FIND_MODE And objForm.Mode <> SAPbouiCOM.BoFormMode.fm_VIEW_MODE Then
    '            '            ObjMatrix = objForm.Items.Item("37").Specific
    '            '            If eventInfo.ItemUID = "37" And eventInfo.ColUID = "V_-1" And ObjMatrix.RowCount > 1 Then
    '            '                Try
    '            '                    oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '            '                    oMenus = oMenuItem.SubMenus
    '            '                    If oMenus.Exists("Delete Row") = False Then
    '            '                        oCreationPackage.UniqueID = "Delete Row"
    '            '                        oCreationPackage.String = "Delete Row"
    '            '                        oCreationPackage.Enabled = True
    '            '                        oMenus.AddEx(oCreationPackage)
    '            '                    End If
    '            '                Catch ex As Exception
    '            '                    objMain.objApplication.StatusBar.SetText(ex.Message)
    '            '                End Try
    '            '            ElseIf eventInfo.ItemUID = "37" And ObjMatrix.RowCount <= 1 Then
    '            '                oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '            '                oMenus = oMenuItem.SubMenus
    '            '                Try
    '            '                    If oMenus.Exists("Delete Row") = True Then
    '            '                        objMain.objApplication.Menus.RemoveEx("Delete Row")
    '            '                    End If

    '            '                Catch ex As Exception
    '            '                    objMain.objApplication.StatusBar.SetText(ex.Message)
    '            '                End Try
    '            '            End If
    '            '            If eventInfo.ItemUID <> "37" Then
    '            '                Try
    '            '                    oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '            '                    oMenus = oMenuItem.SubMenus
    '            '                    If oMenus.Exists("Delete Row") = True Then
    '            '                        objMain.objApplication.Menus.RemoveEx("Delete Row")
    '            '                    End If
    '            '                Catch ex As Exception
    '            '                    objMain.objApplication.StatusBar.SetText(ex.Message)
    '            '                End Try
    '            '            End If
    '            '        End If
    '            '    Else
    '            '        Try
    '            '            oMenuItem = objMain.objApplication.Menus.Item("1280") 'Data'
    '            '            oMenus = oMenuItem.SubMenus
    '            '            If oMenus.Exists("Delete Row") = True Then
    '            '                objMain.objApplication.Menus.RemoveEx("Delete Row")
    '            '            End If
    '            '        Catch ex As Exception
    '            '            objMain.objApplication.StatusBar.SetText(ex.Message)
    '            '        End Try
    '            '    End If
    '            'End If
    '            '' End If

    '        Catch ex As Exception
    '            objMain.objApplication.StatusBar.SetText(ex.Message)
    '        End Try
    '    End Sub
    '#End Region

End Class
